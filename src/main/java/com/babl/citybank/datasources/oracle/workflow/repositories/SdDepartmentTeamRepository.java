package com.babl.citybank.datasources.oracle.workflow.repositories;

import com.babl.citybank.datasources.oracle.workflow.schemas.SdDepartmentMaster;
import com.babl.citybank.datasources.oracle.workflow.schemas.SdDepartmentTeam;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SdDepartmentTeamRepository extends JpaRepository<SdDepartmentTeam, Integer> {
    List<SdDepartmentTeam> findBySdDepartment(SdDepartmentMaster sdDepartmentMaster);
    SdDepartmentTeam findBySdDepartmentAndTeamName(SdDepartmentMaster sdDepartmentMaster, String team);
}
