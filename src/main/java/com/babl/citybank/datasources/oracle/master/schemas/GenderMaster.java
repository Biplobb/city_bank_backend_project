package com.babl.citybank.datasources.oracle.master.schemas;

import com.babl.citybank.common.Status;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name="gender_master")
@Data
@NoArgsConstructor
public class GenderMaster extends BaseAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "gender_name")
    private String genderName;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
