package com.babl.citybank.datasources.oracle.master.repositories;

import com.babl.citybank.datasources.oracle.master.schemas.GenderMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenderMasterRepository extends JpaRepository<GenderMaster, Integer> {
    GenderMaster findById(int id);
    GenderMaster findByGenderName(String genderName);

}