package com.babl.citybank.datasources.oracle.workflow.repositories;


import com.babl.citybank.datasources.oracle.workflow.schemas.Group;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupRepository extends JpaRepository<Group, Integer> {
    Group findByGroupName(String groupName);
}
