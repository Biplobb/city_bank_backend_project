package com.babl.citybank.datasources.oracle.master.schemas;

import com.babl.citybank.common.Status;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name="state_master")
@Data
@NoArgsConstructor
public class StateMaster extends BaseAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "state_name")
    private String stateName;
    @ManyToOne
    @JoinColumn(name = "country_id", referencedColumnName = "id")
    private CountryMaster countryMaster;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
