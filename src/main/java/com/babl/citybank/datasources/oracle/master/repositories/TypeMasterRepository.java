package com.babl.citybank.datasources.oracle.master.repositories;

import com.babl.citybank.datasources.oracle.master.schemas.TypeMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TypeMasterRepository extends JpaRepository<TypeMaster, Integer> {
    TypeMaster findById(int id);
    TypeMaster findByTypeName(String typeName);

}