package com.babl.citybank.datasources.oracle.customer.schemas;

import com.babl.citybank.common.ProductType;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity(name = "group_holding")
@IdClass(GroupHoldingId.class)
public class GroupHolding implements Serializable {
    @Id
    @Column(name = "group_uid")
    private String groupUid;

    @Id
    @Column(name = "holding_type")
    @Enumerated(EnumType.STRING)
    private ProductType holdingsType;

    @Id
    @Column(name = "holding_name")
    private String holdingName;
}
