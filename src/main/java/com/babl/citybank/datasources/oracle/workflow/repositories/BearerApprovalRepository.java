package com.babl.citybank.datasources.oracle.workflow.repositories;

import com.babl.citybank.datasources.oracle.workflow.schemas.BearerApproval;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BearerApprovalRepository extends JpaRepository<BearerApproval, Integer> {
}
