package com.babl.citybank.datasources.oracle.master.schemas;

import com.babl.citybank.common.Status;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name="salutation_master")
@Data
@NoArgsConstructor
public class SalutationMaster extends BaseAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "salutation_name")
    private String salutationName;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
