package com.babl.citybank.datasources.oracle.master.repositories;

import com.babl.citybank.datasources.oracle.master.schemas.SectorMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SectorMasterRepository extends JpaRepository<SectorMaster, Integer> {
    SectorMaster findById(int id);

    SectorMaster findBySectorNameAndSectorCode(String sectorName,String sectorCode);

}