package com.babl.citybank.datasources.oracle.workflow.repositories;


import com.babl.citybank.datasources.oracle.workflow.schemas.Task;
import com.babl.citybank.datasources.oracle.workflow.schemas.TaskRoute;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRouteRepository extends JpaRepository<TaskRoute, Integer> {
    List<TaskRoute> findByTaskFrom(Task task);
}
