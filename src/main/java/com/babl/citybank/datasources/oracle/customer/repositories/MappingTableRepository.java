package com.babl.citybank.datasources.oracle.customer.repositories;

import com.babl.citybank.datasources.oracle.customer.schemas.MappingTable;
import com.babl.citybank.datasources.oracle.customer.schemas.MappingTableId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MappingTableRepository extends JpaRepository<MappingTable, MappingTableId> {
    MappingTable findByMaincustid(String cb);
}
