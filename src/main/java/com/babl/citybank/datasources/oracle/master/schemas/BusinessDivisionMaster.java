package com.babl.citybank.datasources.oracle.master.schemas;

import com.babl.citybank.common.Status;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name="business_division_master")
@Data
@NoArgsConstructor
public class BusinessDivisionMaster extends BaseAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "business_division_name")
    private String businessDivisionName;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
