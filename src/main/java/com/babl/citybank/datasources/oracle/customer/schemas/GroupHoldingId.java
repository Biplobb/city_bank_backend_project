package com.babl.citybank.datasources.oracle.customer.schemas;


import com.babl.citybank.common.ProductType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GroupHoldingId implements Serializable {
    @Column(name = "group_uid")
    private String groupUid;

    @Column(name = "holding_type")
    @Enumerated(EnumType.STRING)
    private ProductType holdingsType;

    @Column(name = "holding_name")
    private String holdingName;
}
