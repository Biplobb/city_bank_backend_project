package com.babl.citybank.datasources.oracle.workflow.schemas;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Entity(name = "welcome_letter")
public class WelcomeLetter implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "sol_id")
    private String solId;

    @Column(name = "branch_name")
    private String branchName;

    @Column(name = "customer_id")
    private String customerId;

    @Column(name = "account_number")
    private String accountNumber;

    @Column(name = "title")
    private String title;

    @Column(name = "customer_name")
    private String customerName;

    @Column(name = "acct_op_date")
    private LocalDate accountOpenDate;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "phone")
    private String phone;

    @Column(name = "scheme_code")
    private String schemeCode;

    @Column(name = "scheme_description")
    private String scheme_description;

    @Column(name = "account_name")
    private String accountName;

    @Column(name = "designation")
    private String designation;

    @Column(name = "free_text")
    private String freeText;

    @Column(name = "pod_no")
    private String podNo;

    @Column(name = "status")
    private String status;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "priority")
    private String priority;

    @Column(name = "generation_date", columnDefinition = "DATE")
    private LocalDate generationDate;

    @Column(name = "return_date", columnDefinition = "DATE")
    private LocalDate returnDate;

    @Column(name = "action_date", columnDefinition = "DATE")
    private LocalDate actionDate;
}
