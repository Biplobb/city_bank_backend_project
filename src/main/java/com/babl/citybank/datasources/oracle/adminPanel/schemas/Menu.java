package com.babl.citybank.datasources.oracle.adminPanel.schemas;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "menu")
public class Menu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false ,name="name")
    private String name;

    @Column(nullable = false,name="lft")
    private Integer lft;

    @Column(nullable = false,name="rgt")
    private Integer rgt;

    @Column(nullable = false,name="parent_id")
    private Integer parentId;
}
