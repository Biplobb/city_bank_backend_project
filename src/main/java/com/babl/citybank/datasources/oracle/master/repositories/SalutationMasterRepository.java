package com.babl.citybank.datasources.oracle.master.repositories;

import com.babl.citybank.datasources.oracle.master.schemas.SalutationMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalutationMasterRepository extends JpaRepository<SalutationMaster, Integer> {

    SalutationMaster findById(int id);

SalutationMaster findBySalutationName(String salutationName);

}