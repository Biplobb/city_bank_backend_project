package com.babl.citybank.datasources.oracle.workflow.repositories;

import com.babl.citybank.datasources.oracle.workflow.schemas.AppFile;
import com.babl.citybank.datasources.oracle.workflow.schemas.Application;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AppFileRepository extends JpaRepository<AppFile, Integer> {
    List<AppFile> findByApplication(Application application);
}
