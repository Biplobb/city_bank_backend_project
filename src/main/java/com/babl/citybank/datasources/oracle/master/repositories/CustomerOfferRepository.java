package com.babl.citybank.datasources.oracle.master.repositories;

import com.babl.citybank.datasources.oracle.master.schemas.CustomerOffer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerOfferRepository extends JpaRepository<CustomerOffer,Integer> {

    CustomerOffer findById(int id);

    CustomerOffer findByCustomerIdAndProductCode(String customerId, String productCode);
   /* @Query("SELECT C.customerId,C.productCode from ProductMaster P inner join CustomerOffer C on P.productCode=C.productCode where C.productCode=?1")
    CustomerOffer cutomerProductMappintTable(int productCode);*/
}
