package com.babl.citybank.datasources.oracle.workflow.schemas;


import com.babl.citybank.common.AssignType;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity(name = "task")
@Data
public class Task implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String title;

    @Column(name = "startable")
    private String startable;

    @Column(name = "assign_department")
    private String assignDepartment;

    @Column(name = "assign_department_name")
    private String assignmentDepartmentName;

    @Column(name = "out_inbox")
    private String outInbox;

    @ManyToMany
    @JoinTable(
            name = "task_group",
            joinColumns = @JoinColumn(name = "task_title", referencedColumnName = "title"),
            inverseJoinColumns = @JoinColumn(name = "group_name", referencedColumnName = "group_name")
    )
    private List<Group> groupList;
}
