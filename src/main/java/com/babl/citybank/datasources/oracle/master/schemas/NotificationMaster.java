package com.babl.citybank.datasources.oracle.master.schemas;
import com.babl.citybank.common.Status;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name="notification_master")
@Data
@NoArgsConstructor
public class NotificationMaster extends BaseAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "type")
    private String type;

    @Column(name= "message")
    private String message;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
