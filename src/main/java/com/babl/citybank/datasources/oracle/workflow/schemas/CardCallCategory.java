package com.babl.citybank.datasources.oracle.workflow.schemas;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "card_call_category")
@Data
public class CardCallCategory implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "call_category")
    private String callCategory;
    @ManyToOne
    @JoinColumn(name = "team_id", referencedColumnName = "id")
    private CardMaintenanceTeam cardMaintenanceTeam;
    @Column(name = "supporting_documents")
    private String SupportingDocuments;
    @Column(name = "call_back")
    private String callBack;
}
