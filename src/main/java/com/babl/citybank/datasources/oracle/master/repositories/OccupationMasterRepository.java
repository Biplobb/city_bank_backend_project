package com.babl.citybank.datasources.oracle.master.repositories;

import com.babl.citybank.datasources.oracle.master.schemas.OccupationMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OccupationMasterRepository extends JpaRepository<OccupationMaster, Integer> {

    OccupationMaster findById(int id);

OccupationMaster findByOccupationName(String occupationName);

}