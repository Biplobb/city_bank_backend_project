package com.babl.citybank.datasources.oracle.workflow.schemas;

import com.babl.citybank.common.DeferralStatus;
import com.babl.citybank.common.Status;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity(name = "deferral")
public class Deferral implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "type")
    private String type;

    @Column(name = "due_date")
    private LocalDate dueDate;

    @Column(name = "duration")
    private Integer duration;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "applied_by")
    private String appliedBy;

    @Column(name = "application_date")
    private LocalDate applicationDate;

    @Column(name = "approved_by")
    private String approvedBy;

    @Column(name = "approval_date")
    private LocalDate approvalDate;

    @Column(name = "closed_by")
    private String closedBy;

    @Column(name = "closed_date")
    private LocalDate closedDate;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private DeferralStatus status;

    @Column(name = "target_to")
    private String targetTo;

    @ManyToOne
    @JoinColumn(name = "app_id", referencedColumnName = "id")
    private Application application;

    @OneToOne
    @JoinColumn(name = "file_id", referencedColumnName = "id")
    private AppFile appFile;
}
