package com.babl.citybank.datasources.oracle.master.repositories;

import com.babl.citybank.datasources.oracle.master.schemas.CityMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityMasterRepository extends JpaRepository<CityMaster, Integer> {
    CityMaster findById(int id);
    CityMaster findByCityName(String cityName);
}