package com.babl.citybank.datasources.oracle.master.repositories;

import com.babl.citybank.datasources.oracle.master.schemas.StateMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StateMasterRepository extends JpaRepository<StateMaster, Integer> {

    StateMaster findById(int id);

    StateMaster findByStateName(String stateName);

}