package com.babl.citybank.datasources.oracle.workflow.schemas;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity(name = "group_wf")
@Data
public class Group implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "group_name")
    private String groupName;


    @ManyToMany(mappedBy = "groupList")
    private List<Task> taskList;

}
