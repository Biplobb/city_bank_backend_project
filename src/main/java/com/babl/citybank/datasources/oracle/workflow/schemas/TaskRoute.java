package com.babl.citybank.datasources.oracle.workflow.schemas;


import com.babl.citybank.common.AssignType;
import com.babl.citybank.common.RouteType;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "task_route")
@Data
public class TaskRoute implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "task_from", referencedColumnName = "title")
    private Task taskFrom;

    @ManyToOne
    @JoinColumn(name = "task_to", referencedColumnName = "title")
    private Task taskTo;

    @Column(name = "route_type")
    @Enumerated(EnumType.STRING)
    private RouteType routeType;

    @Column(name = "route_condition")
    private String routeCondition;


    @Column(name = "assign_type")
    @Enumerated(EnumType.STRING)
    private AssignType assignType;

    @Column(name = "assign_variable")
    private String assignVariable;

    @Column(name = "assign_group_table")
    private String assignGroupTable;

    @Column(name = "assign_group_condition")
    private String assignGroupCondition;

}
