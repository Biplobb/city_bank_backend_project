package com.babl.citybank.datasources.oracle.workflow.schemas;

import com.babl.citybank.common.SDRegion;
import com.babl.citybank.common.Status;
import com.babl.citybank.datasources.oracle.master.schemas.BaseAuditingEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity(name="sd_department_master")
@Data
@EqualsAndHashCode(callSuper = false)
public class SdDepartmentMaster extends BaseAuditingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "department_name")
    private String departmentName;

    @Column(name = "region")
    @Enumerated(EnumType.STRING)
    private SDRegion region;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
