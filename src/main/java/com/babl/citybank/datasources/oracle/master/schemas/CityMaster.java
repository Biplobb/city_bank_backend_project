package com.babl.citybank.datasources.oracle.master.schemas;

import com.babl.citybank.common.Status;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name="city_master")
@Data
@NoArgsConstructor
public class CityMaster extends BaseAuditingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "city_name")
    private String cityName;

    @ManyToOne
    @JoinColumn(name = "state_id",referencedColumnName = "id")
    private StateMaster stateMaster ;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

}
