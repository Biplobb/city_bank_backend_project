package com.babl.citybank.datasources.oracle.master.schemas;

import com.babl.citybank.common.Status;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name="sub_sector_master")
@Data
@NoArgsConstructor
public class SubSectorMaster extends BaseAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "sub_sector_code")
    private String subSectorCode;


    @Column(name = "sub_sector_name")
    private String subSectorName;
    @ManyToOne
    @JoinColumn(name = "sector_id", referencedColumnName = "id")
    private SectorMaster sectorMaster;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
