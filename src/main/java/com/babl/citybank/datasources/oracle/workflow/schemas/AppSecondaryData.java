package com.babl.citybank.datasources.oracle.workflow.schemas;

import com.babl.citybank.common.ApprovalStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity(name = "app_secondary_data")
@NoArgsConstructor
@AllArgsConstructor
public class AppSecondaryData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "app_id", referencedColumnName = "id")
    private Application application;

    @Column(name = "identifier")
    private String identifier;

    @Column(name = "data_type")
    private String dataType;

    @Column(name = "secondary_data", columnDefinition = "CLOB")
    private String secondaryData;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date", columnDefinition = "TIMESTAMP")
    private LocalDateTime createdDate;

    @Column(name = "approval_status")
    @Enumerated(EnumType.STRING)
    private ApprovalStatus approvalStatus;

    @Column(name = "approved_by")
    private String approvedBy;

    @Column(name = "approval_date", columnDefinition = "TIMESTAMP")
    private LocalDateTime approvalDate;
}
