package com.babl.citybank.datasources.oracle.workflow.repositories;


import com.babl.citybank.datasources.oracle.workflow.schemas.BranchMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BranchMasterRepository extends JpaRepository<BranchMaster, Integer> {
    BranchMaster findById(int id);
    BranchMaster findByBranchNameAndSolId(String branchName,String solId);
}