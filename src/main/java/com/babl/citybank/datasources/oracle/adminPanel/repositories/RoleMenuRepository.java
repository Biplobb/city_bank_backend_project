package com.babl.citybank.datasources.oracle.adminPanel.repositories;


import com.babl.citybank.datasources.oracle.adminPanel.schemas.RoleMenu;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoleMenuRepository extends JpaRepository<RoleMenu, Integer> {
    List<RoleMenu> findByRoleId(Integer roleId);
    List<RoleMenu> findByRoleIdNot(Integer roleId);
    RoleMenu findByRoleIdAndMenuId(Integer roleId, Integer menuId);
    RoleMenu findById(int id);
}
