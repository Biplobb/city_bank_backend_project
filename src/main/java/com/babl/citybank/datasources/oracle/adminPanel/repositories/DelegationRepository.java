package com.babl.citybank.datasources.oracle.adminPanel.repositories;

import com.babl.citybank.datasources.oracle.adminPanel.schemas.Delegation;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DelegationRepository extends JpaRepository<Delegation, Integer> {
    Delegation findByDelegatedUser(User user);
    Delegation findByMainUser(User user);
}
