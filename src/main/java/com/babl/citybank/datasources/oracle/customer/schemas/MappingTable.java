package com.babl.citybank.datasources.oracle.customer.schemas;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Entity(name = "DEDUP_TEMP_APPLICANT_IND")
@IdClass(MappingTableId.class)
public class MappingTable implements Serializable {
    @Id
    @Column(name = "MAINCUSTID")
    private String maincustid;

    @Id
    @Column(name = "SZ_SOURCE")
    private String szSource;

    @Column(name = "SZ_FULL_NAME")
    private String szFullName;

    @Column(name = "SZ_SHORT_NAME")
    private String szShortName;

    @Column(name = "DT_BIRTH", columnDefinition = "DATE")
    private LocalDate dtBirth;

    @Column(name = "C_GENDER")
    private String cGender;

    @Column(name = "SZ_PASSPORT")
    private String szPassport;

    @Column(name = "SZ_OCCUPATION")
    private String szOccupation;

    @Column(name = "EMAIL_ID")
    private String emailId;

    @Column(name = "SZ_REGISTRATION_NO")
    private String szRegistrationNo;

    @Column(name = "SZ_NATIONAL_ID")
    private String szNationalId;

    @Column(name = "SZ_TIN_NUMBER")
    private String szTinNumber;

    @Column(name = "SZ_FATHERS_FULL_NAME")
    private String szFathersFullName;

    @Column(name = "SZ_FINNACLE_CUSTOMER_ID")
    private String szFinacleCustomerId;

    @Column(name = "SZ_ABABIL_CUSTOMER_ID")
    private String szAbabilCustomerId;

    @Column(name = "SZ_TRANZWARE_CUSTOMER_ID")
    private String szTranzwareCustomerId;

    @Column(name = "SZ_SPOUSE_NAME")
    private String szSpouseName;

    @Column(name = "SZ_CONTITUTION")
    private String szContitution;

    @Column(name = "SZCUSTOMERID")
    private String szCustomerId;

    @Column(name = "IDENTIFICATION_NO")
    private String identificationNo;

    @Column(name = "DATA_STATUS_JNT")
    private String dataStatusJnt;

    @Column(name = "DATA_COMMENTS_JNT")
    private String dataCommentsJnt;

    @Column(name = "CUST_COMU_PHONE_NUM")
    private String customerCommunicationPhoneNumber;

    @Column(name = "CUST_OPN_DATE", columnDefinition = "DATE")
    private LocalDate creationDate;
}
