package com.babl.citybank.datasources.oracle.workflow.schemas;

import com.babl.citybank.common.SDRegion;
import com.babl.citybank.common.Status;
import com.babl.citybank.datasources.oracle.master.schemas.BaseAuditingEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name="branch_master")
@Data
@NoArgsConstructor
public class BranchMaster extends BaseAuditingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "branch_name")
    private String branchName;

    @Column(name="sol_id")
    private String solId;

    @Column(name = "region")
    @Enumerated(EnumType.STRING)
    private SDRegion region;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

}
