package com.babl.citybank.datasources.oracle.master.schemas;

import com.babl.citybank.common.Status;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name="constitution_master")
@Data
@NoArgsConstructor
public class ConstitutionMaster extends BaseAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "constitution_name")
    private String constitutionName;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
