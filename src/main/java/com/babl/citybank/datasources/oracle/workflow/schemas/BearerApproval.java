package com.babl.citybank.datasources.oracle.workflow.schemas;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity(name = "bearer_approval")
public class BearerApproval implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "service_type")
    private String serviceType;

    @Column(name = "varname")
    private String varName;

    @Column(name = "approval_required")
    private String approvalRequired;
}
