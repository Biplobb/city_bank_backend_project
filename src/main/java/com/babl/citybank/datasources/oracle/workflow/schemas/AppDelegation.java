package com.babl.citybank.datasources.oracle.workflow.schemas;

import com.babl.citybank.common.AppStatus;
import com.babl.citybank.common.SDRegion;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.User;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity(name = "app_delegation")
@Data
public class AppDelegation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "app_id", referencedColumnName = "id")
    private Application application;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @Column(name = "user_group_name")
    private String userGroupName;

    @Column(name = "sd_region")
    @Enumerated(EnumType.STRING)
    private SDRegion sdRegion;

    @ManyToOne
    @JoinColumn(name = "task_id", referencedColumnName = "id")
    private Task task;

    @Enumerated(EnumType.STRING)
    private AppStatus status;

    @Column(name = "inbox_status")
    private String inboxStatus;

    @Column(name = "sent_by_role")
    private String sentByRole;

    @Column(name = "sent_by_username")
    private String sentByUsername;

    @Column(name = "service_type")
    private String serviceType;

    @Column(name = "sub_service_type")
    private String subserviceType;

    @Column(name = "del_init_timestamp", columnDefinition = "TIMESTAMP")
    private LocalDateTime delInitialTimestamp;

    @Column(name = "del_finish_timestamp", columnDefinition = "TIMESTAMP")
    private LocalDateTime delFinishTimestamp;

    @Column(name = "app_data", columnDefinition = "CLOB")
    private String appData;

}
