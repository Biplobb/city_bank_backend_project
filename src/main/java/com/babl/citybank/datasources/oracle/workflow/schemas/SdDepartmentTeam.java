package com.babl.citybank.datasources.oracle.workflow.schemas;

import com.babl.citybank.common.Status;
import com.babl.citybank.datasources.oracle.master.schemas.BaseAuditingEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity(name = "sd_department_team")
@EqualsAndHashCode(callSuper = false)
public class SdDepartmentTeam extends BaseAuditingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "team_name")
    private String teamName;

    @ManyToOne
    @JoinColumn(name = "department_name", referencedColumnName = "department_name")
    private SdDepartmentMaster sdDepartment;

    @Column(name = "assignment_condition")
    private String assignmentCondition;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
