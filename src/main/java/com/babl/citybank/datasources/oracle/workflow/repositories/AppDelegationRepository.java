package com.babl.citybank.datasources.oracle.workflow.repositories;


import com.babl.citybank.common.AppStatus;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.User;
import com.babl.citybank.datasources.oracle.workflow.schemas.AppDelegation;
import com.babl.citybank.datasources.oracle.workflow.schemas.Application;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AppDelegationRepository extends JpaRepository<AppDelegation, Integer> {
    AppDelegation findById(int id);
    List<AppDelegation> findByServiceTypeAndStatus(String serviceType, AppStatus status);
    List<AppDelegation> findByApplicationAndStatus(Application application, AppStatus status);
    AppDelegation findByApplicationAndStatusAndUser(Application application, AppStatus appStatus, User user);
    List<AppDelegation> findByStatusAndUserOrderById(AppStatus appStatus, User user);
    List<AppDelegation> findByApplicationAndUserGroupNameOrderByDelFinishTimestampDesc(Application application, String groupName);
}
