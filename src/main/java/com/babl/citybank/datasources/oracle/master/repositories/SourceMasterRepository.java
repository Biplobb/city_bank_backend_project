package com.babl.citybank.datasources.oracle.master.repositories;

import com.babl.citybank.datasources.oracle.master.schemas.SourceMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SourceMasterRepository extends JpaRepository<SourceMaster, Integer> {

    SourceMaster findById(int id);

SourceMaster findBySourceCodeAndDescription(String sourceCode,String description);

}