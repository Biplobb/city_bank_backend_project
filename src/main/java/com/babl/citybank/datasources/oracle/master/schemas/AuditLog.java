package com.babl.citybank.datasources.oracle.master.schemas;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "audit_log")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuditLog implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    //table name
    @Column(name = "object_type")
    private String objectType;

    //single entry of the table
    @Column(name = "object_id")
    private Integer objectId;

    //insert, update or delete
    @Column(name = "event_type")
    private String eventType;

    //operation time
    @Column(name = "event_time", columnDefinition = "TIMESTAMP")
    private LocalDateTime eventTime;


    //the user who has performed the operation
    @Column(name = "username")
    private String username;


    //may contain multiple fields, should be in json formate
    @Column(name = "data")
    private String data;

}
