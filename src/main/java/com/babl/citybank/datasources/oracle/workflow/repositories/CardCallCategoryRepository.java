package com.babl.citybank.datasources.oracle.workflow.repositories;

import com.babl.citybank.datasources.oracle.workflow.schemas.CardCallCategory;
import com.babl.citybank.datasources.oracle.workflow.schemas.CardMaintenanceTeam;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface CardCallCategoryRepository extends JpaRepository<CardCallCategory,Integer> {
    List<CardCallCategory> findByCardMaintenanceTeam(CardMaintenanceTeam team);
}
