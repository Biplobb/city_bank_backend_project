package com.babl.citybank.datasources.oracle.master.repositories;

import com.babl.citybank.datasources.oracle.master.schemas.SegmentMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SegmentMasterRepository extends JpaRepository<SegmentMaster, Integer> {

    SegmentMaster findById(int id);

SegmentMaster findBySegmentName(String segmentName);

}