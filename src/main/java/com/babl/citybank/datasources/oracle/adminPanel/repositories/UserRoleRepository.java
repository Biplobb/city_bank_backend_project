package com.babl.citybank.datasources.oracle.adminPanel.repositories;


import com.babl.citybank.datasources.oracle.adminPanel.schemas.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRoleRepository extends JpaRepository<UserRole, Integer> {
    UserRole findByUsernameAndRoleId(String username, int roleId);
    List<UserRole> findByUsername(String username);
    List<UserRole> findByRoleId(int roleId);
}
