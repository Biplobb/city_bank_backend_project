package com.babl.citybank.datasources.oracle.adminPanel.schemas;

import com.babl.citybank.datasources.oracle.master.schemas.BusinessDivisionMaster;
import com.babl.citybank.datasources.oracle.workflow.schemas.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity(name = "usr")
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @ManyToOne
    @JoinColumn(name = "group_name", referencedColumnName = "group_name")
    private Group group;

    @Column(name = "workplace")
    private String workplace;

    @ManyToOne
    @JoinColumn(name = "branch", referencedColumnName = "id")
    private BranchMaster branch;

    @ManyToOne
    @JoinColumn(name = "sd_department", referencedColumnName = "id")
    private SdDepartmentMaster sdDepartment;

    @ManyToOne
    @JoinColumn(name = "sd_department_team", referencedColumnName = "id")
    private SdDepartmentTeam sdDepartmentTeam;

    @ManyToOne
    @JoinColumn(name = "csu", referencedColumnName = "id")
    private CSUMaster csu;

    @ManyToOne
    @JoinColumn(name = "business_division", referencedColumnName = "id")
    private BusinessDivisionMaster businessDivision;

    @ManyToOne
    @JoinColumn(name = "card_maintenance_team", referencedColumnName = "id")
    private CardMaintenanceTeam cardMaintenanceTeam;

}
