package com.babl.citybank.datasources.oracle.master.repositories;

import com.babl.citybank.datasources.oracle.master.schemas.LineOfBusinessMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LineOfBusinessMasterRepository extends JpaRepository<LineOfBusinessMaster, Integer> {

    LineOfBusinessMaster findById(int id);

LineOfBusinessMaster findByLineOfBusinessName(String lineOfBusinessName);
}