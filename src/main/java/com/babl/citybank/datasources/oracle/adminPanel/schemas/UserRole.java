package com.babl.citybank.datasources.oracle.adminPanel.schemas;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "user_role")
public class UserRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "username")
    private String username;
    @Column(name = "role_id")
    private Integer roleId;
}
