package com.babl.citybank.datasources.oracle.master.schemas;

import com.babl.citybank.common.Status;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name="segment_master")
@Data
@NoArgsConstructor
public class SegmentMaster extends BaseAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "segment_name")
    private String segmentName;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
