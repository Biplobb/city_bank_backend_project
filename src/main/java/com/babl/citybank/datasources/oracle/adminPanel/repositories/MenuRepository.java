package com.babl.citybank.datasources.oracle.adminPanel.repositories;


import com.babl.citybank.datasources.oracle.adminPanel.schemas.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MenuRepository extends JpaRepository<Menu, Integer> {
    @Query("select m1 from menu m1, menu m2 where m2.id = ?1 and m1.lft > m2.lft and m1.rgt < m2.rgt")
    List<Menu> getMenues(int id);

    @Query(value = "SELECT (COUNT(parent.name) - 1) " +
            "FROM menu node, menu parent " +
            "WHERE node.lft BETWEEN parent.lft AND parent.rgt and node.id = ?1 " +
            "GROUP BY node.name " +
            "ORDER BY node.lft", nativeQuery = true)
    Integer getDepth(int id);

    Menu findByName(String name);

    List<Menu> findByParentId(int id);

    Menu findById(int id);
}
