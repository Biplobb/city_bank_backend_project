package com.babl.citybank.datasources.oracle.master.schemas;

import com.babl.citybank.common.Status;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "band_master")
@Data
@NoArgsConstructor
public class BandMaster extends BaseAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "band_no")
    private String bandNo;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
