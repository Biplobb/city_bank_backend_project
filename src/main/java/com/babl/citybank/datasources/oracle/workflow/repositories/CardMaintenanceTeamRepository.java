package com.babl.citybank.datasources.oracle.workflow.repositories;

import com.babl.citybank.datasources.oracle.workflow.schemas.CardMaintenanceTeam;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CardMaintenanceTeamRepository extends JpaRepository<CardMaintenanceTeam,Integer> {
}
