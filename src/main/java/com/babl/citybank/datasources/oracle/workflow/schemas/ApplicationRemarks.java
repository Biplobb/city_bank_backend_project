package com.babl.citybank.datasources.oracle.workflow.schemas;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "application_remarks")
@Data
public class ApplicationRemarks {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "remarks")
    private String remarks;
    @Column(name = "create_by_user_name")
    private String createByUserName;
    @Column(name = "create_by_user_role")
    private String createByUserRole;
    @Column(name = "application_remarks_date",columnDefinition = "TIMESTAMP")
    private LocalDate applicationRemarksDate;

    @ManyToOne
    @JoinColumn(name = "application_id", referencedColumnName = "id")
    private Application application;
}
