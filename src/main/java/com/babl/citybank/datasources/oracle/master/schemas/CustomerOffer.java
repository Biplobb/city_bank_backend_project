package com.babl.citybank.datasources.oracle.master.schemas;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name="customer_offer")
public class CustomerOffer extends BaseAuditingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "customer_id")
    private String customerId;
    @Column(name = "product_code")
    private String productCode;

}
