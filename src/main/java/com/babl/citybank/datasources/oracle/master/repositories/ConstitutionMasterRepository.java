package com.babl.citybank.datasources.oracle.master.repositories;

import com.babl.citybank.datasources.oracle.master.schemas.ConstitutionMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConstitutionMasterRepository extends JpaRepository<ConstitutionMaster, Integer> {
    ConstitutionMaster findById(int id);
    ConstitutionMaster findByConstitutionName(String constitutionName);
}