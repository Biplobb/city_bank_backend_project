package com.babl.citybank.datasources.oracle.workflow.repositories;

import com.babl.citybank.datasources.oracle.workflow.schemas.AppMessage;
import com.babl.citybank.datasources.oracle.workflow.schemas.Application;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AppMessageRepository extends JpaRepository<AppMessage, Integer> {
    List<AppMessage> findByApplication(Application application);
}
