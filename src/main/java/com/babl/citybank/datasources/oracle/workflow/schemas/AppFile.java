package com.babl.citybank.datasources.oracle.workflow.schemas;

import com.babl.citybank.datasources.oracle.master.schemas.BaseAuditingEntity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "app_file")
public class AppFile extends BaseAuditingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "app_id", referencedColumnName = "id")
    private Application application;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "type")
    private String type;

    @Column(name = "path")
    private String path;
}
