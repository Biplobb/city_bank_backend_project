package com.babl.citybank.datasources.oracle.master.schemas;

import com.babl.citybank.common.Status;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name="source_master")
@Data
@NoArgsConstructor
public class SourceMaster extends BaseAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "source_code")
    private String sourceCode;

    @Column(name="description")
    private String description;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
