package com.babl.citybank.datasources.oracle.workflow.repositories;

import com.babl.citybank.datasources.oracle.workflow.schemas.Application;
import com.babl.citybank.datasources.oracle.workflow.schemas.ApplicationRemarks;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ApplicationRemarksRepository extends JpaRepository<ApplicationRemarks,Integer> {

    List<ApplicationRemarks> findByApplication(Application application_id);
}
