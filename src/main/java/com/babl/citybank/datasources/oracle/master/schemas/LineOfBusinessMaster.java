package com.babl.citybank.datasources.oracle.master.schemas;

import com.babl.citybank.common.Status;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name="line_of_business_master")
@Data
@NoArgsConstructor
public class LineOfBusinessMaster extends BaseAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "line_of_business_name")
    private String lineOfBusinessName;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
