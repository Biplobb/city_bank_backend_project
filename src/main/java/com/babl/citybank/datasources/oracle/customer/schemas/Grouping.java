package com.babl.citybank.datasources.oracle.customer.schemas;

import com.babl.citybank.common.Status;
import com.babl.citybank.datasources.oracle.master.schemas.BaseAuditingEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name="grouping")
@Data
@NoArgsConstructor
public class Grouping extends BaseAuditingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "customer_id")
    private String customerId;

    @Column(name = "customer_name")
    private String customerName;

    @Column(name = "type")
    private String type;

    @Column(name = "description_of_grouping")
    private String descriptionOfGrouping;

    @Column(name = "group_uid")
    private String groupUid;

    @Column(name = "role")
    private String role;

    @Column(name = "rv_value")
    private String rvValue;

    @Column(name = "branch_id")
    private Integer branchId;

    @Column(name = "auditors")
    private String auditors;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
