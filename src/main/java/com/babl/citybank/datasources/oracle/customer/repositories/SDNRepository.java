package com.babl.citybank.datasources.oracle.customer.repositories;

import com.babl.citybank.datasources.oracle.customer.schemas.SDN;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SDNRepository extends JpaRepository<SDN, Integer> {
}
