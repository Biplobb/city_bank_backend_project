package com.babl.citybank.datasources.oracle.master.repositories;


import com.babl.citybank.datasources.oracle.master.schemas.ProductMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductMasterRepository extends JpaRepository<ProductMaster, Integer>{
    ProductMaster findById(int id);
    ProductMaster findByProductCode(String productCode);
    ProductMaster findByProductNameAndProductCode(String productName,String productCode);

}
