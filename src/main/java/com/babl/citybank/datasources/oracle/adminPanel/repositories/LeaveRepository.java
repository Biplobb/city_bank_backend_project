package com.babl.citybank.datasources.oracle.adminPanel.repositories;

import com.babl.citybank.datasources.oracle.adminPanel.schemas.Leave;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface LeaveRepository extends JpaRepository<Leave, Integer> {
    List<Leave> findByUserAndStartDateLessThanEqualAndEndDateGreaterThanEqual(User user, LocalDate start, LocalDate end);
}
