package com.babl.citybank.datasources.oracle.adminPanel.schemas;

import com.babl.citybank.datasources.oracle.master.schemas.BaseAuditingEntity;
import lombok.Data;

import javax.persistence.*;

@Entity(name = "delegation")
@Data
public class Delegation extends BaseAuditingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "main_user", referencedColumnName = "id")
    private User mainUser;

    @OneToOne
    @JoinColumn(name = "delegated_user", referencedColumnName = "id")
    private User delegatedUser;
}
