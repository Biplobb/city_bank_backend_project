package com.babl.citybank.datasources.oracle.workflow.repositories;


import com.babl.citybank.datasources.oracle.workflow.schemas.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Integer> {
    Task findByTitle(String title);
}
