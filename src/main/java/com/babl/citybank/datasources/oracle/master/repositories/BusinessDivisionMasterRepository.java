package com.babl.citybank.datasources.oracle.master.repositories;

import com.babl.citybank.datasources.oracle.master.schemas.BusinessDivisionMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BusinessDivisionMasterRepository extends JpaRepository<BusinessDivisionMaster, Integer> {
    BusinessDivisionMaster findById(int id);
    BusinessDivisionMaster findByBusinessDivisionName(String businessDivisionName);
}