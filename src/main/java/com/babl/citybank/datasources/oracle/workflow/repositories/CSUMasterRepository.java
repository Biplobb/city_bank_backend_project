package com.babl.citybank.datasources.oracle.workflow.repositories;

import com.babl.citybank.datasources.oracle.workflow.schemas.CSUMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CSUMasterRepository extends JpaRepository<CSUMaster, Integer> {
}
