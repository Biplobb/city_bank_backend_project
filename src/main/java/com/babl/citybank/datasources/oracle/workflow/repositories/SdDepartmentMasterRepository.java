package com.babl.citybank.datasources.oracle.workflow.repositories;

import com.babl.citybank.datasources.oracle.workflow.schemas.SdDepartmentMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SdDepartmentMasterRepository extends JpaRepository<SdDepartmentMaster,Integer> {
SdDepartmentMaster findById(int id);
SdDepartmentMaster findByDepartmentName(String departmentName);

}
