package com.babl.citybank.datasources.oracle.master.schemas;

import com.babl.citybank.common.Status;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name="category_master")
@Data
@NoArgsConstructor
public class CategoryMaster extends BaseAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "category_name")
    private String categoryName;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
