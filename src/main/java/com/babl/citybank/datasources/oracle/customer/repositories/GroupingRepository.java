package com.babl.citybank.datasources.oracle.customer.repositories;


import com.babl.citybank.common.Status;
import com.babl.citybank.datasources.oracle.customer.schemas.Grouping;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GroupingRepository extends JpaRepository<Grouping, Integer>{
    Grouping findById(int id);
    Grouping findByGroupUidAndCustomerId(String groupUid,String customerId);
    Grouping  findByCustomerId(String customerId);
    List<Grouping> findByGroupUid(String groupUid);
    List<Grouping> findByStatus(Status status);
    List<Grouping> findByStatusAndBranchId(Status status, int branchId);
}
