package com.babl.citybank.datasources.oracle.master.schemas;

import com.babl.citybank.common.Status;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name="occupation_master")
@Data
@NoArgsConstructor
public class OccupationMaster extends BaseAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "occupation_name")
    private String occupationName;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
