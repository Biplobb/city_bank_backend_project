package com.babl.citybank.datasources.oracle.master.repositories;

import com.babl.citybank.datasources.oracle.master.schemas.NotificationMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationMasterRepository extends JpaRepository<NotificationMaster, Integer> {
    NotificationMaster findById(int id);
}
