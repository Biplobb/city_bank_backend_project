package com.babl.citybank.datasources.oracle.workflow.repositories;

import com.babl.citybank.common.DeferralStatus;
import com.babl.citybank.datasources.oracle.workflow.schemas.Application;
import com.babl.citybank.datasources.oracle.workflow.schemas.Deferral;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeferralRepository extends JpaRepository<Deferral, Integer> {
    List<Deferral> findByApplication(Application application);
    List<Deferral> findByApprovedByAndStatus(String approvedBy, DeferralStatus status);
    List<Deferral> findByTargetToAndStatus(String username, DeferralStatus status);
    Deferral findById(int id);
}
