package com.babl.citybank.datasources.oracle.adminPanel.repositories;


import com.babl.citybank.datasources.oracle.adminPanel.schemas.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByAuthority(String authority);
    Role findById(int id);
}
