package com.babl.citybank.datasources.oracle.customer.repositories;

import com.babl.citybank.datasources.oracle.customer.schemas.GroupHolding;
import com.babl.citybank.datasources.oracle.customer.schemas.GroupHoldingId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupHoldingRepository extends JpaRepository<GroupHolding, GroupHoldingId> {
}
