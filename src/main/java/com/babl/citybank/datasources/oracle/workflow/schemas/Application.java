package com.babl.citybank.datasources.oracle.workflow.schemas;


import com.babl.citybank.common.AppStatus;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity(name = "application")
@Data
public class Application implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "app_data", columnDefinition = "CLOB")
    private String appData;

    @Column(name = "app_title")
    private String appTitle;

    @Column(name = "app_number")
    private String appNumber;

    @Column(name = "app_status")
    @Enumerated(EnumType.STRING)
    private AppStatus appStatus;

    @Column(name = "app_init_user")
    private String appInitUser;

    @Column(name = "app_current_user")
    private String appCurrentUser;

    @Column(name = "cb_number")
    private String cbNumber;

    @Column(name = "account_number")
    private String accountNumber;

    @Column(name = "customer_name")
    private String customerName;

    @Column(name = "parent")
    private Integer parent;

    @Column(name = "auditors")
    private String auditors;

    @Column(name = "sol_id")
    private String solId;

    @Column(name = "urgency")
    private Integer urgency;

    @Column(name = "app_init_timestamp", columnDefinition = "TIMESTAMP")
    private LocalDateTime appInitTimestamp;

    @Column(name = "app_finish_timestamp", columnDefinition = "TIMESTAMP")
    private LocalDateTime appFinishTimestamp;

}
