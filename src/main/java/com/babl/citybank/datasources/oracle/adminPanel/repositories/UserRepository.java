package com.babl.citybank.datasources.oracle.adminPanel.repositories;

import com.babl.citybank.datasources.oracle.adminPanel.schemas.User;
import com.babl.citybank.datasources.oracle.master.schemas.BusinessDivisionMaster;
import com.babl.citybank.datasources.oracle.workflow.schemas.*;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUsername(String username);
    User findById(int id);
    List<User> findByUsernameLike(String text);
    List<User> findByGroupAndWorkplace(Group group, String workplace);
    List<User> findByGroupAndBranch(Group group, BranchMaster branch);
    List<User> findByGroupAndSdDepartment(Group group, SdDepartmentMaster sdDepartment);
    List<User> findByGroupAndSdDepartmentTeam(Group group, SdDepartmentTeam sdDepartmentTeam);
    List<User> findByGroupAndCsu(Group group, CSUMaster csu);
    List<User> findByGroupAndBusinessDivision(Group group, BusinessDivisionMaster businessDivision);

}
