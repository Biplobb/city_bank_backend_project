package com.babl.citybank.datasources.oracle.master.repositories;

import com.babl.citybank.datasources.oracle.master.schemas.CountryMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryMasterRepository extends JpaRepository<CountryMaster, Integer> {
    CountryMaster findById(int id);
    CountryMaster findByCountryName(String countryName);
}