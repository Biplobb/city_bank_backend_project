package com.babl.citybank.datasources.oracle.workflow.schemas;

import com.babl.citybank.common.SDRegion;
import com.babl.citybank.common.Status;
import com.babl.citybank.datasources.oracle.master.schemas.BaseAuditingEntity;
import com.babl.citybank.datasources.oracle.master.schemas.BusinessDivisionMaster;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity(name = "csu_master")
@EqualsAndHashCode(callSuper = false)
public class CSUMaster extends BaseAuditingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "region")
    @Enumerated(EnumType.STRING)
    private SDRegion region;


    @Column(name = "unit_name")
    private String unitName;

    @ManyToOne
    @JoinColumn(name = "business_division_name", referencedColumnName = "business_division_name")
    private BusinessDivisionMaster businessDivision;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
