package com.babl.citybank.datasources.oracle.master.repositories;

import com.babl.citybank.datasources.oracle.master.schemas.SubSectorMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubSectorMasterRepository extends JpaRepository<SubSectorMaster, Integer> {
    SubSectorMaster findById(int id);
    SubSectorMaster findBySubSectorNameAndSubSectorCode(String subSectorName,String subSectorCode);
}