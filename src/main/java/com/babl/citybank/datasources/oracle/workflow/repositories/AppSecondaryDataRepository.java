package com.babl.citybank.datasources.oracle.workflow.repositories;

import com.babl.citybank.datasources.oracle.workflow.schemas.AppSecondaryData;
import com.babl.citybank.datasources.oracle.workflow.schemas.Application;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AppSecondaryDataRepository extends JpaRepository<AppSecondaryData, Integer> {
    AppSecondaryData findById(int id);
    AppSecondaryData findByApplicationAndIdentifier(Application application, Integer identifier);
    AppSecondaryData findByApplicationAndDataType(Application application, String identifier);
    List<AppSecondaryData> findByApplication(Application application);

}
