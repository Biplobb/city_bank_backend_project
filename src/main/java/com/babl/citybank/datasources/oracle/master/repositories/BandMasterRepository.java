package com.babl.citybank.datasources.oracle.master.repositories;

import com.babl.citybank.datasources.oracle.master.schemas.BandMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BandMasterRepository extends JpaRepository<BandMaster, Integer> {
    BandMaster findById(int id);
    BandMaster findByBandNo(String bandNo);
}