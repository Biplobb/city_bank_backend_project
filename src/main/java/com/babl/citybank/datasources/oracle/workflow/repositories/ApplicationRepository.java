package com.babl.citybank.datasources.oracle.workflow.repositories;


import com.babl.citybank.datasources.oracle.workflow.schemas.Application;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ApplicationRepository extends JpaRepository<Application, Integer> {
    Application findById(int id);
}
