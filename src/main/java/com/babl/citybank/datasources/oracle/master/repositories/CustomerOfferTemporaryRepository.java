package com.babl.citybank.datasources.oracle.master.repositories;

import com.babl.citybank.datasources.oracle.master.schemas.CustomerOfferTemporary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerOfferTemporaryRepository extends JpaRepository<CustomerOfferTemporary,Integer> {
    CustomerOfferTemporary findById(int id);
}
