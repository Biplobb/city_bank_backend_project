package com.babl.citybank.datasources.oracle.customer.schemas;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MappingTableId implements Serializable {

    private String maincustid;

    private String szSource;
}
