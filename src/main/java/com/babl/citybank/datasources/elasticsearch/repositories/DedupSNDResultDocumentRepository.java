package com.babl.citybank.datasources.elasticsearch.repositories;

import com.babl.citybank.datasources.elasticsearch.schemas.DedupSNDResultDocument;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface DedupSNDResultDocumentRepository extends ElasticsearchRepository<DedupSNDResultDocument, String> {
}
