package com.babl.citybank.datasources.elasticsearch.documents;

import lombok.Data;

import java.util.List;

@Data
public class IndividualDedupData {
    private String customerId;

    private String cb;

    private String customerName;

    private List<SNDData> exactSndMatch;

    private List<SNDData> partialSndMatch;

    private NidVerificationData nidVerificationData;
}
