package com.babl.citybank.datasources.elasticsearch.documents;

import lombok.Data;

@Data
public class RelatedCbData {
    private String cb;

    private String customerName;

    private String relationShip;
}
