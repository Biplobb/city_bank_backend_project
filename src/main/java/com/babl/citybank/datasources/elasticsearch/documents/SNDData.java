package com.babl.citybank.datasources.elasticsearch.documents;

import lombok.Data;

@Data
public class SNDData {
    private String name;

    private String country;

    private String dob;

    private String type;

    private String otherInformation;
}
