package com.babl.citybank.datasources.elasticsearch.schemas;

import com.babl.citybank.datasources.elasticsearch.documents.IndividualDedupData;
import com.babl.citybank.datasources.elasticsearch.documents.NidVerificationData;
import com.babl.citybank.datasources.elasticsearch.documents.RelatedCbData;
import com.babl.citybank.datasources.elasticsearch.documents.SNDData;
import com.babl.citybank.modules.customer.resources.DedupResource;
import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.Column;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Document(indexName = "dedup-snd-result")
public class DedupSNDResultDocument {
    @Id
    private String id;

    private String currentTime;

    private String username;

    private String dedupType;

    private DedupResource individualDedupData;

    private DedupResource jointDedupData;

    private DedupResource companyDedupData;

}
