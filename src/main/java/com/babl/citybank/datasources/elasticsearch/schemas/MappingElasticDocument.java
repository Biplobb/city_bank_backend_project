package com.babl.citybank.datasources.elasticsearch.schemas;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.Id;
import java.io.Serializable;

@Data
@Document(indexName = "mapping-table")
public class MappingElasticDocument implements Serializable {
    @Id
    private String id;
    private String sz_full_name;
    private String sz_short_name;
    private String dt_birth;
    private String c_gender;
    private String sz_passport;
    private String email_id;
    private String sz_registration_no;
    private String sz_national_id;
    private String sz_tin_number;
    private String sz_source;
    private String sz_contitution;
    private String maincustid;
    private String sz_acct_type;
    private String szcustomerid;
    private String identification_no;
    private String cust_comu_phone_num;
}
