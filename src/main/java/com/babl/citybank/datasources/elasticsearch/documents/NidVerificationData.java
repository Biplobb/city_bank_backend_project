package com.babl.citybank.datasources.elasticsearch.documents;

import lombok.Data;

@Data
public class NidVerificationData {
    private String serviceRef;

    private String serviceTime;

    private String searchedFor;

    private boolean isVerified;

    private String nameBengali;

    private String nameEnglish;

    private String fathersName;

    private String mothersName;

    private String spouseName;

    private String presentAddress;

    private String permanentAddress;
}
