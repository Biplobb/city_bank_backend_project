package com.babl.citybank.datasources.elasticsearch.repositories;

import com.babl.citybank.datasources.elasticsearch.schemas.MappingElasticDocument;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface MappingElasticDocumentRepository extends ElasticsearchRepository<MappingElasticDocument, String> {
    MappingElasticDocument findByMaincustid(String maincustid);
}
