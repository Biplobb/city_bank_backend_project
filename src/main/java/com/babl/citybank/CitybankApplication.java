package com.babl.citybank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class CitybankApplication {

	public static void main(String[] args) {
		SpringApplication.run(CitybankApplication.class, args);
	}

}
