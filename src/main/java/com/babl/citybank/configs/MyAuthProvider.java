package com.babl.citybank.configs;

import com.babl.citybank.datasources.oracle.adminPanel.repositories.DelegationRepository;
import com.babl.citybank.datasources.oracle.adminPanel.repositories.LeaveRepository;
import com.babl.citybank.datasources.oracle.adminPanel.repositories.UserRepository;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.User;
import com.babl.citybank.modules.adminPanel.services.UserRoleService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Slf4j
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MyAuthProvider implements AuthenticationProvider {
    private final HttpServletResponse httpServletResponse;
    private final UserRoleService userRoleService;
    private final UserRepository userRepository;
    private final DelegationRepository delegationRepository;
    private final LeaveRepository leaveRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        User user = userRepository.findByUsername(username);

        if (user == null || !user.getPassword().equals(password)) {
            throw new BadCredentialsException("Credential not valid");
        }

        List<String> roles360 = userRoleService.getRoleOfUser(username);
        List<String> delegatedRoles = userRoleService.getDelegatedRole(user);

        Set<String> allRoles = new HashSet<>(roles360);
        allRoles.addAll(delegatedRoles);

        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();


        for (String role : allRoles){
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ROLE_360_" + role);
            grantedAuthorities.add(grantedAuthority);
        }

        log.info("added " + grantedAuthorities +" authorities for user "+username);
        return new UsernamePasswordAuthenticationToken(username, password, grantedAuthorities);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}