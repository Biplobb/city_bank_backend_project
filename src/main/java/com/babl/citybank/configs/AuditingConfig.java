package com.babl.citybank.configs;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Configuration
@EnableJpaAuditing
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AuditingConfig implements AuditorAware<String> {
    private final HttpServletRequest httpServletRequest;

    @Override
    public Optional<String> getCurrentAuditor() {
        return httpServletRequest.getUserPrincipal().getName() == null ?
                Optional.empty() :  Optional.of(httpServletRequest.getUserPrincipal().getName());
    }
}
