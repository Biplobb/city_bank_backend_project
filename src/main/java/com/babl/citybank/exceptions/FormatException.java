package com.babl.citybank.exceptions;

public class FormatException extends RuntimeException {
    public FormatException(String msg){
        super(msg);
    }
}
