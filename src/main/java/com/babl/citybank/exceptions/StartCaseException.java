package com.babl.citybank.exceptions;

public class StartCaseException extends RuntimeException {
    public StartCaseException(String code){
        super(code);
    }
}
