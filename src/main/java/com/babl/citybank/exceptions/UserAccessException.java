package com.babl.citybank.exceptions;

public class UserAccessException extends RuntimeException {
    public UserAccessException(String code){
        super(code);
    }
}
