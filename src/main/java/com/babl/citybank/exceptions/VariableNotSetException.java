package com.babl.citybank.exceptions;

import org.springframework.remoting.RemoteTimeoutException;

public class VariableNotSetException extends RuntimeException {
    public VariableNotSetException(String msg) {
        super(msg);
    }
}
