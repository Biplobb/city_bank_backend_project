package com.babl.citybank.exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
public class ApiError {
    private LocalDateTime timestamp;

    private int status;

    private String error;

    private String message;

    public ApiError(HttpStatus httpStatus, String errorMessage){
        this.timestamp = LocalDateTime.now();
        this.status = httpStatus.value();
        this.error = httpStatus.toString();
        this.message = errorMessage;
    }
}
