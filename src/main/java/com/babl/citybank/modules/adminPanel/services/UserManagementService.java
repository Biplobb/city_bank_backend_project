package com.babl.citybank.modules.adminPanel.services;


import com.babl.citybank.datasources.oracle.adminPanel.repositories.RoleRepository;
import com.babl.citybank.datasources.oracle.adminPanel.repositories.UserRepository;
import com.babl.citybank.datasources.oracle.adminPanel.repositories.UserRoleRepository;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.Role;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.User;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.UserRole;
import com.babl.citybank.datasources.oracle.master.repositories.BusinessDivisionMasterRepository;
import com.babl.citybank.datasources.oracle.master.schemas.BusinessDivisionMaster;
import com.babl.citybank.datasources.oracle.workflow.repositories.*;
import com.babl.citybank.datasources.oracle.workflow.schemas.*;
import com.babl.citybank.exceptions.ObjectNotFoundException;
import com.babl.citybank.modules.adminPanel.dtos.CreateEditUserDTO;
import com.babl.citybank.modules.adminPanel.resources.UserResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
@Transactional
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserManagementService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final UserRoleRepository userRoleRepository;
    private final GroupRepository groupRepository;
    private final BusinessDivisionMasterRepository businessDivisionRepository;
    private final BranchMasterRepository branchMasterRepository;
    private final SdDepartmentMasterRepository sdDepartmentMasterRepository;
    private final SdDepartmentTeamRepository sdDepartmentTeamRepository;
    private final CSUMasterRepository csuMasterRepository;


    public boolean checkIfPresent(String username){
        User user = userRepository.findByUsername(username);
        if (user == null)
            return false;
        return true;
    }

    public boolean addUser(CreateEditUserDTO createEditUserDTO){
        if (userRepository.findByUsername(createEditUserDTO.getUsername()) != null) {
            log.info("user already exists in DB");
            return false;
        }
        User user =  new User();
        user.setUsername(createEditUserDTO.getUsername());
        user.setPassword(createEditUserDTO.getPassword());
        user.setFirstName(createEditUserDTO.getFirstName());
        user.setLastName(createEditUserDTO.getLastName());
        user.setEmail(createEditUserDTO.getEmail());

        String workplace = createEditUserDTO.getWorkplace();

        if (workplace != null){
            Group group = groupRepository.findByGroupName(createEditUserDTO.getGroup());
            if (group == null)
                throw new  ObjectNotFoundException("Group not found");

            if (workplace.equals("BRANCH")){
                Optional<BranchMaster> branchMaster = branchMasterRepository.findById(createEditUserDTO.getBranch());
                if (branchMaster.isPresent()) {
                    user.setBranch(branchMaster.get());
                    user.setWorkplace(workplace);
                }
                else throw new  ObjectNotFoundException("Branch not found");
            }
            else if (workplace.equals("CSU")){
                if (group.getGroupName().equals("APPROVAL_OFFICER")){
                    Optional<BusinessDivisionMaster> businessDivisionMaster = businessDivisionRepository.findById(createEditUserDTO.getBusinessDivision());
                    if (businessDivisionMaster.isPresent()){
                        user.setBusinessDivision(businessDivisionMaster.get());
                        user.setWorkplace(workplace);
                    }
                    else throw new  ObjectNotFoundException("Business Division not found");
                }
                else {
                    Optional<CSUMaster> csuMaster = csuMasterRepository.findById(createEditUserDTO.getCsu());

                    if (csuMaster.isPresent()){
                        user.setCsu(csuMaster.get());
                        user.setWorkplace(workplace);
                    }
                    else throw new  ObjectNotFoundException("CSU not found");
                }
            }
            else if (workplace.equals("SD")){
                if (group.getGroupName().equals("ADMIN")){
                    Optional<SdDepartmentMaster> sdDepartmentMaster = sdDepartmentMasterRepository.findById(createEditUserDTO.getSdDepartment());
                    if (sdDepartmentMaster.isPresent())
                        user.setSdDepartment(sdDepartmentMaster.get());
                    else throw new  ObjectNotFoundException("SD Department not found");
                }
                else {
                    Optional<SdDepartmentTeam> sdDepartmentTeam = sdDepartmentTeamRepository.findById(createEditUserDTO.getSdDepartmentTeam());
                    if (sdDepartmentTeam.isPresent())
                        user.setSdDepartmentTeam(sdDepartmentTeam.get());
                    else throw new  ObjectNotFoundException("SD Team not found");
                }
                user.setWorkplace(workplace);
            }
            user.setGroup(group);
        }

        if (createEditUserDTO.getRoleId() != null){
            Optional<Role> role = roleRepository.findById(createEditUserDTO.getRoleId());
            if (role.isPresent()){
                UserRole userRole = new UserRole();
                userRole.setRoleId(createEditUserDTO.getRoleId());
                userRole.setUsername(createEditUserDTO.getUsername());
                userRoleRepository.save(userRole);
                log.info("role is assigned to user");
            }
            else {
                log.info("Invalid role id.");
                throw new  ObjectNotFoundException("Invalid role id.");
            }
        }

        userRepository.save(user);

        log.info("successfully added user with username " + createEditUserDTO.getUsername());
        return true;
    }

    public boolean editUser(CreateEditUserDTO createEditUserDTO){
        User user = userRepository.findByUsername(createEditUserDTO.getUsername());
        if (user == null) {
            log.info("user does not exists");
            return false;
        }
        user.setFirstName(createEditUserDTO.getFirstName());
        user.setLastName(createEditUserDTO.getLastName());
        user.setEmail(createEditUserDTO.getEmail());
        userRepository.save(user);
        log.info("successfully edited user with username " + createEditUserDTO.getUsername());
        return true;
    }

    public boolean deleteUser(int userId){
        User user = userRepository.findById(userId);
        if (user == null){
            log.info("user does not exists");
            return false;
        }

        userRepository.delete(user);
        log.info("successfully deleted user with user id " + userId);
        return true;
    }


    public Page<User> getAllUser(Pageable pageable){
        Page<User> userPage = userRepository.findAll(pageable);
        return userPage;

    }

    public List<User> getAllUser(){
        //return

        List<User> response = new ArrayList<>();
        List<User> userList = userRepository.findAll();

        for (User user : userList){

            User usr = new User();
            usr.setId(user.getId());
            usr.setUsername(user.getUsername());
            usr.setEmail(user.getEmail());
            usr.setFirstName(user.getFirstName());
            usr.setLastName(user.getLastName());
            response.add(usr);
        }

        return response;

    }


    public List<User> searchUser(String username){

        List<User> userList =  userRepository.findByUsernameLike(username + "%");

        List<User> response = new ArrayList<>();

        for (User user : userList){

            User usr = new User();
            usr.setId(user.getId());
            usr.setUsername(user.getUsername());
            usr.setEmail(user.getEmail());
            usr.setFirstName(user.getFirstName());
            usr.setLastName(user.getLastName());
            response.add(usr);
        }

        return response;

    }



}
