package com.babl.citybank.modules.adminPanel.services;



import com.babl.citybank.datasources.oracle.adminPanel.repositories.*;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.*;
import com.babl.citybank.exceptions.ObjectNotFoundException;
import com.babl.citybank.modules.adminPanel.dtos.RoleMenuAddUpdateDTO;
import com.babl.citybank.modules.adminPanel.resources.MenuResource;
import com.babl.citybank.modules.adminPanel.resources.RoleAndMenuOfUserResource;
import com.babl.citybank.modules.adminPanel.resources.RoleMenuResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RoleMenuService {
    private final RoleMenuRepository roleMenuRepository;
    private final UserRoleRepository userRoleRepository;
    private final RoleRepository roleRepository;
    private final MenuRepository menuRepository;
    private final UserRoleService userRoleService;
    private final UserRepository userRepository;

    private final HttpServletRequest httpServletRequest;
    private final HttpServletResponse httpServletResponse;


    public boolean addRoleMenu(RoleMenuAddUpdateDTO roleMenuAddUpdateDTO) {
        RoleMenu roleMenu = roleMenuRepository.findByRoleIdAndMenuId(roleMenuAddUpdateDTO.getRoleId(), roleMenuAddUpdateDTO.getMenuId());

        if (roleMenu != null){
            log.info("role is already assigned to this menu");
            return false;
        }

        Menu menu = menuRepository.findById(roleMenuAddUpdateDTO.getMenuId());
        if (menu == null){
            log.info("Menu is not valid");
            return false;
        }

        if (menu.getRgt() - menu.getLft() > 1){
            log.info("Parent menu can not be assigned");
            return false;
        }

        roleMenu = new RoleMenu();
        roleMenu.setRoleId(roleMenuAddUpdateDTO.getRoleId());
        roleMenu.setMenuId(roleMenuAddUpdateDTO.getMenuId());
        roleMenuRepository.save(roleMenu);
        log.info("RoleMenu added successfully");
        return true;
    }

    public void addRoleMenuAll(List<RoleMenuAddUpdateDTO> roleMenuAddUpdateDTOS){
        for (RoleMenuAddUpdateDTO roleMenuAddUpdateDTO : roleMenuAddUpdateDTOS)
            addRoleMenu(roleMenuAddUpdateDTO);
    }

    public boolean updateRoleMenu(RoleMenuAddUpdateDTO roleMenuAddUpdateDTO, int id) {
        RoleMenu roleMenu = roleMenuRepository.findById(id);
        if (roleMenu == null) {
            log.info("RoleMenu not found with id = " + id);
            return false;
        }

        Menu menu = menuRepository.findById(roleMenuAddUpdateDTO.getMenuId());
        if (menu == null){
            log.info("Menu is not valid");
            return false;
        }

        if (menu.getRgt() - menu.getLft() > 1){
            log.info("Parent menu can not be assigned");
            return false;
        }

        RoleMenu ifExist = roleMenuRepository.findByRoleIdAndMenuId(roleMenuAddUpdateDTO.getRoleId(), roleMenuAddUpdateDTO.getMenuId());

        if (ifExist != null){
            log.info("role menu already exist");
            return false;
        }

        roleMenu.setRoleId(roleMenuAddUpdateDTO.getRoleId());
        roleMenu.setMenuId(roleMenuAddUpdateDTO.getMenuId());
        roleMenuRepository.save(roleMenu);
        log.info("RoleMenu updated successfully with id = " + id);
        return true;
    }

    public boolean deleteRoleMenu(int id) {
        RoleMenu roleMenu = roleMenuRepository.findById(id);
        if (roleMenu == null) {
            log.info("RoleMenu not found with id = " + id);
            return false;
        }
        roleMenuRepository.delete(roleMenu);
        log.info("RoleMenu deleted successfully with id = " + id);
        return true;
    }

    public void deleteRoleMenuBulk(List<Integer> roleMenuIdList){
        for (int id : roleMenuIdList){
            boolean status = deleteRoleMenu(id);
            log.info("delete role menu status : " + status);
        }
    }

    public RoleMenuResource getRoleMenuResource(int id) {
        RoleMenu roleMenu = roleMenuRepository.findById(id);
        if (roleMenu == null) {
            log.info("RoleMenu not found with id = " + id);
            return null;
        }
        RoleMenuResource roleMenuResource = new RoleMenuResource();
        roleMenuResource.setId(roleMenu.getId());
        roleMenuResource.setRoleId(roleMenu.getRoleId());
        roleMenuResource.setMenuId(roleMenu.getMenuId());
        log.info("RoleMenu returned successfully with id = " + id);
        return roleMenuResource;
    }

    /*

    public List<MenuResource> getMenuResourcesOfUser(String applicationName, String username){
        Set<Menu> menus = new HashSet<>();
        List<MenuResource> menuResources = new ArrayList<>();

        List<UserRole> userRoleList = userRoleRepository.findByUsername(username);
        if (userRoleList == null) {
            return menuResources;
        }

        Application application = applicationRepository.findByApplicationName(applicationName);
        if (application == null){
            log.info("application not found");
            return null;
        }

        for (UserRole userRole : userRoleList){
            Role role = roleRepository.getOne(userRole.getRoleId());
            if (role.getApplicationId().equals(application.getId())){
                List<RoleMenu> roleMenuList = roleMenuRepository.findByRoleId(role.getId());
                for (RoleMenu roleMenu : roleMenuList) {
                    Menu menu = menuRepository.getOne(roleMenu.getMenuId());

                    menus.add(menu);
                }
            }

        }

        for (Menu menu : menus){
            MenuResource menuResource = new MenuResource();
            menuResource.setId(menu.getId());
            menuResource.setName(menu.getName());
            menuResource.setLeft(menu.getLft());
            menuResource.setDepth(menuRepository.getDepth(menu.getId()));
            menuResources.add(menuResource);
        }

        Collections.sort(menuResources, new Comparator<MenuResource>() {
            @Override
            public int compare(MenuResource m1, MenuResource m2) {
                return m1.getLeft().compareTo(m2.getLeft());
            }
        });


        return menuResources;
    }

    */



    private void populateMenuResource(int id, int depth, Set<Integer> menuIdSet, List<MenuResource> menuResourceList){
        if (menuIdSet.contains(id)) {
            depth = depth + 1;

            Menu menu = menuRepository.getOne(id);
            MenuResource menuResource = new MenuResource();

            menuResource.setId(id);
            menuResource.setName(menu.getName());
            menuResource.setLeft(menu.getLft());
            menuResource.setDepth(depth);

            menuResourceList.add(menuResource);

        }

        List<Menu> childList = menuRepository.findByParentId(id);

        for (Menu menu : childList){
            populateMenuResource(menu.getId(), depth, menuIdSet, menuResourceList);
        }

    }


    public List<MenuResource> getMenuResourcesOfUser(String username){
        List<MenuResource> menuResourceList = new ArrayList<>();
        Set<Integer> menuIdSet = new HashSet<>();

        List<UserRole> userRoleList = userRoleRepository.findByUsername(username);
        if (userRoleList == null) {
            return new ArrayList<>();
        }


        for (UserRole userRole : userRoleList){
            List<RoleMenu> menuList = roleMenuRepository.findByRoleId(userRole.getRoleId());
            for (RoleMenu roleMenu : menuList){
                menuIdSet.add(roleMenu.getMenuId());
            }
        }

        Menu menu = menuRepository.findByName("root");

        if (menu == null) {
            log.info("Application not found");
            return null;
        }

        populateMenuResource(menu.getId(), 0, menuIdSet, menuResourceList);

        return menuResourceList;
    }

    public RoleAndMenuOfUserResource getRoleMenuOfUser(String username){
        User user = userRepository.findByUsername(username);

        if (user == null)
            throw new ObjectNotFoundException("user not found");

        String cookies = httpServletRequest.getHeader("cookie");
        String[] cookieArray = cookies.split(";");
        for (String cookie : cookieArray){
            String[] cookieKeyVal = cookie.split("=");
            if (cookieKeyVal[0].trim().equals("JSESSIONID")) {
                httpServletResponse.addHeader("spring_session", cookieKeyVal[1].trim());
                httpServletResponse.addHeader("Access-Control-Expose-Headers", "spring_session");
            }
        }

        List<UserRole> userRoleList = userRoleRepository.findByUsername(username);
        if (userRoleList == null)
            return null;


        List<String> authorities = new ArrayList<>();
        List<String> menus = new ArrayList<>();
        Set<Integer> menuIdSet = new HashSet<>();
        Set<Integer> parentMenuIdSet = new HashSet<>();

        for (UserRole userRole : userRoleList){
            Role role = roleRepository.getOne(userRole.getRoleId());
            authorities.add(role.getAuthority());
        }

        for (UserRole userRole : userRoleList){
            List<RoleMenu> menuList = roleMenuRepository.findByRoleId(userRole.getRoleId());
            for (RoleMenu roleMenu : menuList){
                menuIdSet.add(roleMenu.getMenuId());
            }
        }

        List<Menu> allMenus = menuRepository.findAll();

//        for (Integer menuId : menuIdSet){
//            Menu menu = menuRepository.getOne(menuId);
//            menus.add(menu.getName());
//        }

        for (Menu menu : allMenus){
            if (menuIdSet.contains(menu.getId())){
                menus.add(menu.getName());
                parentMenuIdSet.add(menu.getParentId());
            }
        }

        for (Menu menu : allMenus){
            if (parentMenuIdSet.contains(menu.getId())){
                menus.add(menu.getName());
            }
        }

        RoleAndMenuOfUserResource resource = new RoleAndMenuOfUserResource();
        resource.setRoles(authorities);
        resource.setMenus(menus);
        resource.setWorkplace(user.getWorkplace());

        return resource;

    }

}