package com.babl.citybank.modules.adminPanel.services;

import com.babl.citybank.datasources.oracle.adminPanel.repositories.UserRepository;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.Leave;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.User;
import com.babl.citybank.datasources.oracle.adminPanel.repositories.LeaveRepository;
import com.babl.citybank.exceptions.ObjectNotFoundException;
import com.babl.citybank.lib.DatePickerFormatToLocaldate;
import com.babl.citybank.modules.adminPanel.dtos.AssignLeaveDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LeaveService {
    private final UserRepository userRepository;
    private final LeaveRepository leaveRepository;

    public boolean assignLeave(AssignLeaveDTO assignLeaveDTO){
        User user = userRepository.findByUsername(assignLeaveDTO.getUsername());

        if (user == null)
            throw new ObjectNotFoundException("User not found");

        Leave leave = new Leave();
        leave.setUser(user);
        leave.setStartDate(DatePickerFormatToLocaldate.convert(assignLeaveDTO.getStartDate()));
        leave.setEndDate(DatePickerFormatToLocaldate.convert(assignLeaveDTO.getEndDate()));

        leaveRepository.save(leave);

        return true;
    }

    public boolean isOnLeave(User user){
        LocalDate now = LocalDate.now();
        List<Leave> leaves = leaveRepository
                .findByUserAndStartDateLessThanEqualAndEndDateGreaterThanEqual(user, now, now);
        if (leaves.size() > 0)
            return true;
        return false;
    }


}
