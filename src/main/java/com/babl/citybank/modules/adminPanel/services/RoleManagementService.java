package com.babl.citybank.modules.adminPanel.services;


import com.babl.citybank.datasources.oracle.adminPanel.repositories.RoleRepository;
import com.babl.citybank.datasources.oracle.adminPanel.repositories.UserRoleRepository;
import com.babl.citybank.modules.adminPanel.resources.RoleResource;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.Role;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.UserRole;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Transactional
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RoleManagementService {
    private final RoleRepository roleRepository;
    private final UserRoleRepository userRoleRepository;

    public boolean addRole(String authority){

        Role role = roleRepository.findByAuthority(authority);

        if (role != null){
            log.info("role already exists");
            return false;
        }

        role = new Role();
        role.setAuthority(authority);
        roleRepository.save(role);
        log.info("role added successfully");
        return true;
    }

    public boolean deleteRole(int roleId){
        Role role = roleRepository.findById(roleId);
        if (role == null){
            log.info("role not found");
            return false;
        }

        List<UserRole> userRoles = userRoleRepository.findByRoleId(roleId);

        if (userRoles.size() == 0) {
            roleRepository.delete(role);
            log.info("role deleted successfully");
            return true;
        }

        log.info("cannot delete role: users exist in this role");
        return false;
    }

    public boolean updateRole(int roleId, String authority){

        Role role = roleRepository.findById(roleId);
        if (role == null){
            log.info("role not found");
            return false;
        }

        Role newRole = roleRepository.findByAuthority(authority);
        if (newRole != null){
            log.info("role already exists");
            return  false;
        }

        role.setAuthority(authority);
        roleRepository.save(role);
        log.info("role updated successfully");
        return true;
    }

    public RoleResource getRole(int roleId){
        Role role = roleRepository.findById(roleId);
        if (role == null){
            log.info("role not found");
            return null;
        }

        return new RoleResource(roleId, role.getAuthority());
    }

    public List<Role> getAll(){
        return roleRepository.findAll();
    }

}
