package com.babl.citybank.modules.adminPanel.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateEditUserDTO {
    @NotBlank
    private String username;

    @NotBlank
    private String password;

    private String firstName;

    private String lastName;

    private String email;

    private Integer roleId;

    private String workplace;

    private String group;

    private Integer branch;

    private Integer sdDepartment;

    private Integer sdDepartmentTeam;

    private Integer csu;

    private Integer businessDivision;
}
