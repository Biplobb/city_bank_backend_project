package com.babl.citybank.modules.adminPanel.dtos;

import lombok.Data;

import java.time.LocalDate;

@Data
public class AssignLeaveDTO {
    private String username;
    private String startDate;
    private String endDate;
}
