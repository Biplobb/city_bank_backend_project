package com.babl.citybank.modules.adminPanel.resources;

import lombok.Data;

import java.util.List;

@Data
public class UserResource {
    private int id;

    private String username;

    private String firstName;

    private String lastName;

    private String email;

    private List<String> roles;
}
