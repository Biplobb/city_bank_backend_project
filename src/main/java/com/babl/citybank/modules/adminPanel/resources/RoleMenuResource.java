package com.babl.citybank.modules.adminPanel.resources;

import lombok.Data;

@Data
public class RoleMenuResource {
    private Integer id;

    private Integer roleId;

    private Integer menuId;

}