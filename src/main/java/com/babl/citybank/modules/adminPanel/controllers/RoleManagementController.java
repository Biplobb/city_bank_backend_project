package com.babl.citybank.modules.adminPanel.controllers;


import com.babl.citybank.modules.adminPanel.resources.RoleResource;
import com.babl.citybank.modules.adminPanel.resources.StatusResource;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.Role;
import com.babl.citybank.modules.adminPanel.services.RoleManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RoleManagementController {
    private final RoleManagementService roleManagementService;

    @RequestMapping(value = "/role/add/{roleName}", method = RequestMethod.POST)
    public StatusResource addRole(@PathVariable("roleName")  final String roleName){
        boolean status = roleManagementService.addRole(roleName);
        StatusResource statusResource = new StatusResource("SUCCESSFUL", "");
        if (!status)
            statusResource.setStatus("UNSUCCESSFUL");
        return statusResource;
    }

    @RequestMapping(value = "role/update/{roleId}/{roleName}", method = RequestMethod.POST)
    public StatusResource updateRole(@PathVariable("roleId") final int roleId,
                                     @PathVariable("roleName") final String roleName){
        boolean status = roleManagementService.updateRole(roleId, roleName);
        StatusResource statusResource = new StatusResource("SUCCESSFUL", "");
        if (!status)
            statusResource.setStatus("UNSUCCESSFUL");
        return statusResource;
    }

    @RequestMapping(value = "role/delete/{roleId}", method = RequestMethod.POST)
    public StatusResource deleteRole(@PathVariable("roleId") final int roleId){
        boolean status = roleManagementService.deleteRole(roleId);
        StatusResource statusResource = new StatusResource("SUCCESSFUL", "");
        if (!status)
            statusResource.setStatus("UNSUCCESSFUL");
        return statusResource;
    }

    @RequestMapping(value = "role/getById/{roleId}", method = RequestMethod.GET)
    public RoleResource getRoleResource(@PathVariable("roleId") final int roleId){
        return roleManagementService.getRole(roleId);
    }

    @RequestMapping(value = "role/get", method = RequestMethod.GET)
    public List<Role> getApplicationRole(){
        return roleManagementService.getAll();
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test(){
        return "ok";
    }

}
