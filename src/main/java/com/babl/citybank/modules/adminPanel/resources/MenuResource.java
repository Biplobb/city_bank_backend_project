package com.babl.citybank.modules.adminPanel.resources;

import lombok.Data;

@Data
public class MenuResource {
    private int id;

    private String name;

    private Integer left;

    private int depth;
}
