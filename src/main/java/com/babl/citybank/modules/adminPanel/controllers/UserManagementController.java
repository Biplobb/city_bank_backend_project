package com.babl.citybank.modules.adminPanel.controllers;


import com.babl.citybank.datasources.oracle.adminPanel.repositories.UserRepository;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.User;
import com.babl.citybank.modules.adminPanel.dtos.CreateEditUserDTO;
import com.babl.citybank.modules.adminPanel.resources.UserResource;
import com.babl.citybank.modules.adminPanel.services.UserManagementService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserManagementController {
    private final UserManagementService userManagementService;
    private final UserRepository userRepository;

    @RequestMapping(path = "/user/add", method = RequestMethod.POST)
    public boolean addUser(@RequestBody @Validated final CreateEditUserDTO addUserDTO){
        log.info("received request to add user");
        return userManagementService.addUser(addUserDTO);
    }

    @RequestMapping(path = "/user/edit", method = RequestMethod.POST)
    public boolean editUser(@RequestBody @Validated final CreateEditUserDTO createEditUserDTO){
        return userManagementService.editUser(createEditUserDTO);
    }

    @RequestMapping(path = "/user/delete/{id}", method = RequestMethod.DELETE)
    public boolean deleteUser(@PathVariable("id") final int id){
        return userManagementService.deleteUser(id);
    }

    @RequestMapping(path = "/user/get/all", method = RequestMethod.GET)
    public Object getAll(){
        //Pageable pageable = PageRequest.of(pageNum, 25);
        return userManagementService.getAllUser();
    }

    @RequestMapping(path = "/user/get/{username}", method = RequestMethod.GET)
    public User findByUsername(@PathVariable("username") final String username){
        User user = userRepository.findByUsername(username);

        User usr = new User();
        usr.setId(user.getId());
        usr.setUsername(user.getUsername());
        usr.setEmail(user.getEmail());
        usr.setFirstName(user.getFirstName());
        usr.setLastName(user.getLastName());
        return usr;
    }

    @RequestMapping(path = "/user/search/{text}", method = RequestMethod.GET)
    public Object searchUser(@PathVariable("text") String text){
        return userManagementService.searchUser(text);
    }


}
