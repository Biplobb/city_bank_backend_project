package com.babl.citybank.modules.adminPanel.dtos;

import lombok.Data;

@Data
public class RoleMenuAddUpdateDTO {
    private int roleId;

    private int menuId;

}