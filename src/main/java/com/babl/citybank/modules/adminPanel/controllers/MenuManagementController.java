package com.babl.citybank.modules.adminPanel.controllers;


import com.babl.citybank.modules.adminPanel.dtos.MenuAddUpdateDTO;
import com.babl.citybank.modules.adminPanel.resources.MenuResource;
import com.babl.citybank.modules.adminPanel.resources.ReactSortableTreeMenuResource;
import com.babl.citybank.modules.adminPanel.services.MenuManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MenuManagementController {
    private final MenuManagementService menuManagementService;

    @RequestMapping(value = "menu/add", method = RequestMethod.POST)
    public boolean addMenu(@RequestBody @Validated final MenuAddUpdateDTO menuAddUpdateDTO){
        return menuManagementService.addMenu(menuAddUpdateDTO);
    }

    @RequestMapping(value = "menu/getSub/{menuName}", method = RequestMethod.GET)
    public List<MenuResource> getSubMenu(@PathVariable("menuName") final String menuName){
        return menuManagementService.getSubMenu(menuName);
    }

    @RequestMapping(value = "menu/getApplicationSub", method = RequestMethod.GET)
    public List<MenuResource> getApplicationMenues(){
        return menuManagementService.getSubMenu("root");
    }

    @RequestMapping(value = "menu/reactSortableMenu", method = RequestMethod.GET)
    public ReactSortableTreeMenuResource getReactSortableMenu(){
        return menuManagementService.allReactSortableMenu("root");
    }

    @RequestMapping(value = "menu/availableToAssign/{roleId}", method = RequestMethod.GET)
    public List<?> getAvailableToAssignMenu(@PathVariable("roleId") final int roleId){
        return menuManagementService.availableToAssignRole(roleId);
    }

    @RequestMapping(value = "menu/assignedMenu/{roleId}", method = RequestMethod.GET)
    public List<?> getAssignedMenu(@PathVariable("roleId") final int roleId){
        return menuManagementService.assignedMenuToRole(roleId);
    }

    @RequestMapping(value = "menu/get/{id}", method = RequestMethod.GET)
    public MenuResource getMenuResource(@PathVariable("id") final int id){
        return menuManagementService.getMenu(id);
    }

    @RequestMapping(value = "menu/delete/{id}", method = RequestMethod.POST)
    public boolean deleteMenu(@PathVariable("id") final int id){
        return menuManagementService.deleteMenu(id);
    }
}
