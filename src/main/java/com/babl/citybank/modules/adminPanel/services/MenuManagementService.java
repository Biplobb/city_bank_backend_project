package com.babl.citybank.modules.adminPanel.services;


import com.babl.citybank.modules.adminPanel.dtos.MenuAddUpdateDTO;
import com.babl.citybank.datasources.oracle.adminPanel.repositories.MenuRepository;
import com.babl.citybank.datasources.oracle.adminPanel.repositories.RoleMenuRepository;
import com.babl.citybank.modules.adminPanel.resources.MenuResource;
import com.babl.citybank.modules.adminPanel.resources.ReactSortableTreeMenuResource;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.Menu;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.RoleMenu;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.*;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MenuManagementService {
    private final MenuRepository menuRepository;
    private final RoleMenuRepository roleMenuRepository;


    @PersistenceContext
    private EntityManager entityManager;


    @Transactional
    public boolean addMenu(MenuAddUpdateDTO menuAddUpdateDTO){
        int id = menuAddUpdateDTO.getId();
        String menuName = menuAddUpdateDTO.getName();
        String position = menuAddUpdateDTO.getPosition();

        String wholeTable = "select id from menu";
        String getRightPos = "select u from menu u where id = :menuId";
        //String addQuery = "insert into application(application_name) values('amar astha')";
        String updateRight = "update menu set rgt = rgt + 2 where rgt > :myRight";
        String updateLeft = "update menu set lft = lft + 2 where lft > :myRight";
        String addMenu = "INSERT INTO menu(name, lft, rgt, parent_id) VALUES(:menuName, :myRight + 1, :myRight + 2, :parentId)";

        try {
            TypedQuery<Integer> typedQuery = entityManager.createQuery(wholeTable, Integer.class);
            typedQuery.setLockMode(LockModeType.WRITE);
            typedQuery.getResultList();


            TypedQuery<Menu> query = entityManager.createQuery(getRightPos, Menu.class);
            //query.setLockMode(LockModeType.WRITE);
            query.setParameter("menuId", id);

            Menu menu = query.getSingleResult();

            Integer right;
            Integer parentId;

            if (position.equals("CHILD")) {
                right = menu.getLft();
                parentId = menu.getId();
            }
            else if (position.equals("RIGHT")) {
                right = menu.getRgt();
                parentId = menu.getParentId();
            }
            else
                return false;


            Query nativeQuery = entityManager.createNativeQuery(updateRight);
            nativeQuery.setParameter("myRight", right);
            nativeQuery.executeUpdate();

            nativeQuery = entityManager.createNativeQuery(updateLeft);
            nativeQuery.setParameter("myRight", right);
            nativeQuery.executeUpdate();


            nativeQuery = entityManager.createNativeQuery(addMenu);
            nativeQuery.setParameter("myRight", right);
            nativeQuery.setParameter("menuName", menuName);
            nativeQuery.setParameter("parentId", parentId);
            nativeQuery.executeUpdate();


            return true;

        }
        catch (Exception e){
            System.out.println(e);
        }
        return false;
    }

    @Transactional
    public boolean deleteMenu(int id){
        String wholeTable = "select id from menu";
        String menuToDelete = "SELECT u FROM menu u WHERE id = :menuId";
        String deleteQuery = "Delete from menu where id = :menuId";
        String updateChild = "UPDATE menu SET rgt = rgt - 1, lft = lft - 1 WHERE lft BETWEEN :deleteLeft AND :deleteRight";
        String updateRight = "UPDATE menu SET rgt = rgt - 2 WHERE rgt > :deleteRight";
        String updateLeft = "UPDATE menu SET lft = lft - 2 WHERE lft > :deleteRight";


        try {
            TypedQuery<Integer> typedQuery = entityManager.createQuery(wholeTable, Integer.class);
            typedQuery.setLockMode(LockModeType.WRITE);
            typedQuery.getResultList();


            TypedQuery<Menu> query = entityManager.createQuery(menuToDelete, Menu.class);
            //query.setLockMode(LockModeType.WRITE);
            query.setParameter("menuId", id);

            Menu menu = query.getSingleResult();

            menuRepository.delete(menu);

            Query nativeQuery;

            nativeQuery = entityManager.createNativeQuery(updateChild);
            nativeQuery.setParameter("deleteLeft", menu.getLft());
            nativeQuery.setParameter("deleteRight", menu.getRgt());
            nativeQuery.executeUpdate();

            nativeQuery = entityManager.createNativeQuery(updateRight);
            nativeQuery.setParameter("deleteRight", menu.getRgt());
            nativeQuery.executeUpdate();

            nativeQuery = entityManager.createNativeQuery(updateLeft);
            nativeQuery.setParameter("deleteRight", menu.getRgt());
            nativeQuery.executeUpdate();

            return true;

        }
        catch (Exception e){
            e.printStackTrace();
        }
        return false;

    }


    @Transactional
    public MenuResource getMenu(int id){
        MenuResource menuResource = new MenuResource();

        Menu menu = menuRepository.getOne(id);
        menuResource.setDepth(-1);
        menuResource.setId(menu.getId());
        menuResource.setName(menu.getName());
        return menuResource;
    }


    /*
    @Transactional
    public MenuGridResource getApplicationMenu(String applicationName){
        Menu menu = menuRepository.getByName(applicationName);

        if (menu == null) {
            log.info("Application not found");
            return null;
        }

        return getSubMenu(menu.getId());
    }


    @Transactional
    public MenuGridResource getSubMenu(int id){
        List<Menu> childs = menuRepository.findByParentId(id);

        MenuGridResource menuTree = new MenuGridResource();

        Menu root = menuRepository.getOne(id);
        menuTree.setId(id);
        menuTree.setName(root.getName());
        menuTree.setLeft(root.getLft());


        List<MenuGridResource> menuGridResourceList = new ArrayList<>();

        for (Menu menu : childs){
            MenuGridResource childTree = getSubMenu(menu.getId());
            menuGridResourceList.add(childTree);
        }

        Collections.sort(menuGridResourceList, new Comparator<MenuGridResource>() {
            @Override
            public int compare(MenuGridResource m1, MenuGridResource m2) {
                return m1.getLeft().compareTo(m2.getLeft());
            }
        });

        menuTree.setChild(menuGridResourceList);

        return menuTree;
    }*/


    @Transactional
    public List<MenuResource> getSubMenu(String menuName){
        Menu root = menuRepository.findByName(menuName);

        if (root == null){
            log.info("Root Menu not found");
            return new ArrayList<>();
        }

        List<Menu> menues = menuRepository.getMenues(root.getId());

        List<MenuResource> menuResources = new ArrayList<>();

        MenuResource rootMenuResource = new MenuResource();
        rootMenuResource.setDepth(0);
        rootMenuResource.setId(root.getId());
        rootMenuResource.setLeft(root.getLft());
        rootMenuResource.setName(root.getName());

        menuResources.add(rootMenuResource);

        for (Menu menu : menues){
            MenuResource menuResource = new MenuResource();
            menuResource.setId(menu.getId());
            menuResource.setName(menu.getName());
            menuResource.setDepth(menuRepository.getDepth(menu.getId()));
            menuResource.setLeft(menu.getLft());
            menuResources.add(menuResource);
        }

        Collections.sort(menuResources, new Comparator<MenuResource>() {
            @Override
            public int compare(MenuResource m1, MenuResource m2) {
                return m1.getLeft().compareTo(m2.getLeft());
            }
        });

        return menuResources;
    }


    @Transactional
    public ReactSortableTreeMenuResource allReactSortableMenu(String menuName){
        Menu rootMenu = menuRepository.findByName(menuName);

        if (rootMenu == null) {
            log.info("Menu not found");
            return null;
        }

        List<Menu> childs = menuRepository.findByParentId(rootMenu.getId());

        ReactSortableTreeMenuResource treeMenuResource = new ReactSortableTreeMenuResource();

        treeMenuResource.setTitle(rootMenu.getName());
        treeMenuResource.setMenuId(rootMenu.getId());
        treeMenuResource.setLeft(rootMenu.getLft());


        List<ReactSortableTreeMenuResource> treeMenuResourceList = new ArrayList<>();

        for (Menu menu : childs){
            ReactSortableTreeMenuResource childTree = allReactSortableMenu(menu.getName());
            treeMenuResourceList.add(childTree);
        }

        Collections.sort(treeMenuResourceList, new Comparator<ReactSortableTreeMenuResource>() {
            @Override
            public int compare(ReactSortableTreeMenuResource m1, ReactSortableTreeMenuResource m2) {
                return m1.getLeft().compareTo(m2.getLeft());
            }
        });

        treeMenuResource.setChildren(treeMenuResourceList);

        return treeMenuResource;

    }



    public List<?> availableToAssignRole(int roleId){
        String searchQuery = "select id, name, lft, parent_id, rgt from menu where id not in ";
        searchQuery = searchQuery + " (select distinct menu_id from role_menu where role_id = :roleId) ";

        Query query = entityManager.createNativeQuery(searchQuery);

        query.setParameter("roleId", roleId);

        List<?> resultList = query.getResultList();

        List<MenuResource> menuResourceList = new ArrayList<>();
        Set<Integer> parentSet = new HashSet<>();


        if (resultList.size() > 0){
            for (int i=0; i<resultList.size(); i++){
                Object[] object = (Object[]) resultList.get(i);

                int left = ((BigDecimal)object[2]).intValue();
                int right = ((BigDecimal)object[4]).intValue();

                if (right == left + 1) {
                    parentSet.add(((BigDecimal) object[3]).intValue());

                    MenuResource menuResource = new MenuResource();
                    menuResource.setId(((BigDecimal) object[0]).intValue());
                    menuResource.setName((String) object[1]);
                    menuResource.setLeft(((BigDecimal) object[2]).intValue());
                    menuResource.setDepth(menuRepository.getDepth(((BigDecimal) object[0]).intValue()));

                    menuResourceList.add(menuResource);
                }
            }

            for (int i=0; i<resultList.size(); i++){
                Object[] object = (Object[]) resultList.get(i);

                int menuId = ((BigDecimal) object[0]).intValue();

                if (parentSet.contains(menuId)){
                    MenuResource menuResource = new MenuResource();
                    menuResource.setId(((BigDecimal) object[0]).intValue());
                    menuResource.setName((String) object[1]);
                    menuResource.setLeft(((BigDecimal) object[2]).intValue());
                    menuResource.setDepth(menuRepository.getDepth(((BigDecimal) object[0]).intValue()));

                    menuResourceList.add(menuResource);
                }
            }
        }

        Collections.sort(menuResourceList, new Comparator<MenuResource>() {
            @Override
            public int compare(MenuResource m1, MenuResource m2) {
                return m1.getLeft().compareTo(m2.getLeft());
            }
        });

        return menuResourceList;

    }

    @Transactional
    public List<MenuResource> assignedMenuToRole(int roleId){
        /*String searchQuery = "select  menu.id as menu_id, menu.name, menu.lft, role_menu.id as role_menu_id from role_menu, menu ";
        searchQuery = searchQuery + " where role_menu.menu_id = menu.id and role_menu.role_id = :roleId ";

        Query query = entityManager.createNativeQuery(searchQuery);

        query.setParameter("roleId", roleId);

        List<?> resultList = query.getResultList();

        List<MenuResource> menuResourceList = new ArrayList<>();

        if (resultList.size() > 0){
            for (int i=0; i<resultList.size(); i++){
                Object[] object = (Object[]) resultList.get(i);

                MenuResource menuResource = new MenuResource();
                menuResource.setId((int)object[3]);             ///role menu id
                menuResource.setName((String) object[1]);
                menuResource.setLeft((int)object[2]);
                menuResource.setDepth(menuRepository.getDepth((int)object[0]));

                menuResourceList.add(menuResource);
            }
        }*/

        List<Menu> allMenus = menuRepository.findAll();
        List<RoleMenu> roleMenus = roleMenuRepository.findByRoleId(roleId);
        Map<Integer, Integer> menuMap = new HashMap<>();
        for (RoleMenu roleMenu : roleMenus){
            menuMap.put(roleMenu.getMenuId(), roleMenu.getId());
        }

        List<MenuResource> menuResourceList = new ArrayList<>();
        Set<Integer> parentsMenuSet = new HashSet<>();

        for (Menu menu : allMenus){
            if (menuMap.containsKey(menu.getId())){
                parentsMenuSet.add(menu.getParentId());

                MenuResource menuResource = new MenuResource();
                menuResource.setId(menuMap.get(menu.getId()));          ///role menu id
                menuResource.setName(menu.getName());
                menuResource.setLeft(menu.getLft());
                menuResource.setDepth(menuRepository.getDepth(menu.getId()));

                menuResourceList.add(menuResource);
            }
        }

        for (Menu menu : allMenus){
            if (parentsMenuSet.contains(menu.getId())){
                MenuResource menuResource = new MenuResource();
                menuResource.setId(menu.getId());
                menuResource.setName(menu.getName());
                menuResource.setLeft(menu.getLft());
                menuResource.setDepth(menuRepository.getDepth(menu.getId()));

                menuResourceList.add(menuResource);
            }
        }

        Collections.sort(menuResourceList, new Comparator<MenuResource>() {
            @Override
            public int compare(MenuResource m1, MenuResource m2) {
                return m1.getLeft().compareTo(m2.getLeft());
            }
        });

        return menuResourceList;
    }

}
