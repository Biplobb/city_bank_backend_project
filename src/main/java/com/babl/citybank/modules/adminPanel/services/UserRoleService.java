package com.babl.citybank.modules.adminPanel.services;


import com.babl.citybank.datasources.oracle.adminPanel.repositories.DelegationRepository;
import com.babl.citybank.datasources.oracle.adminPanel.repositories.RoleRepository;
import com.babl.citybank.datasources.oracle.adminPanel.repositories.UserRoleRepository;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.Delegation;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.User;
import com.babl.citybank.modules.adminPanel.resources.UserRoleResource;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.Role;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.UserRole;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserRoleService {
    private final RoleRepository roleRepository;
    private final UserRoleRepository userRoleRepository;
    private final DelegationRepository delegationRepository;
    private final LeaveService leaveService;

    public boolean assignUserRole(int roleId, String  userName){
        UserRole userRole = userRoleRepository.findByUsernameAndRoleId(userName, roleId);
        if (userRole != null){
            log.info("role already assigned");
            return  false;
        }

        Role role = roleRepository.findById(roleId);
        if (role == null){
            log.info("role not found");
            return false;
        }


        userRole = new UserRole();
        userRole.setRoleId(roleId);
        userRole.setUsername(userName);
        userRoleRepository.save(userRole);
        log.info("role is assigned to user");
        return true;
    }

    public boolean unassignUserRole(int userRoleId){
        UserRole userRole = userRoleRepository.getOne(userRoleId);
        if (userRole == null){
            log.info("invalid assignment id");
            return false;
        }

        userRoleRepository.delete(userRole);
        return true;
    }

    public List<UserRoleResource> getUsersOfARole(int roleId){
        Role role = roleRepository.findById(roleId);
        if (role == null){
            log.info("role not found");
            return null;
        }

        List<UserRole> userRoleList = userRoleRepository.findByRoleId(roleId);
        List<UserRoleResource> userRoleResources = new ArrayList<>();

        for (UserRole userRole : userRoleList){
            UserRoleResource userRoleResource = new UserRoleResource();
            userRoleResource.setId(userRole.getId());
            userRoleResource.setUsername(userRole.getUsername());
            userRoleResource.setAuthority(role.getAuthority());

            userRoleResources.add(userRoleResource);
        }
        return userRoleResources;
    }

    public List<String> getRoleOfUser(String username){
        List<UserRole> userRoleList = userRoleRepository.findByUsername(username);
        if (userRoleList == null)
            return new ArrayList<>();


        List<String> authorities = new ArrayList<>();
        for (UserRole userRole : userRoleList){
            Role role = roleRepository.getOne(userRole.getRoleId());
            authorities.add(role.getAuthority());
        }
        return authorities;
    }

    public List<String> getDelegatedRole(User user){
        Delegation delegation = delegationRepository.findByDelegatedUser(user);
        List<String> authorities = new ArrayList<>();
        if (delegation != null && leaveService.isOnLeave(delegation.getMainUser()))
            authorities = getRoleOfUser(delegation.getMainUser().getUsername());
        return authorities;
    }
}
