package com.babl.citybank.modules.adminPanel.resources;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleResource {
    private int roleId;

    private String authority;
}
