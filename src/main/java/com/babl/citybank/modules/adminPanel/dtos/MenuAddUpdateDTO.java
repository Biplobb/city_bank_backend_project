package com.babl.citybank.modules.adminPanel.dtos;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class MenuAddUpdateDTO {
    @NotNull
    private int id;

    @NotBlank
    private String name;

    @Pattern(regexp = "RIGHT|CHILD")
    private String position;
}
