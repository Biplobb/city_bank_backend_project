package com.babl.citybank.modules.adminPanel.resources;

import lombok.Data;

import java.util.List;

@Data
public class RoleAndMenuOfUserResource {
    private List<String> roles;

    private List<String> menus;

    private String workplace;
}
