package com.babl.citybank.modules.adminPanel.controllers;



import com.babl.citybank.modules.adminPanel.dtos.RoleMenuAddUpdateDTO;
import com.babl.citybank.datasources.oracle.adminPanel.repositories.MenuRepository;
import com.babl.citybank.modules.adminPanel.resources.MenuResource;
import com.babl.citybank.modules.adminPanel.resources.RoleMenuResource;
import com.babl.citybank.modules.adminPanel.resources.StatusResource;
import com.babl.citybank.modules.adminPanel.services.RoleMenuService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RoleMenuController {
    private final RoleMenuService roleMenuService;
    private final MenuRepository menuRepository;

    @RequestMapping(value = "roleMenu/add", method = RequestMethod.POST)
    public StatusResource addRoleMenu(@RequestBody @Validated final RoleMenuAddUpdateDTO roleMenuAddUpdateDTO) {
        boolean status = roleMenuService.addRoleMenu(roleMenuAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "roleMenu/addBulk", method = RequestMethod.POST)
    public void addRoleMenu(@RequestBody @Validated final List<RoleMenuAddUpdateDTO> roleMenuAddUpdateDTOs) {
        roleMenuService.addRoleMenuAll(roleMenuAddUpdateDTOs);
    }

    @RequestMapping(value = "roleMenu/update/{id}", method = RequestMethod.POST)
    public StatusResource updateRoleMenu(@RequestBody @Validated final RoleMenuAddUpdateDTO roleMenuAddUpdateDTO, @PathVariable("id") final Integer id) {
        boolean status = roleMenuService.updateRoleMenu(roleMenuAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "roleMenu/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteRoleMenu(@PathVariable("id") final Integer id) {
        boolean status = roleMenuService.deleteRoleMenu(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }


    @RequestMapping(value = "roleMenu/deleteBulk", method = RequestMethod.POST)
    public void deleteRoleMenu(@RequestBody final List<Integer> roleMenuIdList) {
        log.info(roleMenuIdList.toString());
        log.info("deleting bulk role menu");
        roleMenuService.deleteRoleMenuBulk(roleMenuIdList);
    }


    @RequestMapping(value = "roleMenu/get/{id}", method = RequestMethod.GET)
    public RoleMenuResource getRoleMenuResource(@PathVariable("id") final Integer id) {
        return roleMenuService.getRoleMenuResource(id);
    }


    @RequestMapping(value = "user/getMenu/{username}", method = RequestMethod.GET)
    public List<MenuResource> getMenuesOfUser(@PathVariable("username") final String username){
        return roleMenuService.getMenuResourcesOfUser(username);
    }

}