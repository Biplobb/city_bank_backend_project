package com.babl.citybank.modules.generic.dtos;

import lombok.Data;

import java.util.Map;

@Data
public class CrudGenericDTO {
    private String objectName;

    private String action;

    //private List<GenericVariable> keys;

    //private List<GenericVariable> variables;

    private Map<String, ?> keys;

    private Map<String, ?> variables;
}

