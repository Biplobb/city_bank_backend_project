package com.babl.citybank.modules.generic.services;



import com.babl.citybank.modules.generic.dtos.CrudGenericDTO;
import com.babl.citybank.modules.generic.dtos.SelectGenericDTO;
import com.babl.citybank.datasources.oracle.master.repositories.AuditLogRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GenericService {
    private final EntityManager entityManager;

    private final HttpServletRequest httpServletRequest;

    private final AuditLogRepository auditLogRepository;

    private String getCurrentAuditor(){
        return httpServletRequest.getUserPrincipal().getName();
    }


    public boolean addGeneric(CrudGenericDTO crudGenericDTO, boolean audit){
        String username = getCurrentAuditor();
        LocalDateTime currentTime = LocalDateTime.now();

        String sqlQueryFirst = "insert into " + crudGenericDTO.getObjectName() + "(";
        String sqlQueryLast = ") values (";

        //List<GenericVariable> variables = crudGenericDTO.getVariables();
        Map<String, ?> variables = crudGenericDTO.getVariables();

        int i = 1;


        for (Map.Entry<String, ?> entry : variables.entrySet()){
            sqlQueryFirst = sqlQueryFirst + entry.getKey();
            sqlQueryLast = sqlQueryLast + ":" + entry.getKey() + "Val";

            if (i < variables.size()){
                sqlQueryFirst = sqlQueryFirst + ", ";
                sqlQueryLast = sqlQueryLast + ", ";
            }

            i++;

        }

        if (audit){

            sqlQueryFirst = sqlQueryFirst + ", created_by, created_date, modified_by, modified_date ";
            sqlQueryLast = sqlQueryLast + ", :createdByVal, :createdDateVal, :modifiedByVal, :modifiedDateVal";

        }

        String sqlQuery = sqlQueryFirst + sqlQueryLast + ")";


        try {

            Query query = entityManager.createNativeQuery(sqlQuery);
            for (Map.Entry<String, ?> entry : variables.entrySet()) {
                String queryVariable = entry.getKey() + "Val";
                query.setParameter(queryVariable, entry.getValue());
            }

            if (audit){
                query.setParameter("createdByVal", username);
                query.setParameter("createdDateVal", currentTime);
                query.setParameter("modifiedByVal", username);
                query.setParameter("modifiedDateVal", currentTime);

            }

            //System.out.println(sqlQuery);

            query.executeUpdate();


            return true;
        }
        catch (Exception e){
            System.out.println(e);
        }

        return false;
    }


    public boolean updateGeneric(CrudGenericDTO crudGenericDTO){
        String sqlQuery = "update " + crudGenericDTO.getObjectName() + " set ";

        Map<String, ?> variables = crudGenericDTO.getVariables();
        Map<String, ?> keys = crudGenericDTO.getKeys();

        int size = variables.size();
        int i = 1;

        for (Map.Entry<String, ?> entry : variables.entrySet()){
            sqlQuery = sqlQuery + entry.getKey() + " = :" + entry.getKey() + "Val";

            if (i < size){
                sqlQuery = sqlQuery + ", ";
            }
            i++;
        }


        sqlQuery = sqlQuery + " where ";

        size = keys.size();
        i = 1;

        for (Map.Entry<String, ?> entry : keys.entrySet()){
            sqlQuery = sqlQuery + entry.getKey() + " = :" + entry.getKey() + "Key";

            if (i < size){
                sqlQuery = sqlQuery + "and ";
            }
            i++;
        }

        try {
            Query query = entityManager.createNativeQuery(sqlQuery);

            for (Map.Entry<String, ?> entry : variables.entrySet()) {
                String queryVariable = entry.getKey() + "Val";
                query.setParameter(queryVariable, entry.getValue());
                //System.out.println("Var " + queryVariable + " " + entry.getValue());
            }

            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                String queryVariable = entry.getKey() + "Key";
                query.setParameter(queryVariable, entry.getValue());
                //System.out.println("key " + queryVariable + " " + entry.getValue());
            }

            query.executeUpdate();
            return true;
        }
        catch (Exception e){
            System.out.println(e);
        }

        //System.out.println(sqlQuery);
        return false;
    }

    public boolean deleteGeneric(CrudGenericDTO crudGenericDTO){
        String sqlQuery = "delete from " +  crudGenericDTO.getObjectName() + " where ";

        Map<String, ?> keys = crudGenericDTO.getKeys();

        int size = keys.size();
        int i = 1;

        for (Map.Entry<String, ?> entry : keys.entrySet()){
            sqlQuery = sqlQuery + entry.getKey() + " = :" + entry.getKey() + "Val";

            if (i < size){
                sqlQuery = sqlQuery + "and ";
            }
            i++;
        }

        //System.out.println(sqlQuery);

        try {
            Query query = entityManager.createNativeQuery(sqlQuery);

            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                String queryVariable = entry.getKey() + "Val";
                query.setParameter(queryVariable, entry.getValue());
            }

            query.executeUpdate();
            return true;
        }
        catch (Exception e){
            System.out.println(e);
        }


        return false;
    }

    public List<?> selectGeneric(SelectGenericDTO selectGenericDTO){
        String sqlQuery = "select ";

        List<String> columns = selectGenericDTO.getFields();

        //System.out.println(columns);

        int size = columns.size();
        int i = 1;

        for (String column : columns){
            sqlQuery = sqlQuery + column;

            if (i < size){
                sqlQuery = sqlQuery + ", ";
            }
            i++;

        }

        sqlQuery = sqlQuery + " from " + selectGenericDTO.getObjectName();

        Map<String, ?> searchFields = selectGenericDTO.getSearchFields();

        if (searchFields != null){
            if (searchFields.size() > 0){
                size = searchFields.size();
                i = 1;


                sqlQuery = sqlQuery + " where ";

                for (Map.Entry<String, ?> entry : searchFields.entrySet()){
                    sqlQuery = sqlQuery + entry.getKey() + " = :" + entry.getKey() + "Val";

                    if (i < size){
                        sqlQuery = sqlQuery + "and ";
                    }
                    i++;
                }

            }
        }

        //System.out.println(sqlQuery);

        try {
            Query query = entityManager.createNativeQuery(sqlQuery);

            if (searchFields != null) {
                if (searchFields.size() > 0) {

                    for (Map.Entry<String, ?> entry : searchFields.entrySet()) {
                        String queryVariable = entry.getKey() + "Val";
                        query.setParameter(queryVariable, entry.getValue());
                    }
                }
            }

            List<?> resultList =  query.getResultList();

            List<HashMap> hashMapList = new ArrayList<>();

            if (resultList.size() == 0)
                return hashMapList;


            for (int row=0; row<resultList.size(); row++){
                HashMap<String, Object> rowMap = new HashMap<>();

                Object[] object = (Object[]) resultList.get(row);

                for (int columnNo = 0; columnNo < object.length; columnNo++){
                    rowMap.put(columns.get(columnNo), object[columnNo]);
                }

                hashMapList.add(rowMap);

            }

            return hashMapList;
        }
        catch (Exception e){
            System.out.println(e);
        }

        return null;
    }

    public List<HashMap> selectAllQuery(SelectGenericDTO selectGenericDTO){
        try {

            String queryStringForColumnName = "desc " + selectGenericDTO.getObjectName();
            Query query = entityManager.createNativeQuery(queryStringForColumnName);

            List<Object[]> objectsList = query.getResultList();

            List<String> columns = new ArrayList<>();

            for (Object[] objects : objectsList) {
                columns.add((String) objects[0]);
            }


            String queryStringForData = "select * from " + selectGenericDTO.getObjectName();

            Query dataQuery = entityManager.createNativeQuery(queryStringForData);

            List<Object[]> dataArrayList = dataQuery.getResultList();

            List<HashMap> hashMapList = new ArrayList<>();

            if (dataArrayList.size() == 0)
                return hashMapList;

            for (int row = 0; row < dataArrayList.size(); row++) {
                HashMap<String, Object> rowMap = new HashMap<>();

                Object[] dataArray = dataArrayList.get(row);

                for (int columnNo = 0; columnNo < dataArray.length; columnNo++) {
                    rowMap.put(columns.get(columnNo), dataArray[columnNo]);
                }

                hashMapList.add(rowMap);

            }

            return hashMapList;

        }
        catch (Exception e){
            System.out.println(e);
        }

        return null;

    }

}
