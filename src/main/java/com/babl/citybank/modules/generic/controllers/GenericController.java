package com.babl.citybank.modules.generic.controllers;



import com.babl.citybank.modules.generic.dtos.CrudGenericDTO;
import com.babl.citybank.modules.generic.dtos.SelectGenericDTO;
import com.babl.citybank.modules.generic.services.GenericService;
import com.babl.citybank.modules.generic.services.OracleSelectGenericService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import java.util.List;


@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GenericController {
    private final GenericService genericService;
    private final OracleSelectGenericService oracleSelectGenericService;
    private final EntityManager entityManager;


    @RequestMapping(value = "/generic/crud", method = RequestMethod.POST)
    public ResponseEntity addGeneric(@RequestBody CrudGenericDTO crudGenericDTO) throws Exception{
        if (crudGenericDTO.getAction().equals("create")) {
            boolean status = genericService.addGeneric(crudGenericDTO, false);
            if (status)
                return new ResponseEntity(HttpStatus.CREATED);
        }
        else if (crudGenericDTO.getAction().equals("update")) {
            boolean status = genericService.updateGeneric(crudGenericDTO);
            if (status)
                return new ResponseEntity(HttpStatus.ACCEPTED);
        }
        else if (crudGenericDTO.getAction().equals("delete")) {
            boolean status = genericService.deleteGeneric(crudGenericDTO);
            if (status)
                return new ResponseEntity(HttpStatus.ACCEPTED);
        }
        else
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        return new ResponseEntity(HttpStatus.EXPECTATION_FAILED);
    }


    @RequestMapping(value = "/generic/get", method = RequestMethod.POST)
    public List<?>  selectGeneric(@RequestBody SelectGenericDTO selectGenericDTO){
        return oracleSelectGenericService.selectGeneric(selectGenericDTO, entityManager);
    }

    @RequestMapping(value = "/generic/getAll", method = RequestMethod.POST)
    public List<?> selectAllGeneric(@RequestBody SelectGenericDTO selectGenericDTO){
        return oracleSelectGenericService.selectAllQuery(selectGenericDTO, entityManager);
    }

}
