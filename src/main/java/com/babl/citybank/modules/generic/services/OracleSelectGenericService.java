package com.babl.citybank.modules.generic.services;

import com.babl.citybank.modules.generic.dtos.SelectGenericDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OracleSelectGenericService {

    public List<?> selectGeneric(SelectGenericDTO selectGenericDTO, EntityManager entityManager){
        //EntityManager entityManager = entityManagerFactory.createEntityManager();

        String sqlQuery = "select ";

        List<String> columns = selectGenericDTO.getFields();

        //System.out.println(columns);

        int size = columns.size();
        int i = 1;

        for (String column : columns){
            sqlQuery = sqlQuery + column;

            if (i < size){
                sqlQuery = sqlQuery + ", ";
            }
            i++;

        }

        sqlQuery = sqlQuery + " from " + selectGenericDTO.getObjectName();

        Map<String, ?> searchFields = selectGenericDTO.getSearchFields();

        if (searchFields != null){
            if (searchFields.size() > 0){
                size = searchFields.size();
                i = 1;


                sqlQuery = sqlQuery + " where ";

                for (Map.Entry<String, ?> entry : searchFields.entrySet()){
                    sqlQuery = sqlQuery + entry.getKey() + " = :" + entry.getKey() + "Val";

                    if (i < size){
                        sqlQuery = sqlQuery + "and ";
                    }
                    i++;
                }

            }
        }

        //System.out.println(sqlQuery);

        try {
            Query query = entityManager.createNativeQuery(sqlQuery);

            if (searchFields != null) {
                if (searchFields.size() > 0) {

                    for (Map.Entry<String, ?> entry : searchFields.entrySet()) {
                        String queryVariable = entry.getKey() + "Val";
                        query.setParameter(queryVariable, entry.getValue());
                    }
                }
            }

            List<?> resultList =  query.getResultList();

            List<HashMap> hashMapList = new ArrayList<>();

            if (resultList.size() == 0)
                return hashMapList;


            for (int row=0; row<resultList.size(); row++){
                HashMap<String, Object> rowMap = new HashMap<>();

                Object[] object = (Object[]) resultList.get(row);

                for (int columnNo = 0; columnNo < object.length; columnNo++){
                    rowMap.put(columns.get(columnNo), object[columnNo]);
                }

                hashMapList.add(rowMap);

            }

            return hashMapList;
        }
        catch (Exception e){

            System.out.println(e);
        }


        return null;
    }

    public List<?> selectAllQuery(SelectGenericDTO selectGenericDTO, EntityManager entityManager){
        //EntityManager entityManager = entityManagerFactory.createEntityManager();
        Map<String, ?> searchFields = selectGenericDTO.getSearchFields();
        int size, i;

        try {

            String queryStringForColumnName = "select column_name FROM user_tab_columns WHERE table_name='" + selectGenericDTO.getObjectName() + "' ORDER BY column_id";
            Query query = entityManager.createNativeQuery(queryStringForColumnName);


            List<Object> objectsList = query.getResultList();

            List<String> columns = new ArrayList<>();

            for (Object objects : objectsList) {
                columns.add((String) objects);
            }


            String queryStringForData = "select * from " + selectGenericDTO.getObjectName();

            if (searchFields != null){
                if (searchFields.size() > 0){
                    size = searchFields.size();
                    i = 1;


                    queryStringForData = queryStringForData + " where ";

                    for (Map.Entry<String, ?> entry : searchFields.entrySet()){
                        queryStringForData = queryStringForData + entry.getKey() + " = :" + entry.getKey() + "Val";

                        if (i < size){
                            queryStringForData = queryStringForData + " and ";
                        }
                        i++;
                    }

                }
            }

            //System.out.println(queryStringForData);

            Query dataQuery = entityManager.createNativeQuery(queryStringForData);


            if (searchFields != null) {
                if (searchFields.size() > 0) {

                    for (Map.Entry<String, ?> entry : searchFields.entrySet()) {
                        String queryVariable = entry.getKey() + "Val";
                        dataQuery.setParameter(queryVariable, entry.getValue());
                    }
                }
            }

            List<Object[]> dataArrayList = dataQuery.getResultList();

            List<HashMap> hashMapList = new ArrayList<>();

            if (dataArrayList.size() == 0)
                return hashMapList;

            for (int row = 0; row < dataArrayList.size(); row++) {
                HashMap<String, Object> rowMap = new HashMap<>();

                Object[] dataArray = dataArrayList.get(row);

                for (int columnNo = 0; columnNo < dataArray.length; columnNo++) {
                    rowMap.put(columns.get(columnNo), dataArray[columnNo]);
                }

                hashMapList.add(rowMap);

            }


            return hashMapList;

        }
        catch (Exception e){

            System.out.println(e);
        }

//        try{
//            String queryStringForData = "select * from " + selectGenericDTO.getObjectName();
//
//            Query dataQuery = entityManager.createNativeQuery(queryStringForData);
//
//            List<Object[]> dataArrayList = dataQuery.getResultList();
//            entityManager.close();
//            return dataArrayList;
//
//        }
//        catch (Exception e){
//            entityManager.close();
//            System.out.println(e);
//        }

        return null;

    }

    public List selectFromDatabase(String queryString, EntityManager entityManager){
        try{
            Query query = entityManager.createNativeQuery(queryString);
            return query.getResultList();
        }
        catch (Exception e){
            System.out.println(e);
        }

        return new ArrayList<>();
    }


}
