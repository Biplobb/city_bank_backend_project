package com.babl.citybank.modules.generic.dtos;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class SelectGenericDTO {
    private String objectName;

    private List<String> fields;

    private Map<String, ?> searchFields;
}
