package com.babl.citybank.modules.workflow.dtos;

import lombok.Data;

import java.time.LocalDate;

@Data
public class CreateDeferralDTO {
    private int appId;

    private String type;

    private String dueDate;

    private Integer duration;

    private String remarks;
}
