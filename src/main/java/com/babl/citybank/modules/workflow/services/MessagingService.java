package com.babl.citybank.modules.workflow.services;

import com.babl.citybank.datasources.oracle.workflow.repositories.AppDelegationRepository;
import com.babl.citybank.datasources.oracle.workflow.repositories.AppMessageRepository;
import com.babl.citybank.datasources.oracle.workflow.repositories.ApplicationRepository;
import com.babl.citybank.datasources.oracle.workflow.schemas.AppDelegation;
import com.babl.citybank.datasources.oracle.workflow.schemas.AppMessage;
import com.babl.citybank.datasources.oracle.workflow.schemas.Application;
import com.babl.citybank.exceptions.CaseNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Transactional
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MessagingService {
    private final ApplicationRepository applicationRepository;
    private final AppDelegationRepository appDelegationRepository;
    private final AppMessageRepository appMessageRepository;

    private Application getApplicationFromDelId(int appDelId){
        AppDelegation appDelegation = appDelegationRepository.findById(appDelId);

        if (appDelegation == null)
            throw new CaseNotFoundException("Case no found");

        return appDelegation.getApplication();
    }

    private AppMessage toResponse(AppMessage appMessage){
        AppMessage response = new AppMessage();
        response.setMessage(appMessage.getMessage());
        response.setCreatedBy(appMessage.getCreatedBy());
        response.setCreatedDate(appMessage.getCreatedDate());
        return response;
    }

    public boolean addMessage(int appDelId, String message){
        Application application = getApplicationFromDelId(appDelId);

        AppMessage appMessage = new AppMessage();
        appMessage.setApplication(application);
        appMessage.setMessage(message);
        appMessageRepository.save(appMessage);
        return true;
    }

    public List<AppMessage> getApplicationMessages(int appDelId){
        Application application = getApplicationFromDelId(appDelId);
        List<AppMessage> returnData = new ArrayList<>();
        List<AppMessage> appMessageList = appMessageRepository.findByApplication(application);

        for (AppMessage appMessage : appMessageList){
            returnData.add(toResponse(appMessage));
        }

        return returnData;
    }

}
