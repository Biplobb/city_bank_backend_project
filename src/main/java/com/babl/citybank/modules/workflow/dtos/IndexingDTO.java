package com.babl.citybank.modules.workflow.dtos;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class IndexingDTO {
    private int appId;
    private Map<String, String> fileNames;
}
