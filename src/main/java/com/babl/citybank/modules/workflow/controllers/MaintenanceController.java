package com.babl.citybank.modules.workflow.controllers;

import com.babl.citybank.modules.workflow.services.MaintenanceService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MaintenanceController {
    private final MaintenanceService maintenanceService;

    @PostMapping("/bearer/approval")
    public String isRequired(@RequestBody List<String> varNames){
        return maintenanceService.isApprovalRequired(varNames);
    }
}
