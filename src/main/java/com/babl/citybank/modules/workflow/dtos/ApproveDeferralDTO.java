package com.babl.citybank.modules.workflow.dtos;

import lombok.Data;

import javax.validation.constraints.Pattern;

@Data
public class ApproveDeferralDTO {
    private int appId;

    @Pattern(regexp = "APPROVE|REJECT")
    private String approval;
}
