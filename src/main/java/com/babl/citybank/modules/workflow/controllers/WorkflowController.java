package com.babl.citybank.modules.workflow.controllers;


import com.babl.citybank.datasources.oracle.workflow.schemas.SdDepartmentMaster;
import com.babl.citybank.datasources.oracle.workflow.schemas.SdDepartmentTeam;
import com.babl.citybank.modules.workflow.dtos.IndexingDTO;
import com.babl.citybank.modules.workflow.services.WorkflowService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class WorkflowController {
    private final WorkflowService workflowService;

    @GetMapping("/startCase/{title}")
    public Object startCase(@PathVariable("title") String title){
        return workflowService.startCase(title);
    }

    @GetMapping("/variables/{appId}")
    public Object getCaseVariables(@PathVariable("appId") int appId ){
        return workflowService.getCaseVariables(appId);
    }

    @PostMapping("/variables/{appId}")
    public void setCaseVariables(@PathVariable("appId") int appId, @RequestBody Map map){
        workflowService.setCaseVariables(appId, map, false);
    }

    @PostMapping("/save/{appId}")
    public void saveCaseData(@PathVariable("appId") int appId, @RequestBody Map map){
        workflowService.setCaseVariables(appId, map, true);
    }

    @GetMapping("/case/route/{appId}")
    public void routeCase(@PathVariable("appId") int appId){
        workflowService.routeCase(appId);
    }

    @GetMapping("/inbox")
    public Object getInbox(){
        return workflowService.getInboxCases();
    }

    @GetMapping("/inbox/return")
    public Object getReturn(){
        return workflowService.getInboxCategoryWise("RETURN");
    }

    @GetMapping("/inbox/saved")
    public Object getSaved(){
        return workflowService.getInboxCategoryWise("SAVED");
    }

    @GetMapping("/inbox/waiting")
    public Object getToView(){
        return workflowService.getInboxCategoryWise("SELF");
    }

    @GetMapping("/bmBulkAccountOpening")
    public Object getBMBulkAccountOpening(){
        return workflowService.getBMBulkAccountOpening();
    }

    @PostMapping("/case/upload")
    public void uploadCaseFile(@RequestParam("appId") final int appId,
                               @RequestParam("type") final String[] type,
                               @RequestParam("file") final MultipartFile[] multipartFiles){

        System.out.println(type.length);
        System.out.println(multipartFiles.length);
        workflowService.uploadMultipleFile(appId, type, multipartFiles);

    }

    @GetMapping("/file/{name}")
    public ResponseEntity getFileByName (@PathVariable("name") String name) throws Exception{
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.IMAGE_JPEG);
        return new ResponseEntity<>(workflowService.getCaseFile(name), httpHeaders, HttpStatus.OK) ;
    }

    @GetMapping("/case/files/{appId}")
    public List<String> getCaseFiles(@PathVariable("appId") int appId){
        return workflowService.getCaseFileName(appId);
    }

    @PostMapping("/case/split")
    public Object splitPdf(@RequestParam("file") final MultipartFile file, @RequestParam("appId") final int appId) throws Exception{
        return workflowService.splitPdf(file, appId);
    }

    @PostMapping("/case/mapFiles")
    public Object mapDocuments(@RequestBody IndexingDTO indexingDTO){
        return workflowService.renameFiles(indexingDTO.getAppId(), indexingDTO.getFileNames());
    }

    @PostMapping("/case/claim/{appId}")
    public void claimCase(@PathVariable("appId") int appId){
        workflowService.claimCase(appId);
    }

    @PostMapping("/case/claim")
    public void claimBulk(@RequestBody List<Integer> appIds){
        workflowService.claimMultipleCase(appIds);
    }

    @PostMapping("/case/reassign/{appId}/{username}")
    public void reassignCase(@PathVariable("appId")int appId, @PathVariable("username") String username){
        workflowService.reassignCase(appId, username);
    }

    @PostMapping("/case/reassign/{username}")
    public void reassignBulk(@PathVariable("username") String username, @RequestBody List<Integer> appIdList){
        for (Integer appId : appIdList) {
            workflowService.reassignCase(appId, username);
        }
    }

    @GetMapping("/sdTeam/get")
    public List<String> getSdTeam(){
        return workflowService.getSdTeams();
    }

    @GetMapping("/reassign/getUsers/{teamName}")
    public List<String> getUsernameToReassign(@PathVariable("teamName") String teamName){
        return workflowService.usersToAssignSD(teamName);
    }

    @GetMapping("/claimable")
    public Object getClaimable(){
        return workflowService.getClaimableCase();
    }

    @GetMapping("/cardClaimable")
    public Object getCardClaimable(){
        return workflowService.getCardMaintenanceClaimable();
    }

    @GetMapping("/checkers")
    public List<String> getCheckers(){
        return workflowService.getNextUserList("CHECKER");
    }

    @GetMapping("/user/approvalOfficers")
    public List<String> getApprovalOfficers(){
        return workflowService.getNextUserList("APPROVAL_OFFICER");
    }

    @GetMapping("/user/absCheckers")
    public List<String> getAbsCheckers(){
        return workflowService.getAbsCheckers();
    }



}
