package com.babl.citybank.modules.workflow.utils;

import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class WorkflowUtils {
    public String mapAddToString(Map<String, Object> map, String appData) {
        Map<String, String> oldMap = stringToMap(appData);

        for (String key : map.keySet()) {
            String val = null;
            if (map.get(key) != null) {
                val = map.get(key).toString();
                val = val.replace(",", "^");
            }
            oldMap.put(key, val);
        }

        String newString = oldMap.toString();

        newString = newString.replace("{", "");
        newString = newString.replace("}", "");
        return newString.trim();

    }

    public Map<String, String> stringToMap(String string) {
        ///mapped appdata is saved in string

        Map<String, String> map = new HashMap<>();

        if (string == null)
            return map;

        String[] keyValues = string.split(",");
        for (String keyVal : keyValues) {
            String[] ss = keyVal.trim().split("=");
            if (ss.length < 2)
                map.put(ss[0], null);
            else
                map.put(ss[0], ss[1]);
        }


        return map;
    }

    public boolean checkCondition(String expression, Map<String, String> variables) {
        String[] splitted = expression.split("[ =()&|]+");

        String[] ss = new String[splitted.length];

        int pos = 0;
        for (String s : splitted){
            s = s.replace('^', ' ');
            ss[pos] = s;
            pos++;
        }

        List<Boolean> boolList = new ArrayList();

        int i = 0;



        if (expression.length() > 0 && expression.charAt(0) == '(')
            i = 1;

        for (; i < ss.length; ) {
            /*if (ss[i].equals("and") || ss[i].equals("or"))
                i++;*/
            boolean val = variables.get(ss[i]) == null ? ss[i + 1].equals("null") : variables.get(ss[i]).equals(ss[i + 1]);

            boolList.add(val);
            i = i + 2;
        }

        Stack<Object> stack = new Stack<>();

        int boolTaken = 0;


        for (int j = 0; j < expression.length(); j++) {
            if (j + 2 < expression.length() && expression.substring(j, j + 2).equals("&&")) {
                stack.push("and");
                j = j + 1;
            } else if (j + 2 < expression.length() && expression.substring(j, j + 2).equals("==")) {
                stack.push(boolList.get(boolTaken));
                boolTaken++;
                j++;
            } else if (j + 2 < expression.length() && expression.substring(j, j + 2).equals("||")) {
                stack.push("or");
                j++;
            } else if (expression.charAt(j) == '(')
                stack.push("(");
            else if (expression.charAt(j) == ')') {
                //stack.push(")");
                boolean bb = (boolean) stack.pop();

                while (!stack.empty() && !stack.peek().equals("(")) {
                    Object op = stack.pop();
                    boolean two = (boolean) stack.pop();
                    if (op.equals("and"))
                        bb = bb && two;
                    else
                        bb = bb || two;
                }
                stack.pop();
                stack.push(bb);
            }

        }


        boolean result = (boolean) stack.pop();
        while (!stack.empty()) {
            Object op = stack.pop();
            boolean two = (boolean) stack.pop();
            if (op.equals("and"))
                result = result && two;
            else
                result = result || two;
        }

        return result;

    }
}
