package com.babl.citybank.modules.workflow.utils;

import com.babl.citybank.datasources.oracle.adminPanel.repositories.UserRepository;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.User;
import com.babl.citybank.datasources.oracle.customer.repositories.GroupingRepository;
import com.babl.citybank.datasources.oracle.workflow.repositories.ApplicationRepository;
import com.babl.citybank.datasources.oracle.workflow.repositories.GroupRepository;
import com.babl.citybank.datasources.oracle.workflow.schemas.*;
import com.babl.citybank.exceptions.VariableNotSetException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TriggerFunctions {
    private final WorkflowUtils workflowUtils;
    private final ApplicationRepository applicationRepository;
    private final UserRepository userRepository;
    private final GroupRepository groupRepository;

    private Group bomGroup;
    private Group bmGroup;

    @EventListener
    public void setCheckerGroup(ContextRefreshedEvent event){
        this.bomGroup = groupRepository.findByGroupName("BOM");
        this.bmGroup = groupRepository.findByGroupName("BM");
    }


    private String getAuditor(String role, String auditors){
        String[] ad = auditors.split(",");
        for (String as : ad){
            String[] keyVal = as.split(":");
            if (keyVal[0].trim().equals(role)) {
                return keyVal[1].trim();
            }
        }
        return null;
    }

    private String setAuditors(String auditors, String role, String username){
        String[] ad = auditors.split(",");
        for (String as : ad){
            String[] keyVal = as.split(":");
            if (keyVal[0].trim().equals(role)){
                return auditors.replace(keyVal[1].trim(), username);
            }

        }
        return auditors + ", " + role + " : " + username;
    }



    public void branchFlowTrigger(Application application, AppDelegation appDelegation, User user, Map<String, String> caseVariableMap, String currentAppData, String currentTaskTitle){
        String appInitUser = application.getAppInitUser();
        String username = user.getUsername();


        BranchMaster userBranch = null;
        CSUMaster userUnit = null;

        if (user.getWorkplace() != null && user.getWorkplace().equals("BRANCH")){
            userBranch = user.getBranch();
        }
        else if (user.getWorkplace() != null && user.getWorkplace().equals("CSU")){
            userUnit = user.getCsu();
        }



        String auditors = application.getAuditors();
        String nextUserName = "";
        if (auditors == null)
            auditors = "";

        String userGroupName = user.getGroup().getGroupName();

        Map<String, Object> newVariableMap = new HashMap<>();

        if (userGroupName.equals("CS")){
            if (currentTaskTitle.equals("cs_data_capture")){
                if ((caseVariableMap.get("cs_deferal").equals("YES") && caseVariableMap.get("bm_approval") == null) ||
                        (caseVariableMap.get("cs_bearer") != null && caseVariableMap.get("cs_bearer").equals("BM")) ||
                        (caseVariableMap.get("cs_bearer") != null && caseVariableMap.get("cs_bearer").equals("BOTH")) ){
                    List<User> userList = userRepository.findByGroupAndBranch(bmGroup, userBranch);
                    if (userList.size() == 0)
                        throw new VariableNotSetException("Next Bm not found in workflow");

                    nextUserName = userList.get(0).getUsername();

                }
                else {
                    List<User> userList = userRepository.findByGroupAndBranch(bomGroup, userBranch);
                    if (userList.size() == 0)
                        throw new VariableNotSetException("Next Bom not found in workflow");

                    nextUserName = userList.get(0).getUsername();

                    if (caseVariableMap.get("bom_approval") == null){
                        List<User> bmUserList = userRepository.findByGroupAndBranch(bmGroup, userBranch);
                        if (bmUserList.size() == 0)
                            throw new VariableNotSetException("Next Bm not found in workflow");

                        String bmUser = bmUserList.get(0).getUsername();

                        newVariableMap.put("bm_bulk_user", bmUser);
                        caseVariableMap.put("bm_bulk_user", bmUser);
                    }

                }
            }
            else if (currentTaskTitle.equals("csu_cso_data_capture")){
                if (caseVariableMap.get("cs_deferal").equals("YES") && caseVariableMap.get("deferal_approval") == null){
                    nextUserName = caseVariableMap.get("next_user");
                }
                else {
                    List<User> bomUser = userRepository.findByGroupAndCsu(bomGroup, userUnit);
                    if (bomUser.size() == 0)
                        throw new VariableNotSetException("Next Bom not found in workflow");
                    nextUserName = bomUser.get(0).getUsername();
                }
            }
        }
        else if (userGroupName.equals("BOM")){
            if (caseVariableMap.get("bom_approval").equals("REJECT") || caseVariableMap.get("bom_approval").equals("RETURN")){
                nextUserName = appInitUser;
            }
            else if (caseVariableMap.get("bom_approval").equals("APPROVED")){
                ///round robin logic
            }
        }
        else if (userGroupName.equals("BM")){
            nextUserName = appInitUser;
        }
        else if (userGroupName.equals("MAKER")){
            if (currentTaskTitle.equals("maker_signiture_photo_upload")){
                nextUserName = caseVariableMap.get("next_user");
            }
            else if (currentTaskTitle.equals("maker_update_all_information") || currentTaskTitle.equals("maker_fdr_opening") || currentTaskTitle.equals("maker_dps_opening")){
                //System.out.println(currentTaskTitle);
                if (caseVariableMap.get("maker_update_all_info_send_to").equals("CS")){
                    nextUserName = appInitUser;
                }
                else {
                    nextUserName = caseVariableMap.get("next_user");
                }
            }
            else if (currentTaskTitle.equals("abs_maker_account_opening")){
                if (caseVariableMap.get("abs_deferal").equals("NO"))
                    nextUserName = caseVariableMap.get("next_user");
                else
                    nextUserName = "abhapproval";
                ///change it as soon as possible
            }
            //System.out.println(nextUserName);
        }
        else if (userGroupName.equals("CHECKER")){
            if (currentTaskTitle.equals("abs_checker_account_opening")){
                if (caseVariableMap.get("checker_approval").equals("RETURN")){
                    nextUserName = appInitUser;
                }
            }
            else {
                if (caseVariableMap.get("checker_approval").equals("NOTAPPROVED")){
                    nextUserName = getAuditor("MAKER", auditors);
                }
            }
        }
        else if (userGroupName.equals("APPROVAL_OFFICER")){
            nextUserName = appInitUser;
        }
        else if (userGroupName.equals("MERCHANT")){
            nextUserName = "merchant";
        }
        else if (userGroupName.equals("CALL_CENTER")){
            if (caseVariableMap.get("call_center_approval").equals("RETURN"))
                nextUserName = appInitUser;
        }
        else  if(userGroupName.equals("CARD_DIVISION")){
            if (caseVariableMap.get("approval").equals("RETURN"))
                nextUserName = appInitUser;
        }


        newVariableMap.put("next_user", nextUserName);
        caseVariableMap.put("next_user", nextUserName);


        if (auditors.equals("")) {
            appDelegation.setServiceType(caseVariableMap.get("serviceType"));
            appDelegation.setSubserviceType(caseVariableMap.get("subServiceType"));

            if (userBranch != null)
                application.setSolId(userBranch.getSolId());
        }

        application.setCbNumber(caseVariableMap.get("cbNumber"));
        application.setAccountNumber(caseVariableMap.get("accountNumber"));
        application.setCustomerName(caseVariableMap.get("customerName"));

        if (caseVariableMap.get("urgency") == null)
            application.setUrgency(0);
        else
            application.setUrgency(Integer.parseInt(caseVariableMap.get("urgency")));


        //auditors = auditors + ", " + userGroupName + " : " + username;
        auditors = setAuditors(auditors, userGroupName, username);
        application.setAuditors(auditors);
        application.setAppData(workflowUtils.mapAddToString(newVariableMap, currentAppData));
        applicationRepository.save(application);
    }


    private void sameServiceSubService(AppDelegation oldDel, AppDelegation newDel){
        newDel.setServiceType(oldDel.getServiceType());
        newDel.setSubserviceType(oldDel.getSubserviceType());
    }

    private void setServiceSubServiceType(AppDelegation newDel, String serviceType, String subServiceType){
        newDel.setServiceType(serviceType);
        newDel.setSubserviceType(subServiceType);
    }

    private void setInboxType(Map<String, String> appData ,String oldTaskTitle, String currentTaskTitle, AppDelegation oldDel, AppDelegation newDel){
        if (oldTaskTitle.equals("bom_approval") && currentTaskTitle.equals("cs_data_capture")){
            newDel.setInboxStatus("RETURN");
        }
        else if (oldTaskTitle.equals("bm_approval") && currentTaskTitle.equals("cs_data_capture") && appData.get("bm_approval").equals("REJECT")){
            newDel.setInboxStatus("RETURN");
        }
        else if (oldTaskTitle.equals("maker_update_all_information") && currentTaskTitle.equals("cs_data_capture")){
            newDel.setInboxStatus("RETURN");
        }
        else if (oldTaskTitle.equals("checker_update_all_information_approval") && currentTaskTitle.equals("maker_update_all_information")){
            newDel.setInboxStatus("RETURN");
        }
        else if (oldTaskTitle.equals("csu_bom_approval") && currentTaskTitle.equals("csu_cso_data_capture")){
            newDel.setInboxStatus("RETURN");
        }
        else if (oldTaskTitle.equals("csu_deferral_approval") && currentTaskTitle.equals("csu_cso_data_capture") && appData.get("deferal_approval").equals("REJECT")){
            newDel.setInboxStatus("RETURN");
        }
        else if (oldTaskTitle.equals("abs_checker_account_opening") && currentTaskTitle.equals("abs_maker_account_opening")){
            newDel.setInboxStatus("RETURN");
        }
        else if (oldTaskTitle.equals("abh_deferal_approval") && currentTaskTitle.equals("abs_maker_account_opening") && appData.get("bm_approval").equals("REJECT")){
            newDel.setInboxStatus("RETURN");
        }
        else if (oldTaskTitle.equals("card_maintenance_operation") && currentTaskTitle.equals("card_maintenance_start")){
            newDel.setInboxStatus("RETURN");
        }
    }

    public void postRoutingTrigger(Map<String, String> appData, AppDelegation oldDel, AppDelegation newDel){
        String oldTaskTitle = oldDel.getTask().getTitle();
        String currentTaskTitle = newDel.getTask().getTitle();

        setInboxType(appData, oldTaskTitle, currentTaskTitle, oldDel, newDel);

        if (oldTaskTitle.equals("checker_update_all_information_approval") && currentTaskTitle.equals("maker_fdr_opening")){
            setServiceSubServiceType(newDel, "FDR Opening", oldDel.getSubserviceType());
        }
        else if (oldTaskTitle.equals("checker_update_all_information_approval") && currentTaskTitle.equals("maker_dps_opening")){
            setServiceSubServiceType(newDel, "DPS Opening", oldDel.getSubserviceType());
        }
        else {
            sameServiceSubService(oldDel, newDel);
        }
    }

}
