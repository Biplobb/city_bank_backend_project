package com.babl.citybank.modules.workflow.services;


import com.babl.citybank.datasources.oracle.adminPanel.repositories.UserRepository;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.User;
import com.babl.citybank.datasources.oracle.workflow.repositories.AppDelegationRepository;
import com.babl.citybank.datasources.oracle.workflow.repositories.ApplicationRemarksRepository;
import com.babl.citybank.datasources.oracle.workflow.repositories.ApplicationRepository;
import com.babl.citybank.datasources.oracle.workflow.schemas.AppDelegation;
import com.babl.citybank.datasources.oracle.workflow.schemas.Application;
import com.babl.citybank.datasources.oracle.workflow.schemas.ApplicationRemarks;
import com.babl.citybank.exceptions.CaseNotFoundException;
import com.babl.citybank.exceptions.UserAccessException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.omg.CORBA.UserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ApplicationRemarksService {
    private final ApplicationRemarksRepository applicationRemarksRepository;
    private final HttpServletRequest httpServletRequest;
    private final UserRepository userRepository;
    private final AppDelegationRepository appDelegationRepository;

    public Map applicationRemarkAdd(String remarks, int appId){
        Map<String,String> result = new HashMap<>();
        if (remarks.equals("undefined") || remarks.equals("null"))
            return result;

        String username = httpServletRequest.getUserPrincipal().getName();

        AppDelegation appDelegation = appDelegationRepository.findById(appId);
        if (appDelegation == null)
            throw new CaseNotFoundException("Case Not found");

        Application application = appDelegation.getApplication();
        User user = userRepository.findByUsername(username);
        if(user == null)
            throw new UserAccessException("User Access Exception");
        String userRole = user.getGroup().getGroupName();

        ApplicationRemarks applicationRemarks = new ApplicationRemarks();
        applicationRemarks.setRemarks(remarks);
        applicationRemarks.setApplicationRemarksDate(LocalDate.now());
        applicationRemarks.setCreateByUserName(username);
        applicationRemarks.setCreateByUserRole(userRole);
        applicationRemarks.setApplication(application);
        applicationRemarksRepository.save(applicationRemarks);
        result.put("massage","Successfully save");
        return result;
    }

    public List<ApplicationRemarks> getApplicationRemark(int appId){
        AppDelegation appDelegation = appDelegationRepository.findById(appId);
        if (appDelegation == null)
            throw new CaseNotFoundException("Case Not found");

        Application application = appDelegation.getApplication();
        return applicationRemarksRepository.findByApplication(application);
    }
}
