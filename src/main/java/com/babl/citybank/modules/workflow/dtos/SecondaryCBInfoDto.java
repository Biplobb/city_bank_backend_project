package com.babl.citybank.modules.workflow.dtos;

import lombok.Data;

import java.util.Map;

@Data
public class SecondaryCBInfoDto {
    int appId;
    String person;
    String cb;
    Map<String, Object> appData;
}
