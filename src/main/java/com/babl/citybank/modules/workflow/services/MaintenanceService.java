package com.babl.citybank.modules.workflow.services;

import com.babl.citybank.datasources.oracle.workflow.schemas.BearerApproval;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MaintenanceService {
    private final EntityManager entityManager;

    public String isApprovalRequired(List<String> varNames){
//        String parameters = "";
//        int sz = varNames.size();
//
//        for (int i = 0; i < sz; i++) {
//            parameters = parameters + "'" + varNames.get(i) + "'";
//            if (i < sz - 1)
//                parameters = parameters + ",";
//        }
//
//        String queryString = "select approval_required from bearer_approval where varname in (" + parameters + ")";
//
//        Query query = entityManager.createNativeQuery(queryString);
//
//        System.out.println(query.getResultList().getClass());

        Query query = entityManager.createQuery("FROM bearer_approval approval where approval.varName in :varnames");
        query.setParameter("varnames", varNames);


        List<BearerApproval> results = query.getResultList();

        System.out.println(results);

        for (BearerApproval bearerApproval : results){
            if (bearerApproval.getApprovalRequired().equals("YES"))
                return "YES";
        }
        return "NO";
    }
}
