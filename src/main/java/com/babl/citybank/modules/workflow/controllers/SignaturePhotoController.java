package com.babl.citybank.modules.workflow.controllers;

import com.babl.citybank.modules.workflow.dtos.SignaturePhotoDTO;
import com.babl.citybank.modules.workflow.services.SignaturePhotoService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SignaturePhotoController {
    private final SignaturePhotoService signaturePhotoService;

    @PostMapping("/upload/signature/photo")
    public boolean uploadSignaturePhoto(@RequestBody SignaturePhotoDTO signaturePhotoDTO){
        return signaturePhotoService.uploadSignaturePhoto(signaturePhotoDTO);
    }

    @GetMapping("/signaturePhoto/{appId}/{type}")
    public Object getSignaturePhoto(@PathVariable("appId") int appId, @PathVariable("type") String type) throws Exception{
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.IMAGE_JPEG);
        return new ResponseEntity<>(signaturePhotoService.signatureName(type, appId), httpHeaders, HttpStatus.OK) ;
    }
}
