package com.babl.citybank.modules.workflow.controllers;

import com.babl.citybank.modules.workflow.services.CardService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CardController {
    private final CardService cardService;


    @GetMapping("/card/call_category")
    public Object getCallCategory(){
        return cardService.getCallCategory();
    }
}
