package com.babl.citybank.modules.workflow.services;

import com.babl.citybank.common.AppStatus;
import com.babl.citybank.datasources.oracle.workflow.repositories.AppDelegationRepository;
import com.babl.citybank.datasources.oracle.workflow.repositories.ApplicationRepository;
import com.babl.citybank.datasources.oracle.workflow.schemas.AppDelegation;
import com.babl.citybank.datasources.oracle.workflow.schemas.Application;
import com.babl.citybank.modules.workflow.utils.TriggerFunctions;
import com.babl.citybank.modules.workflow.utils.WorkflowUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCreationHelper;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DebitCardService {
    private final ApplicationRepository applicationRepository;
    private final WorkflowUtils workflowUtils;
    private final TriggerFunctions triggerFunctions;
    private final AppDelegationRepository appDelegationRepository;

    private Date localDateToDate(LocalDateTime localDate){
        return localDate == null ? null : java.sql.Timestamp.valueOf(localDate);
    }

    public Workbook excelDebitCardList(){
        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("card_report");

        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setDataFormat(workbook.createDataFormat().getFormat("MM/dd/yyyy"));

        List<AppDelegation> appDelegationList = appDelegationRepository.findByServiceTypeAndStatus("DebitCard", AppStatus.ACTIVE);

        Row header = sheet.createRow(0);
        for (int i = 0; i < 24; i++) {
            header.createCell(i);
        }

        header.getCell(0).setCellValue("Date");
        header.getCell(1).setCellValue("Customer Name");
        header.getCell(2).setCellValue("Name on card");
        header.getCell(3).setCellValue("Account Number");
        header.getCell(4).setCellValue("CB No");
        header.getCell(5).setCellValue("DST/RM Code");
        header.getCell(6).setCellValue("Initiating Branch SOL ID");
        header.getCell(7).setCellValue("Source SOL ID");
        header.getCell(8).setCellValue("CASA SOL ID");
        header.getCell(9).setCellValue("Card to be Delivered SOL ID");
        header.getCell(10).setCellValue("Card Type");
        header.getCell(11).setCellValue("CCEP Name/Campaign Name");
        header.getCell(12).setCellValue("Product Type");
        header.getCell(13).setCellValue("Account Type");
        header.getCell(14).setCellValue("Account Currency Type");
        header.getCell(15).setCellValue("Last Comments");
        header.getCell(16).setCellValue("Account Number with CASA SOL");
        header.getCell(17).setCellValue("Created By");
        header.getCell(18).setCellValue("Created Date with time");
        header.getCell(19).setCellValue("Checked By");
        header.getCell(20).setCellValue("Checked Date with time");
        header.getCell(21).setCellValue("Current Status");
        header.getCell(22).setCellValue("Locked/ Closed By");
        header.getCell(23).setCellValue("Locked/ Closed Date with time");


        int rowNum = 1;

        for (AppDelegation appDelegation : appDelegationList){
            Application application = appDelegation.getApplication();
            Map<String, String> map = workflowUtils.stringToMap(application.getAppData());


            List<AppDelegation> bomAppDelegations = appDelegationRepository.findByApplicationAndUserGroupNameOrderByDelFinishTimestampDesc(application, "BOM");
            List<AppDelegation> makerAppDelegations = appDelegationRepository.findByApplicationAndUserGroupNameOrderByDelFinishTimestampDesc(application, "MAKER");

            AppDelegation bomAppDelegation = null;
            AppDelegation makerAppDelegation = null;

            if (bomAppDelegations.size() > 0)
                 bomAppDelegation = bomAppDelegations.get(0);
            if (makerAppDelegations.size() > 0)
                makerAppDelegation  = makerAppDelegations.get(0);



            Row row = sheet.createRow(rowNum);

            for (int i = 0; i < 24; i++) {
                row.createCell(i);
            }

            row.getCell(0).setCellValue(new Date());
            row.getCell(0).setCellStyle(cellStyle);
            row.getCell(1).setCellValue(map.get("name"));
            row.getCell(2).setCellValue(map.get("transliteration"));
            row.getCell(3).setCellValue(map.get("accountNo"));
            row.getCell(4).setCellValue(map.get("document"));
            row.getCell(5).setCellValue(map.get("dse/rm"));
            row.getCell(6).setCellValue(map.get("initialBranchSolId"));
            row.getCell(7).setCellValue(map.get("applicationSource"));
            row.getCell(8).setCellValue(map.get("casaSolId"));
            row.getCell(9).setCellValue(map.get("branchNameReceiveCard"));
            row.getCell(10).setCellValue(map.get("cardType"));
            row.getCell(11).setCellValue(map.get("corporateName"));
            row.getCell(12).setCellValue(map.get("productType"));
            row.getCell(13).setCellValue(map.get("accountType"));
            row.getCell(14).setCellValue("Account Currency Type");
            row.getCell(15).setCellValue("Last Comments");
            row.getCell(16).setCellValue(map.get("casaSolId"));
            row.getCell(17).setCellValue(application.getAppInitUser());
            row.getCell(18).setCellValue(localDateToDate(application.getAppInitTimestamp()));
            row.getCell(18).setCellStyle(cellStyle);

            if (bomAppDelegation != null) {
                row.getCell(19).setCellValue(bomAppDelegation.getUser().getUsername());
                row.getCell(20).setCellValue(localDateToDate(bomAppDelegation.getDelFinishTimestamp()));
                row.getCell(20).setCellStyle(cellStyle);
            }
            row.getCell(21).setCellValue("Active");
            if (makerAppDelegation != null) {
                row.getCell(22).setCellValue(makerAppDelegation.getUser().getUsername());
                row.getCell(23).setCellValue(localDateToDate(makerAppDelegation.getDelFinishTimestamp()));
                row.getCell(23).setCellStyle(cellStyle);
            }

            rowNum++;
        }

        return workbook;

    }

    public String getDebitXml() {
        List<AppDelegation> appDelegationList = appDelegationRepository.findByServiceTypeAndStatus("DebitCard", AppStatus.ACTIVE);

        String xmlFile = "<?xml version=\"1.0\" encoding=\"cp866\"?>\n";
        xmlFile = xmlFile + "<Root>\n";
        for (AppDelegation appDelegation : appDelegationList) {
            Application application = appDelegation.getApplication();
            Map<String, String> map = workflowUtils.stringToMap(application.getAppData());

            xmlFile = xmlFile + "<Record>\n";
            xmlFile = xmlFile + "<FIO>" + map.get("name") + "</FIO>\n";
            xmlFile = xmlFile + "<SEX>M</SEX>\n";
            xmlFile = xmlFile + "</Record>\n";
        }

        xmlFile = xmlFile + "</Root>";
        return xmlFile;
    }
}
