package com.babl.citybank.modules.workflow.controllers;

import com.babl.citybank.modules.workflow.services.FDRService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FDRController {
    private final FDRService fdrService;

    @GetMapping(value = "/fdr/accounts/{cb}")
    public Object getCurrentsavingsOfCB(@PathVariable("cb") final String cb){
        return fdrService.getCurrentsavingsOfCB(cb);
    }

    @GetMapping(value = "/fdr/info/{cb}/{accountNo}")
    public Object getInfo(@PathVariable("cb") final String cb, @PathVariable("accountNo") final String accountNo){
        return fdrService.fdrInfoOfAccount( accountNo, cb);
    }
}
