package com.babl.citybank.modules.workflow.controllers;

import com.babl.citybank.datasources.oracle.workflow.schemas.ApplicationRemarks;
import com.babl.citybank.modules.workflow.services.ApplicationRemarksService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ApplicationRemarksController {
    private final ApplicationRemarksService applicationRemarksService;

    @PostMapping("/appRemarkSave/{remark}/{appId}")
    public Map applicationRemarksSave(@PathVariable("remark") String remark, @PathVariable("appId") int appId){
     return applicationRemarksService.applicationRemarkAdd(remark,appId);
    }
    @GetMapping("/appRemarkGet/{appId}")
    public List<ApplicationRemarks> getApplicationRemark(@PathVariable("appId") int appId){
        return applicationRemarksService.getApplicationRemark(appId);
    }


}
