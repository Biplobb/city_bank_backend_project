package com.babl.citybank.modules.workflow.dtos;

import lombok.Data;

import java.util.List;

@Data
public class BulkDeferralDTO {
    private int appId;
    private List<String> dueDate;
    private List<String> type;
}
