package com.babl.citybank.modules.workflow.controllers;

import com.babl.citybank.datasources.oracle.workflow.schemas.AppMessage;
import com.babl.citybank.modules.workflow.services.MessagingService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MessagingController {
    private final MessagingService messagingService;

    @PostMapping("/message/{delId}/{message}")
    public boolean sendMessage(@PathVariable("delId") int delId,
                               @PathVariable("message") String message){
        return messagingService.addMessage(delId, message);

    }

    @GetMapping("/message/{delId}")
    public List<AppMessage> getApplicationMessages(@PathVariable("delId") int delId){
        return messagingService.getApplicationMessages(delId);
    }
}
