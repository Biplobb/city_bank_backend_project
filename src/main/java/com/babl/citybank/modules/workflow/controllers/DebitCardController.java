package com.babl.citybank.modules.workflow.controllers;

import com.babl.citybank.common.AssignType;
import com.babl.citybank.datasources.oracle.workflow.schemas.Task;
import com.babl.citybank.modules.workflow.services.DebitCardService;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DebitCardController {
    private final DebitCardService debitCardService;

    @GetMapping("/debit/excel")
    public void getDebitExcel(final HttpServletResponse response) throws Exception{
        Workbook workbook = debitCardService.excelDebitCardList();

        response.addHeader("Content-disposition", "attachment;filename=sample.xlsx");
        response.setContentType("application/octet-stream");

        workbook.write(response.getOutputStream());

        response.flushBuffer();
    }

    @RequestMapping(value = "/debit/xml", method = RequestMethod.GET)
    public void getDebitXml(final HttpServletResponse response) throws Exception{
        String xmlFile = debitCardService.getDebitXml();
        response.addHeader("Content-disposition", "attachment;filename=sample.xml");
        response.getOutputStream().print(xmlFile);

        response.flushBuffer();
    }
}
