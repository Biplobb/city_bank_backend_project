package com.babl.citybank.modules.workflow.services;


import com.babl.citybank.common.*;
import com.babl.citybank.datasources.oracle.adminPanel.repositories.UserRepository;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.User;
import com.babl.citybank.datasources.oracle.master.schemas.BusinessDivisionMaster;
import com.babl.citybank.datasources.oracle.workflow.repositories.*;
import com.babl.citybank.datasources.oracle.workflow.schemas.*;
import com.babl.citybank.exceptions.*;
import com.babl.citybank.modules.workflow.utils.CyclicAssignmentUtil;
import com.babl.citybank.modules.workflow.utils.TriggerFunctions;
import com.babl.citybank.modules.workflow.utils.WorkflowUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.*;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class WorkflowService {
    private final TaskRepository taskRepository;
    private final HttpServletRequest httpServletRequest;
    private final UserRepository userRepository;
    private final ApplicationRepository applicationRepository;
    private final AppDelegationRepository appDelegationRepository;
    private final TaskRouteRepository taskRouteRepository;
    private final WorkflowUtils workflowUtils;
    private final TriggerFunctions triggerFunctions;
    private final AppFileRepository appFileRepository;
    private final CyclicAssignmentUtil cyclicAssignmentUtil;
    private final EntityManager entityManager;
    private final GroupRepository groupRepository;
    private final SdDepartmentTeamRepository sdDepartmentTeamRepository;
    private final SdDepartmentMasterRepository sdDepartmentMasterRepository;
    private final BranchMasterRepository branchMasterRepository;
    private final CardCallCategoryRepository cardCallCategoryRepository;
    private Group makerGroup;
    private Group checkerGroup;
    private Group approvalOfficerGroup;
    private Group cardDivision;

    @EventListener
    public void setCheckerGroup(ContextRefreshedEvent event){
        this.makerGroup = groupRepository.findByGroupName("MAKER");
        this.checkerGroup = groupRepository.findByGroupName("CHECKER");
        this.approvalOfficerGroup = groupRepository.findByGroupName("APPROVAL_OFFICER");
        this.cardDivision = groupRepository.findByGroupName("CARD_DIVISION");
    }


    public Object startCase(String title) {
        String username = httpServletRequest.getUserPrincipal().getName();
        User user = userRepository.findByUsername(username);

        if (user == null)
            throw new UserAccessException("");

        Task task = taskRepository.findByTitle(title);
        if (task == null || task.getStartable().equals("NO"))
            throw new StartCaseException("");

        List<Group> groupList = task.getGroupList();
        if (!groupList.contains(user.getGroup()))
            throw new UserAccessException("");

        Application application = new Application();
        AppDelegation appDelegation = new AppDelegation();
        LocalDateTime now = LocalDateTime.now();

        application.setAppInitUser(username);
        application.setAppCurrentUser(username);
        application.setAppStatus(AppStatus.ACTIVE);
        application.setParent(-1);
        application.setUrgency(0);
        application.setAppInitTimestamp(now);

        applicationRepository.save(application);

        appDelegation.setApplication(application);
        appDelegation.setUser(user);
        appDelegation.setUserGroupName(user.getGroup().getGroupName());
        appDelegation.setTask(task);
        appDelegation.setStatus(AppStatus.ACTIVE);
        appDelegation.setDelInitialTimestamp(now);

        appDelegationRepository.save(appDelegation);

        AppDelegation delegation = new AppDelegation();
        delegation.setId(appDelegation.getId());

        return delegation;

    }




    public Object getCaseVariables(int appId) {
        AppDelegation appDelegation = appDelegationRepository.findById(appId);
        Application application = appDelegation.getApplication();
        if (application == null)
            throw new CaseNotFoundException("");
        Map<String, String> map = workflowUtils.stringToMap(application.getAppData());
        Map<String, Object> returnMap = new HashMap<>();
        for (String key : map.keySet()){
            String val = map.get(key);
            if (val != null && val.contains("^"))
                val = val.replace("^", ",");

            Object returnVal = null;
            if (val != null) {
                if (val.equals("true"))
                    returnVal = true;
                else if (val.equals("false"))
                    returnVal = false;
                else
                    returnVal = val;

            }
            returnMap.put(key, returnVal);
        }

        return returnMap;

    }

    public void setCaseVariables(int appId, Map<String, Object> map, boolean toSave) {
        AppDelegation appDelegation = appDelegationRepository.findById(appId);

        if (appDelegation == null || !appDelegation.getStatus().equals(AppStatus.ACTIVE))
            throw new CaseNotFoundException("");

        Application application = appDelegation.getApplication();
        if (application == null)
            throw new CaseNotFoundException("");


        String username = httpServletRequest.getUserPrincipal().getName();
        User user = userRepository.findByUsername(username);

        if (user == null)
            throw new UserAccessException("");

        String appData = application.getAppData();
        String newAppData = workflowUtils.mapAddToString(map, appData);
        application.setAppData(newAppData);
        Map<String, String> newMap = workflowUtils.stringToMap(newAppData);


        appDelegation.setAppData(newAppData);

        if (toSave) {
            appDelegation.setInboxStatus("SAVED");
            appDelegation.setServiceType(newMap.get("serviceType"));
            appDelegation.setSubserviceType(newMap.get("subServiceType"));
            application.setCbNumber(newMap.get("cbNumber"));
            application.setCustomerName(newMap.get("customerName"));
            application.setAccountNumber(newMap.get("accountNumber"));
        }
        applicationRepository.save(application);
        appDelegationRepository.save(appDelegation);
    }


    public void routeCase(int appId) {
        AppDelegation appDelegation = appDelegationRepository.findById(appId);

        if (appDelegation == null || !appDelegation.getStatus().equals(AppStatus.ACTIVE))
            throw new CaseNotFoundException("");

        Application application = appDelegation.getApplication();
        if (application == null)
            throw new CaseNotFoundException("");


        String username = httpServletRequest.getUserPrincipal().getName();
        User user = userRepository.findByUsername(username);

        if (user == null)
            throw new UserAccessException("");


        Task currentTask = appDelegation.getTask();


        String currentAppData = appDelegation.getAppData();
        Map<String, String> caseVariableMap = workflowUtils.stringToMap(currentAppData);


        List<TaskRoute> nextPossibleTaskList = taskRouteRepository.findByTaskFrom(currentTask);
        List<Task> nextTaskList = new ArrayList<>();
        List<AssignType> assignTypeList = new ArrayList<>();
        List<String> assignVariableList = new ArrayList<>();




        for (TaskRoute taskRoute : nextPossibleTaskList) {
            System.out.println(taskRoute.getTaskFrom().getTitle());
            if (taskRoute.getRouteType().equals(RouteType.ALWAYS)) {
                nextTaskList.add(taskRoute.getTaskTo());
                assignTypeList.add(taskRoute.getAssignType());
                assignVariableList.add(taskRoute.getAssignVariable());
            }
            else if (taskRoute.getRouteType().equals(RouteType.CONDITIONAL)) {
                if (workflowUtils.checkCondition(taskRoute.getRouteCondition(), caseVariableMap)){
                    nextTaskList.add(taskRoute.getTaskTo());
                    assignTypeList.add(taskRoute.getAssignType());
                    assignVariableList.add(taskRoute.getAssignVariable());
                }
            }
            else if (taskRoute.getRouteType().equals(RouteType.SEC_JOIN)){
                String queryString = "select count(id) from app_delegation where task_id in(" +
                        " select id from task where title in( select task_from from task_route " +
                        " where task_to = '" + taskRoute.getTaskTo().getTitle() + "' and route_type = 'SEC_JOIN'))" +
                        " and app_id = '" + application.getId() + "' and status = 'ACTIVE'";
                Query query = entityManager.createNativeQuery(queryString);
                int numberOfThreadRemaining = ((BigDecimal) query.getSingleResult()).intValue();

                appDelegation.setStatus(AppStatus.CLOSED);
                appDelegation.setDelFinishTimestamp(LocalDateTime.now());
                appDelegationRepository.save(appDelegation);

                if (numberOfThreadRemaining == 1) {
                    nextTaskList.add(taskRoute.getTaskTo());
                    assignTypeList.add(taskRoute.getAssignType());
                    assignVariableList.add(taskRoute.getAssignVariable());
                }
                else {
                    return;
                }
            }
        }

        triggerFunctions.branchFlowTrigger(application, appDelegation, user, caseVariableMap, currentAppData, currentTask.getTitle());


        appDelegation.setStatus(AppStatus.CLOSED);
        appDelegation.setDelFinishTimestamp(LocalDateTime.now());
        appDelegationRepository.save(appDelegation);

        int index = 0;

        for (Task task : nextTaskList) {
            if (task == null) {
                List<AppDelegation> appDelegationList = appDelegationRepository.findByApplicationAndStatus(application, AppStatus.ACTIVE);
                if (appDelegationList.size() == 0){
                    application.setAppStatus(AppStatus.CLOSED);
                    application.setAppFinishTimestamp(LocalDateTime.now());
                }
                return;
            }

            AssignType assignType = assignTypeList.get(index);
            String assignVar = assignVariableList.get(index);

            User nextUser = null;

            if (assignType.equals(AssignType.EVALUATE)) {
                String nextUsername = caseVariableMap.get(assignVar);
                if (nextUsername == null)
                    throw new VariableNotSetException("Next user variable not set.");


                nextUser = userRepository.findByUsername(nextUsername);

                if (nextUser == null)
                    throw new VariableNotSetException("Next user not found");

                if (!task.getGroupList().contains(nextUser.getGroup()))
                    throw new UserAccessException("");

            } else if (assignType.equals(AssignType.SELF_SERVICE)) {
                //next user is null - claimable
                ///assign group table and condition will be used to claim the case
            } else if (assignType.equals(AssignType.CYCLICAL)){
                String nextUsername = cyclicAssignmentUtil.getNextUser();
                if (nextUsername == null)
                    throw new UserAccessException("");

                nextUser = userRepository.findByUsername(nextUsername);

                if (nextUser == null)
                    throw new VariableNotSetException("Next user not found");

                if (!task.getGroupList().contains(nextUser.getGroup()))
                    throw new UserAccessException("");
            }


            AppDelegation newAppDelegation = new AppDelegation();
            newAppDelegation.setApplication(application);
            newAppDelegation.setTask(task);
            newAppDelegation.setUser(nextUser);
            newAppDelegation.setSentByUsername(username);
            newAppDelegation.setSentByRole(user.getGroup().getGroupName());
            if (nextUser != null){
                newAppDelegation.setUserGroupName(nextUser.getGroup().getGroupName());
            }
            newAppDelegation.setAppData(appDelegation.getAppData());
            newAppDelegation.setStatus(AppStatus.ACTIVE);
            newAppDelegation.setDelInitialTimestamp(LocalDateTime.now());

            triggerFunctions.postRoutingTrigger(caseVariableMap, appDelegation, newAppDelegation);

            appDelegationRepository.save(newAppDelegation);

            index++;
        }

    }


    public void claimCase(int appId) {
        AppDelegation appDelegation = appDelegationRepository.findById(appId);

        if (appDelegation == null || !appDelegation.getStatus().equals(AppStatus.ACTIVE))
            throw new CaseNotFoundException("");

        Application application = appDelegation.getApplication();
        if (application == null)
            throw new CaseNotFoundException("");


        String username = httpServletRequest.getUserPrincipal().getName();
        User user = userRepository.findByUsername(username);

        if (user == null)
            throw new UserAccessException("");

        if (!appDelegation.getTask().getGroupList().contains(user.getGroup())){
            throw new UserAccessException("");
        }

        appDelegation.setDelFinishTimestamp(LocalDateTime.now());

        AppDelegation newAppDelegation = new AppDelegation();
        newAppDelegation.setApplication(application);
        newAppDelegation.setTask(appDelegation.getTask());
        newAppDelegation.setUser(user);
        newAppDelegation.setSentByUsername(appDelegation.getSentByUsername());
        newAppDelegation.setSentByRole(appDelegation.getSentByRole());
        newAppDelegation.setUserGroupName(user.getGroup().getGroupName());
        newAppDelegation.setAppData(appDelegation.getAppData());
        newAppDelegation.setStatus(AppStatus.ACTIVE);
        newAppDelegation.setDelInitialTimestamp(LocalDateTime.now());
        newAppDelegation.setServiceType(appDelegation.getServiceType());
        newAppDelegation.setSubserviceType(appDelegation.getSubserviceType());

        appDelegation.setStatus(AppStatus.CLOSED);
        appDelegationRepository.save(appDelegation);
        appDelegationRepository.save(newAppDelegation);

    }


    public void claimMultipleCase(List<Integer> appIdList){
        for (Integer appId : appIdList){
            try {
                claimCase(appId);
            }
            catch (Exception e){
                log.info(e.toString());
            }
        }
    }


    public void reassignCase(int appId, String username){
        AppDelegation appDelegation = appDelegationRepository.findById(appId);

        if (appDelegation == null || !appDelegation.getStatus().equals(AppStatus.ACTIVE))
            throw new CaseNotFoundException("");

        Application application = appDelegation.getApplication();
        if (application == null)
            throw new CaseNotFoundException("");


        User user = userRepository.findByUsername(username);

        if (user == null)
            throw new UserAccessException("");

        if (!appDelegation.getTask().getGroupList().contains(user.getGroup())){
            throw new UserAccessException("");
        }

        appDelegation.setUser(user);
        appDelegationRepository.save(appDelegation);
    }

    private Map caseList(Application application, AppDelegation appDelegation, String taskTitle){
        Map<String, Object> map = new HashMap<>();
        map.put("appId", appDelegation.getId());
        map.put("appTitle", application.getAppTitle());
        map.put("cbNumber", application.getCbNumber());
        map.put("accountNumber", application.getAccountNumber());
        map.put("customerName", application.getCustomerName());
        map.put("solId", application.getSolId());
        map.put("serviceType", appDelegation.getServiceType());
        map.put("subServiceType", appDelegation.getSubserviceType());
        map.put("appInitTime", application.getAppInitTimestamp());
        map.put("delInitTime", appDelegation.getDelInitialTimestamp());
        map.put("urgency", application.getUrgency());
        map.put("taskTitle", taskTitle);
        map.put("sentByUsername", appDelegation.getSentByUsername());
        map.put("sentByRole", appDelegation.getSentByRole());

        return map;
    }

    private List<Map> getInboxOutInbox(String inboxOutInbox){
        String username = httpServletRequest.getUserPrincipal().getName();
        User user = userRepository.findByUsername(username);

        if (user == null)
            throw new UserAccessException("");

        List<AppDelegation> appDelegationList = appDelegationRepository.findByStatusAndUserOrderById(AppStatus.ACTIVE, user);
        List<Map> inboxList = new ArrayList<>();


        for (AppDelegation appDelegation : appDelegationList) {
            Application application = appDelegation.getApplication();
            Task task = appDelegation.getTask();

            if (appDelegation.getServiceType() == null)
                continue;

            if (inboxOutInbox.equals("INBOX")){
                if (task.getOutInbox() == null) {
                    inboxList.add(caseList(application, appDelegation, task.getTitle()));
                }
            }
            else {
                if (task.getOutInbox() != null && task.getOutInbox().equals(inboxOutInbox)) {
                    inboxList.add(caseList(application, appDelegation, task.getTitle()));
                }
            }


        }

        return inboxList;
    }

    public List<Map> getInboxCases() {
        return getInboxOutInbox("INBOX");
    }

    public List<Map> getBMBulkAccountOpening(){
        return getInboxOutInbox("BULK_BM_ACCOUNT_OPENING");
    }

    public List<Map> getInboxCategoryWise(String category){
        String username = httpServletRequest.getUserPrincipal().getName();
        User user = userRepository.findByUsername(username);

        if (user == null)
            throw new UserAccessException("");

        List<AppDelegation> appDelegationList = appDelegationRepository.findByStatusAndUserOrderById(AppStatus.ACTIVE, user);
        List<Map> inboxList = new ArrayList<>();


        for (AppDelegation appDelegation : appDelegationList) {
            Application application = appDelegation.getApplication();
            Task task = appDelegation.getTask();
            String inboxStatus = appDelegation.getInboxStatus();

            if (appDelegation.getServiceType() == null)
                continue;

            boolean condition;
            if (category.equals("SELF"))
                condition = (inboxStatus == null);
            else
                condition = category.equals(inboxStatus);

            if (task.getOutInbox() == null && condition) {
                inboxList.add(caseList(application, appDelegation, task.getTitle()));
            }

        }

        return inboxList;
    }

    private List<Map> getTeamClaimableCase(SdDepartmentTeam sdDepartmentTeam){
        String teamName = sdDepartmentTeam.getTeamName();
        List<AppDelegation> appDelegationList = appDelegationRepository.findByStatusAndUserOrderById(AppStatus.ACTIVE, null);
        List<Map> claimList = new ArrayList<>();


        for (AppDelegation appDelegation : appDelegationList) {
            Application application = appDelegation.getApplication();
            Map<String, String> appData = workflowUtils.stringToMap(application.getAppData());

            appData.put("serviceType", appDelegation.getServiceType());
            appData.put("subServiceType", appDelegation.getSubserviceType());

            boolean condition = false;
            Task task = appDelegation.getTask();

            if (task.getAssignDepartment().equals("YES")) {
                condition = teamName.equals(task.getAssignmentDepartmentName());
            }
            else if (sdDepartmentTeam.getAssignmentCondition() != null){
                condition = workflowUtils.checkCondition(sdDepartmentTeam.getAssignmentCondition(), appData);
            }

            if (condition) {
                claimList.add(caseList(application, appDelegation, task.getTitle()));
            }
        }

        return claimList;
    }

    public List<Map> getClaimableCase(){
        String username = httpServletRequest.getUserPrincipal().getName();
        User user = userRepository.findByUsername(username);

        if (user == null || user.getWorkplace() == null)
            throw new UserAccessException("");

        String teamName = null;
        SdDepartmentTeam sdDepartmentTeam = null;
        String userGroup = user.getGroup().getGroupName();

        if (user.getWorkplace().equals("SD") && userGroup.equals("SD_ADMIN")) {
            List<Map> mapList = new ArrayList<>();
            SdDepartmentMaster sdDepartmentMaster = user.getSdDepartment();
            if (sdDepartmentMaster == null)
                throw new UserAccessException("");

            List<SdDepartmentTeam> sdDepartmentTeamList = sdDepartmentTeamRepository.findBySdDepartment(sdDepartmentMaster);
            for (SdDepartmentTeam team : sdDepartmentTeamList){
                mapList.addAll(getTeamClaimableCase(team));
            }

            return mapList;
        }
        else{
            sdDepartmentTeam = user.getSdDepartmentTeam();

            if (sdDepartmentTeam == null)
                throw new UserAccessException("");
            return getTeamClaimableCase(sdDepartmentTeam);
        }

    }


    public List<Map> getCardMaintenanceClaimable(){
        String username = httpServletRequest.getUserPrincipal().getName();
        User user = userRepository.findByUsername(username);

        if (user == null || user.getWorkplace() == null)
            throw new UserAccessException("");

        List<AppDelegation> appDelegationList = appDelegationRepository.findByStatusAndUserOrderById(AppStatus.ACTIVE, null);
        List<Map> claimableList = new ArrayList<>();
        if (user.getWorkplace().equals("CARD_DIVISION")){
            if (user.getGroup().getGroupName().equals("CARD_DIVISION")){
                CardMaintenanceTeam cardMaintenanceTeam = user.getCardMaintenanceTeam();
                List<CardCallCategory> cardCallCategories = cardCallCategoryRepository.findByCardMaintenanceTeam(cardMaintenanceTeam);

                //System.out.println(cardCallCategories.size());

                Set<String> callCategorySet = new HashSet<>();

                for (CardCallCategory cardCallCategory : cardCallCategories){
                    callCategorySet.add(cardCallCategory.getCallCategory());
                }

                //System.out.println(callCategorySet);


                for (AppDelegation appDelegation : appDelegationList){
                    if (callCategorySet.contains(appDelegation.getServiceType())){
                        claimableList.add(caseList(appDelegation.getApplication(), appDelegation, appDelegation.getTask().getTitle()));
                    }
                }

            }
            else if (user.getGroup().getGroupName().equals("ADMIN")){
                for (AppDelegation appDelegation : appDelegationList){
                    claimableList.add(caseList(appDelegation.getApplication(), appDelegation, appDelegation.getTask().getTitle()));
                }
            }
        }
        return claimableList;
    }


    public List<String> getNextUserList(String userRole){
        String username = httpServletRequest.getUserPrincipal().getName();
        User user = userRepository.findByUsername(username);

        if (user == null)
            throw new UserAccessException("");

        List<User> nextUsers = new ArrayList<>();

        if (userRole.equals("CHECKER"))
            nextUsers = userRepository.findByGroupAndSdDepartmentTeam(checkerGroup, user.getSdDepartmentTeam());
        else if (userRole.equals("APPROVAL_OFFICER")){
            BusinessDivisionMaster businessDivision = user.getCsu().getBusinessDivision();
            nextUsers = userRepository.findByGroupAndBusinessDivision(approvalOfficerGroup, businessDivision);
        }


        List<String> ll = new ArrayList<>();
        for (User checker : nextUsers){
            ll.add(checker.getUsername());
        }

        return ll;
    }


    public void uploadMultipleFile(int appId, String[] type, MultipartFile[] multipartFiles){
        if (type.length != multipartFiles.length)
            throw new VariableNotSetException("Type and file length is not equal");

        AppDelegation appDelegation = appDelegationRepository.findById(appId);
        if (appDelegation == null)
            throw new CaseNotFoundException("");

        Application application = appDelegation.getApplication();


        String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
        String dir = null;
        if (OS.contains("win")) {
            dir = "C:\\Users\\BABL\\Desktop\\city-bank\\upload\\";
        } else if (OS.contains("nux")) {
            dir = "/home/babl/upload/";
        }

        int len = multipartFiles.length;

        for (int i = 0; i < len ; i++){
            try {
                MultipartFile multipartFile  = multipartFiles[i];
                String fileName = appId + "_" + type[i];

                String orgName = multipartFile.getOriginalFilename();
                int lastPos =orgName.lastIndexOf(".");
                String extension = orgName.substring(lastPos);

                String path = dir + fileName + extension;

                File file = new File(path);
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                fileOutputStream.write(multipartFile.getBytes());
                fileOutputStream.flush();
                fileOutputStream.close();

                AppFile appFile = new AppFile();
                appFile.setApplication(application);
                appFile.setFileName(fileName + extension);
                appFile.setPath(path);
                appFile.setType(type[i]);

                appFileRepository.save(appFile);

            }
            catch (Exception e){
                e.printStackTrace();
            }

        }

    }

    public List<String> getCaseFileName(int appId){
        AppDelegation appDelegation = appDelegationRepository.findById(appId);
        if (appDelegation == null)
            throw new CaseNotFoundException("");

        Application application = appDelegation.getApplication();

        List<AppFile> appFileList = appFileRepository.findByApplication(application);
        List<String> files = new ArrayList<>();

        for (AppFile appFile : appFileList){
            files.add(appFile.getFileName());
        }

        return files;
    }

    public Resource getCaseFile(String fileName) throws Exception{
        if (fileName == null || fileName.equals("undefined"))
            throw new FormatException("Invalid file name: may be undefined");
        String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
        String dir = null;
        if (OS.contains("win")) {
            dir = "file:///C:/Users/BABL/Desktop/city-bank/upload/";
        } else if (OS.contains("nux")) {
            dir = "file:////home/babl/upload/";
        }

        String path = dir + fileName;

        return new UrlResource(path);
    }

    public List<String> splitPdf(MultipartFile multipartFiles, int appId) throws Exception{
        AppDelegation appDelegation = appDelegationRepository.findById(appId);
        if (appDelegation == null)
            throw new CaseNotFoundException("");

        Application application = appDelegation.getApplication();

        PDDocument document = PDDocument.load(multipartFiles.getInputStream());
        PDFRenderer pdfRenderer = new PDFRenderer(document);

        String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
        String dir = null;
        if (OS.contains("win")) {
            dir = "C:\\Users\\BABL\\Desktop\\city-bank\\upload\\";
        } else if (OS.contains("nux")) {
            dir = "/home/babl/upload/";
        }


        int page = 0;

        List<String> fileNames = new ArrayList<>();

        for (page = 0; page < document.getNumberOfPages(); page++){
            //BufferedImage image = pdPage.convertToImage(BufferedImage.TYPE_INT_RGB, 300);
            /*File file = new File(dir + appId + "_" + page + ".jpg");

            FileOutputStream fileOutputStream = new FileOutputStream(file);

            JPEGImageEncoder jpegEncoder = JPEGCodec.createJPEGEncoder(fileOutputStream);
            JPEGEncodeParam jpegEncodeParam = jpegEncoder.getDefaultJPEGEncodeParam(image);
            jpegEncodeParam.setDensityUnit(JPEGEncodeParam.DENSITY_UNIT_DOTS_INCH);
            jpegEncodeParam.setXDensity(300);
            jpegEncodeParam.setYDensity(300);
            jpegEncoder.encode(image, jpegEncodeParam);
            fileOutputStream.close();
            */

            String fileName = application.getId() + "_" + page + ".jpg";

            fileNames.add(fileName);

            File file = new File(dir + fileName);

            BufferedImage bufferedImage = pdfRenderer.renderImageWithDPI(page, 150);
            ImageIO.write(bufferedImage, "jpg", file);


        }
        document.close();

        return fileNames;
    }

    public Object renameFiles(int appId, Map<String, String> fileNames){
        AppDelegation appDelegation = appDelegationRepository.findById(appId);
        if (appDelegation == null)
            throw new CaseNotFoundException("");

        Application application = appDelegation.getApplication();

        String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
        String dir = null;
        if (OS.contains("win")) {
            dir = "C:\\Users\\BABL\\Desktop\\city-bank\\upload\\";
        } else if (OS.contains("nux")) {
            dir = "/home/babl/upload/";
        }

        int applicationId = application.getId();

        for (String oldName : fileNames.keySet()){
            File old = new File(dir + oldName);
            File newFile = new File(dir + applicationId + "_" + fileNames.get(oldName) + ".jpg");

            old.renameTo(newFile);

            AppFile appFile = new AppFile();
            appFile.setApplication(application);
            appFile.setFileName(applicationId + "_" + fileNames.get(oldName) + ".jpg");
            appFile.setPath(dir + applicationId + "_" + fileNames.get(oldName) + ".jpg");
            appFile.setType(fileNames.get(oldName));

            appFileRepository.save(appFile);
        }

        return getCaseFileName(appId);
    }


    public List<String> getSdTeams(){
        String username = httpServletRequest.getUserPrincipal().getName();
        User user = userRepository.findByUsername(username);

        if (user == null || !user.getGroup().getGroupName().equals("SD_ADMIN"))
            throw new UserAccessException("");

        SdDepartmentMaster sdDepartmentMaster = user.getSdDepartment();
        if (sdDepartmentMaster == null)
            throw new ObjectNotFoundException("Sd Department not found");

        List<SdDepartmentTeam> sdDepartmentTeams = sdDepartmentTeamRepository.findBySdDepartment(sdDepartmentMaster);

        List<String> teams = new ArrayList<>();
        for (SdDepartmentTeam sdDepartmentTeam : sdDepartmentTeams){
            teams.add(sdDepartmentTeam.getTeamName());
        }

        return teams;

    }

    public List<String> usersToAssignSD(String teamName){
        String username = httpServletRequest.getUserPrincipal().getName();
        User user = userRepository.findByUsername(username);

        if (user == null || !user.getGroup().getGroupName().equals("SD_ADMIN"))
            throw new UserAccessException("");

        SdDepartmentMaster sdDepartmentMaster = user.getSdDepartment();
        if (sdDepartmentMaster == null)
            throw new ObjectNotFoundException("Sd Department not found");

        SdDepartmentTeam sdDepartmentTeam = sdDepartmentTeamRepository.findBySdDepartmentAndTeamName(sdDepartmentMaster, teamName);
        if (sdDepartmentTeam == null)
            throw new ObjectNotFoundException("Sd Department team not found");

        List<User> userList = userRepository.findByGroupAndSdDepartmentTeam(makerGroup, sdDepartmentTeam);
        List<String> userNames = new ArrayList<>();

        for (User maker : userList){
            userNames.add(maker.getUsername());
        }

        return userNames;

    }


    public List<String> getAbsCheckers(){
        BranchMaster branchMaster = branchMasterRepository.findById(21);
        List<User> userList = userRepository.findByGroupAndBranch(checkerGroup, branchMaster);

        List<String> usernames = new ArrayList<>();

        for (User user : userList){
            usernames.add(user.getUsername());
        }

        return usernames;
    }



}
