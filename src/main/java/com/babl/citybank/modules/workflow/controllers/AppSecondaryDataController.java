package com.babl.citybank.modules.workflow.controllers;

import com.babl.citybank.modules.workflow.dtos.SecondaryCBInfoDto;
import com.babl.citybank.modules.workflow.services.AppSecondaryDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AppSecondaryDataController {
    private final AppSecondaryDataService appSecondaryDataService;

    @GetMapping("/secondaryCB/{appId}/{person}")
    public Map getSecondaryCbInfo(@PathVariable("appId") final int appId, @PathVariable("person") String person){
        return appSecondaryDataService.getSecondaryCBInfo(appId, person);
    }

    @GetMapping("/secondaryCB/{appId}")
    public List<Map> getAllCB(@PathVariable("appId") final int appId){
        return appSecondaryDataService.getTaggedCB(appId);
    }

    @PostMapping("/secondaryCB")
    public boolean saveSecondaryInfo(@RequestBody SecondaryCBInfoDto secondaryCBInfoDto){
        return appSecondaryDataService.saveSecondaryCBInfo(secondaryCBInfoDto);
    }
}
