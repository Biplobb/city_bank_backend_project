package com.babl.citybank.modules.workflow.controllers;

import com.babl.citybank.modules.workflow.dtos.ApproveDeferralDTO;
import com.babl.citybank.modules.workflow.dtos.BulkDeferralDTO;
import com.babl.citybank.modules.workflow.dtos.CreateDeferralDTO;
import com.babl.citybank.modules.workflow.resources.DeferralResource;
import com.babl.citybank.modules.workflow.services.DeferralService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DeferralController {
    private final DeferralService deferralService;

    @PostMapping("/deferral/create")
    public boolean createDeferral(@RequestBody CreateDeferralDTO createDeferralDTO){
        return deferralService.applyForDeferral(createDeferralDTO);
    }

    @PostMapping("/deferral/create/bulk")
    public void createDeferralBulk(@RequestBody BulkDeferralDTO bulkDeferralDTO){
        for (int i =0; i< bulkDeferralDTO.getDueDate().size(); i++){
            CreateDeferralDTO createDeferralDTO = new CreateDeferralDTO();
            createDeferralDTO.setAppId(bulkDeferralDTO.getAppId());
            createDeferralDTO.setDueDate(bulkDeferralDTO.getDueDate().get(i));
            createDeferralDTO.setType(bulkDeferralDTO.getType().get(i));
            deferralService.applyForDeferral(createDeferralDTO);
        }
    }


    @GetMapping("/case/deferral/{appId}")
    public List<DeferralResource> getDeferralOfCase(@PathVariable("appId") int appId){
        return deferralService.getDeferralOfApplication(appId);
    }

    @PostMapping("/deferral/approval")
    public boolean deferralApproval(@RequestBody @Validated ApproveDeferralDTO approveDeferralDTO){
        return deferralService.approveDeferral(approveDeferralDTO);
    }

    @PostMapping("/deferral/upload")
    public boolean uploadFileToDeferral(@RequestParam("deferralId") int deferralId,
                                        @RequestParam("file")MultipartFile multipartFile){
        return deferralService.uploadFileToDeferral(multipartFile, deferralId);
    }

    @GetMapping("/deferral/closable")
    public List<Map> getClosableDeferral(){
        return deferralService.getDeferralForClosing();
    }

    @PostMapping("/deferral/close/{deferralId}")
    public boolean closeDeferral(@PathVariable("deferralId") int deferralId){
        return deferralService.closeDeferral(deferralId);
    }

    @PostMapping("/deferral/raiseSd")
    public boolean raiseFromSD(@RequestBody CreateDeferralDTO createDeferralDTO){
        return deferralService.raiseDeferralFromSD(createDeferralDTO);
    }

    @PostMapping("/deferral/raisedSd/Bulk")
    public void raiseFromSdBulk(@RequestBody BulkDeferralDTO bulkDeferralDTO){
        for (int i =0; i< bulkDeferralDTO.getDueDate().size(); i++){
            CreateDeferralDTO createDeferralDTO = new CreateDeferralDTO();
            createDeferralDTO.setAppId(bulkDeferralDTO.getAppId());
            createDeferralDTO.setDueDate(bulkDeferralDTO.getDueDate().get(i));
            createDeferralDTO.setType(bulkDeferralDTO.getType().get(i));
            deferralService.applyForDeferral(createDeferralDTO);
        }
    }

    @GetMapping("/deferral/CSEnd")
    public List<DeferralResource> getDeferralResourcesCS(){
        return deferralService.getDeferralListInCsEnd();
    }
}
