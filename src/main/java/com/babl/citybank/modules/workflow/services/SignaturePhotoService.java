package com.babl.citybank.modules.workflow.services;

import com.babl.citybank.datasources.oracle.workflow.repositories.AppDelegationRepository;
import com.babl.citybank.datasources.oracle.workflow.repositories.AppFileRepository;
import com.babl.citybank.datasources.oracle.workflow.repositories.ApplicationRepository;
import com.babl.citybank.datasources.oracle.workflow.schemas.AppDelegation;
import com.babl.citybank.datasources.oracle.workflow.schemas.AppFile;
import com.babl.citybank.datasources.oracle.workflow.schemas.Application;
import com.babl.citybank.exceptions.CaseNotFoundException;
import com.babl.citybank.modules.workflow.dtos.SignaturePhotoDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Base64;
import java.util.Locale;

@Transactional
@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SignaturePhotoService {
    private final AppFileRepository appFileRepository;
    private final ApplicationRepository applicationRepository;
    private final AppDelegationRepository appDelegationRepository;
    private final WorkflowService workflowService;

    public boolean uploadSignaturePhoto(SignaturePhotoDTO signaturePhotoDTO){
        AppDelegation appDelegation = appDelegationRepository.findById(signaturePhotoDTO.getAppId());
        if (appDelegation == null)
            throw new CaseNotFoundException("");

        Application application = appDelegation.getApplication();

        String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
        String dir = null;
        if (OS.contains("win")) {
            dir = "C:\\Users\\BABL\\Desktop\\city-bank\\upload\\";
        } else if (OS.contains("nux")) {
            dir = "/home/babl/upload/";
        }

        int applicationId = application.getId();

        String imageData = signaturePhotoDTO.getImage().split(",")[1];

        byte[] data = Base64.getDecoder().decode(imageData);
        try{
            String fileName = applicationId + "_" + signaturePhotoDTO.getType() + ".jpg";
            String path = dir + fileName;
            File file = new File(path);
            OutputStream outputStream = new FileOutputStream(file);
            outputStream.write(data);
            outputStream.flush();
            outputStream.close();

            AppFile appFile = new AppFile();
            appFile.setApplication(application);
            appFile.setFileName(fileName);
            appFile.setPath(path);
            appFile.setType(signaturePhotoDTO.getType());
            appFileRepository.save(appFile);

        }
        catch (Exception e){
            System.out.println(e);
        }
        return true;
    }


    public Object signatureName(String type, int appId) throws Exception{
        AppDelegation appDelegation = appDelegationRepository.findById(appId);
        if (appDelegation == null)
            throw new CaseNotFoundException("");

        Application application = appDelegation.getApplication();

        String  fileName = application.getId()+"_"+type+".jpg";
        return workflowService.getCaseFile(fileName);
    }
}
