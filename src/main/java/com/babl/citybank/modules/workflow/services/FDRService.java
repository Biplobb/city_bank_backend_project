package com.babl.citybank.modules.workflow.services;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FDRService {
    private final EntityManager entityManager;

    public List<Map> getCurrentsavingsOfCB(String cb){
        String queryString = "select ACCOUNTNO, CUST_ID, PRODUCTNAME, PRODUCTCODE from listofaccount where cust_id = '" + cb + "' and ( schm_type = 'SBA' or schm_type = 'CAA')";
        Query query = entityManager.createNativeQuery(queryString);

        List<Object[]> accountList = query.getResultList();
        List<Map> mapList = new ArrayList<>();
        for (Object[] account : accountList){
            Map map = new HashMap();
            map.put("accountNo", account[0]);
            map.put("cb", account[1]);
            map.put("productName", account[2]);
            map.put("productCode", account[3]);

            mapList.add(map);
        }
        return mapList;
    }

    public Object fdrInfoOfAccount(String accountNo, String cb){
        String cbQuery = "select sz_full_name, sz_tin_number, cust_comu_addr1, cust_sector_code, cust_sub_sector_code, cust_occp_code, cust_comu_phone_num from dedup_temp_applicant_ind where maincustid = '" + cb + "'";
        String balanceQuery = "select availablebalance from currentsavings where accountno = '" + accountNo + "'";

        Query getCbInfo = entityManager.createNativeQuery(cbQuery);
        List<Object[]> cbInfo = getCbInfo.getResultList();

        Query getBalance = entityManager.createNativeQuery(balanceQuery);
        List<Object[]> balance = getBalance.getResultList();

        Map<String, Object> map = new HashMap<>();

        if (cbInfo.size() > 0){
            map.put("name", cbInfo.get(0)[0]);
            map.put("tinNumber", cbInfo.get(0)[1]);
            map.put("address", cbInfo.get(0)[2]);
            map.put("sector", cbInfo.get(0)[3]);
            map.put("subSector", cbInfo.get(0)[4]);
            map.put("occupation", cbInfo.get(0)[5]);
            map.put("phone", cbInfo.get(0)[6]);
        }

        Object balanceAmmount = null;

        if (balance.size() > 0){
            balanceAmmount = balance.get(0);
        }

        map.put("availableBalance", balanceAmmount);

        return map;
    }
}
