package com.babl.citybank.modules.workflow.services;

import com.babl.citybank.common.DeferralStatus;
import com.babl.citybank.common.Status;
import com.babl.citybank.datasources.oracle.workflow.repositories.AppDelegationRepository;
import com.babl.citybank.datasources.oracle.workflow.repositories.AppFileRepository;
import com.babl.citybank.datasources.oracle.workflow.schemas.AppDelegation;
import com.babl.citybank.datasources.oracle.workflow.schemas.AppFile;
import com.babl.citybank.exceptions.UserAccessException;
import com.babl.citybank.modules.workflow.dtos.ApproveDeferralDTO;
import com.babl.citybank.modules.workflow.dtos.CreateDeferralDTO;
import com.babl.citybank.datasources.oracle.workflow.repositories.ApplicationRepository;
import com.babl.citybank.datasources.oracle.workflow.repositories.DeferralRepository;
import com.babl.citybank.datasources.oracle.workflow.schemas.Application;
import com.babl.citybank.datasources.oracle.workflow.schemas.Deferral;
import com.babl.citybank.exceptions.CaseNotFoundException;
import com.babl.citybank.modules.workflow.resources.DeferralResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DeferralService {
    private final DeferralRepository deferralRepository;
    private final ApplicationRepository applicationRepository;
    private final HttpServletRequest httpServletRequest;
    private final AppFileRepository appFileRepository;
    private final AppDelegationRepository appDelegationRepository;

    private Deferral createDeferral(CreateDeferralDTO createDeferralDTO){
        AppDelegation appDelegation = appDelegationRepository.findById(createDeferralDTO.getAppId());
        if (appDelegation == null)
            throw new CaseNotFoundException("");

        Application application = appDelegation.getApplication();

        String username = httpServletRequest.getUserPrincipal().getName();

        Deferral deferral = new Deferral();
        deferral.setApplication(application);
        deferral.setTargetTo(application.getAppInitUser());
        deferral.setType(createDeferralDTO.getType());

        String dueDate = createDeferralDTO.getDueDate().split(",")[0];
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy");
        deferral.setDueDate(LocalDate.parse(dueDate, formatter));

        deferral.setDuration(createDeferralDTO.getDuration());
        deferral.setRemarks(createDeferralDTO.getRemarks());
        deferral.setStatus(DeferralStatus.APPROVAL_WAITING);
        deferral.setApplicationDate(LocalDate.now());
        deferral.setAppliedBy(username);

        return deferral;
    }

    public boolean applyForDeferral(CreateDeferralDTO createDeferralDTO){
        Deferral deferral = createDeferral(createDeferralDTO);
        deferralRepository.save(deferral);

        return true;

    }

    private DeferralResource deferralResourceFromDeferral(Deferral deferral){
        DeferralResource deferralResource = new DeferralResource();
        deferralResource.setId(deferral.getId());
        deferralResource.setType(deferral.getType());
        deferralResource.setDueDate(deferral.getDueDate());
        deferralResource.setDuration(deferral.getDuration());
        deferralResource.setRemarks(deferral.getRemarks());
        deferralResource.setAppliedBy(deferral.getAppliedBy());
        deferralResource.setApplicationDate(deferral.getApplicationDate());
        deferralResource.setApprovedBy(deferral.getApprovedBy());
        deferralResource.setApprovalDate(deferral.getApprovalDate());
        deferralResource.setClosedBy(deferral.getClosedBy());
        deferralResource.setClosedDate(deferral.getClosedDate());
        deferralResource.setStatus(deferral.getStatus());
        deferralResource.setAppId(deferral.getApplication().getId());

        String appFileName = deferral.getAppFile() == null ? null : deferral.getAppFile().getFileName();

        deferralResource.setAppFileName(appFileName);

        return deferralResource;
    }

    public List<DeferralResource> getDeferralOfApplication(int appId){
        AppDelegation appDelegation = appDelegationRepository.findById(appId);
        if (appDelegation == null)
            throw new CaseNotFoundException("");

        Application application = appDelegation.getApplication();

        List<Deferral> deferralList = deferralRepository.findByApplication(application);
        List<DeferralResource> deferralResources = new ArrayList<>();

        for (Deferral deferral : deferralList){
            DeferralResource deferralResource = deferralResourceFromDeferral(deferral);
            deferralResources.add(deferralResource);
        }

        return deferralResources;
    }


    public List<DeferralResource> getDeferralListInCsEnd(){
        String username = httpServletRequest.getUserPrincipal().getName();
        List<Deferral> deferralList = deferralRepository.findByTargetToAndStatus(username, DeferralStatus.ACTIVE);
        List<DeferralResource> deferralResources = new ArrayList<>();
        for (Deferral deferral : deferralList){
            deferralResources.add(deferralResourceFromDeferral(deferral));
        }
        return deferralResources;
    }

    public boolean approveDeferral(ApproveDeferralDTO approveDeferralDTO){
        AppDelegation appDelegation = appDelegationRepository.findById(approveDeferralDTO.getAppId());
        if (appDelegation == null){
            System.out.println();
            throw new CaseNotFoundException("");
        }

        Application application = appDelegation.getApplication();

        String username = httpServletRequest.getUserPrincipal().getName();

        DeferralStatus status = approveDeferralDTO.getApproval().equals("APPROVE") ? DeferralStatus.ACTIVE : DeferralStatus.NOTAPPROVED;
        List<Deferral> deferralList = deferralRepository.findByApplication(application);

        for (Deferral deferral : deferralList){
            deferral.setStatus(status);
            deferral.setApprovedBy(username);
            deferral.setApprovalDate(LocalDate.now());
            deferralRepository.save(deferral);
        }

        return true;
    }

    public boolean uploadFileToDeferral(MultipartFile multipartFile, int deferralId){
        Deferral deferral = deferralRepository.findById(deferralId);

        if (deferral == null)
            throw new CaseNotFoundException("");

        String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
        String dir = null;
        if (OS.contains("win")) {
            dir = "C:\\Users\\BABL\\Desktop\\city-bank\\upload\\";
        } else if (OS.contains("nux")) {
            dir = "/home/babl/upload/";
        }

        Application application = deferral.getApplication();


        try {
            String fileName = application.getId() + "_" + deferral.getType();

            String orgName = multipartFile.getOriginalFilename();
            int lastPos =orgName.lastIndexOf(".");
            String extension = orgName.substring(lastPos);

            String path = dir + fileName + extension;

            File file = new File(path);
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(multipartFile.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();

            AppFile appFile = new AppFile();
            appFile.setApplication(application);
            appFile.setFileName(fileName + extension);
            appFile.setPath(path);
            appFile.setType(deferral.getType());
            appFileRepository.save(appFile);

            deferral.setAppFile(appFile);
            deferral.setStatus(DeferralStatus.CLOSE_WAITING);

            deferralRepository.save(deferral);

        }
        catch (Exception e){
            log.info(e.toString());
            return false;
        }
        return true;
    }


    public List<Map> getDeferralForClosing(){
        String username = httpServletRequest.getUserPrincipal().getName();

        List<Deferral> deferralList = deferralRepository.findByApprovedByAndStatus(username, DeferralStatus.CLOSE_WAITING);
        List<Map> deferralResources = new ArrayList<>();

        for (Deferral deferral : deferralList){
            Map<String, Object> map = new HashMap<>();
            Application application = deferral.getApplication();

            map.put("id", deferral.getId());
            map.put("type", deferral.getType());
            map.put("dueDate", deferral.getDueDate());
            map.put("duration", deferral.getDuration());
            map.put("remarks", deferral.getRemarks());
            map.put("appliedBy", deferral.getAppliedBy());
            map.put("applicationDate", deferral.getApplicationDate());
            map.put("approvedBy", deferral.getApprovedBy());
            map.put("approvalDate", deferral.getApprovalDate());
            map.put("closedBy", deferral.getClosedBy());
            map.put("closedDate", deferral.getClosedDate());
            map.put("status", deferral.getStatus());
            map.put("appFileName", deferral.getAppFile().getFileName());
            map.put("appId", application.getId());
            map.put("customerName", application.getCustomerName());
            map.put("cbNumber", application.getCbNumber());
            map.put("accountNumber", application.getAccountNumber());

            deferralResources.add(map);
        }

        return deferralResources;

    }


    public boolean closeDeferral(int deferralId){
        Deferral deferral = deferralRepository.findById(deferralId);

        if (deferral == null)
            throw new CaseNotFoundException("");

        if (!deferral.getStatus().equals(DeferralStatus.CLOSE_WAITING))
            throw new UserAccessException("This deferral can not be closed");

        String username = httpServletRequest.getUserPrincipal().getName();

        deferral.setStatus(DeferralStatus.INACTIVE);
        deferral.setClosedBy(username);
        deferral.setClosedDate(LocalDate.now());
        deferralRepository.save(deferral);
        return true;
    }

    public boolean raiseDeferralFromSD(CreateDeferralDTO createDeferralDTO){
        Deferral deferral = createDeferral(createDeferralDTO);
        deferral.setStatus(DeferralStatus.ACTIVE);
        deferral.setApprovedBy(deferral.getAppliedBy());
        deferral.setApprovalDate(LocalDate.now());

        deferralRepository.save(deferral);

        return true;
    }

}
