package com.babl.citybank.modules.workflow.resources;

import com.babl.citybank.common.DeferralStatus;
import com.babl.citybank.common.Status;
import com.babl.citybank.datasources.oracle.workflow.schemas.AppFile;
import com.babl.citybank.datasources.oracle.workflow.schemas.Application;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
public class DeferralResource {
    private Integer id;

    private String type;

    private LocalDate dueDate;

    private Integer duration;

    private String remarks;

    private String appliedBy;

    private LocalDate applicationDate;

    private String approvedBy;

    private LocalDate approvalDate;

    private String closedBy;

    private LocalDate closedDate;

    private DeferralStatus status;

    private String appFileName;

    private int appId;
}
