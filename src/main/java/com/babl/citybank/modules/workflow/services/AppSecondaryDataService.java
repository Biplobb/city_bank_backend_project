package com.babl.citybank.modules.workflow.services;

import com.babl.citybank.datasources.oracle.workflow.repositories.AppDelegationRepository;
import com.babl.citybank.datasources.oracle.workflow.repositories.AppSecondaryDataRepository;
import com.babl.citybank.datasources.oracle.workflow.schemas.AppDelegation;
import com.babl.citybank.datasources.oracle.workflow.schemas.AppSecondaryData;
import com.babl.citybank.datasources.oracle.workflow.schemas.Application;
import com.babl.citybank.exceptions.CaseNotFoundException;
import com.babl.citybank.exceptions.ObjectNotFoundException;
import com.babl.citybank.modules.workflow.dtos.SecondaryCBInfoDto;
import com.babl.citybank.modules.workflow.utils.WorkflowUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AppSecondaryDataService {
    private final AppDelegationRepository appDelegationRepository;
    private final AppSecondaryDataRepository appSecondaryDataRepository;
    private final WorkflowUtils workflowUtils;

    public boolean saveSecondaryCBInfo(SecondaryCBInfoDto secondaryCBInfoDto){
        AppDelegation appDelegation = appDelegationRepository.findById(secondaryCBInfoDto.getAppId());

        if (appDelegation == null)
            throw new CaseNotFoundException("Case Not Found");
        Application application = appDelegation.getApplication();

        AppSecondaryData appSecondaryData = appSecondaryDataRepository.findByApplicationAndDataType(application, secondaryCBInfoDto.getPerson());
        if (appSecondaryData == null) {
            appSecondaryData = new AppSecondaryData();
            appSecondaryData.setApplication(application);
            appSecondaryData.setDataType(secondaryCBInfoDto.getPerson());
        }

        String appData = workflowUtils.mapAddToString(secondaryCBInfoDto.getAppData(), null);
        appSecondaryData.setSecondaryData(appData);
        appSecondaryData.setIdentifier(secondaryCBInfoDto.getCb());
        //appSecondaryData.setDataType("secondary_cb");
        appSecondaryDataRepository.save(appSecondaryData);
        return true;
    }



    public Map<String, Object> getSecondaryCBInfo(int appId, String person){
        AppDelegation appDelegation = appDelegationRepository.findById(appId);

        if (appDelegation == null)
            throw new CaseNotFoundException("Case Not Found");
        Application application = appDelegation.getApplication();
        AppSecondaryData appSecondaryData = appSecondaryDataRepository.findByApplicationAndDataType(application, person);

        if (appSecondaryData == null)
            throw new ObjectNotFoundException("Object Not found");
        Map<String, String> map = workflowUtils.stringToMap(appSecondaryData.getSecondaryData());
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("cbNumber", appSecondaryData.getIdentifier());

        for (String key : map.keySet()){
            String val = map.get(key);
            if (val != null && val.contains("^"))
                val = val.replace("^", ",");

            Object returnVal = null;
            if (val != null) {
                if (val.equals("true"))
                    returnVal = true;
                else if (val.equals("false"))
                    returnVal = false;
                else
                    returnVal = val;

            }
            returnMap.put(key, returnVal);
        }

        return returnMap;
    }

    public List<Map> getTaggedCB(int appId){
        AppDelegation appDelegation = appDelegationRepository.findById(appId);

        if (appDelegation == null)
            throw new CaseNotFoundException("Case Not Found");
        Application application = appDelegation.getApplication();
        List<AppSecondaryData> appSecondaryDataList = appSecondaryDataRepository.findByApplication(application);
        List<Map> cbList = new ArrayList<>();
        for (AppSecondaryData appSecondaryData : appSecondaryDataList){
            Map<String, Object> map = new HashMap();
            map.put("cbNumber", appSecondaryData.getIdentifier());
            map.put("customer", appSecondaryData.getDataType());
            cbList.add(map);
        }

        return cbList;
    }
}
