package com.babl.citybank.modules.workflow.services;

import com.babl.citybank.datasources.oracle.workflow.repositories.CardCallCategoryRepository;
import com.babl.citybank.datasources.oracle.workflow.repositories.CardMaintenanceTeamRepository;
import com.babl.citybank.datasources.oracle.workflow.schemas.CardCallCategory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CardService {
    private final CardMaintenanceTeamRepository cardMaintenanceTeamRepository;
    private final CardCallCategoryRepository cardCallCategoryRepository;

    public List<Map<String,Object>> getCallCategory(){
        List<Map<String,Object>> list = new ArrayList<>();
        List<CardCallCategory> cardCallCategoryList = cardCallCategoryRepository.findAll();
        for (int i = 0; i <cardCallCategoryList.size() ; i++) {
            Map<String,Object> objectMap = new HashMap<>();
            objectMap.put("id",cardCallCategoryList.get(i).getId());
            objectMap.put("label",cardCallCategoryList.get(i).getCallCategory());
            objectMap.put("callBack",cardCallCategoryList.get(i).getCallBack());
            objectMap.put("cardMaintenanceTeam",cardCallCategoryList.get(i).getCardMaintenanceTeam());
            objectMap.put("supportingDocuments",cardCallCategoryList.get(i).getSupportingDocuments());
            list.add(objectMap);
        }
        return list;
    }
}
