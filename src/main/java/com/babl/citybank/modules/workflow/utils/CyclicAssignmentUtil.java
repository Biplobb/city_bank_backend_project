package com.babl.citybank.modules.workflow.utils;

import com.babl.citybank.datasources.oracle.workflow.repositories.SdDepartmentMasterRepository;
import com.babl.citybank.datasources.oracle.workflow.schemas.SdDepartmentMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.locks.ReentrantLock;

@Slf4j
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CyclicAssignmentUtil {
    private final SdDepartmentMasterRepository sdDepartmentMasterRepository;

    private static int pos = 0;
    private final ReentrantLock reentrantLock = new ReentrantLock();

    public String getNextUser(){
        SdDepartmentMaster sdDepartmentMaster = sdDepartmentMasterRepository.findByDepartmentName("CASA");
//        List<SdProcessFlowMaster> sdProcessFlowMasterList = sdProcessFlowMasterRepository.
//                findBySdDepartmentMasterAndSdGlobalGroupName(sdDepartmentMaster, SDGlobalGroupName.MAKER);

        String username = null;

//        int len = sdProcessFlowMasterList.size();

        reentrantLock.lock();
        try {
//            if (pos >= len)
//                pos = 0;
//            username = sdProcessFlowMasterList.get(pos).getUserName();
            pos = pos + 1;
        }
        catch (Exception e){
            log.info(e.toString());
        }
        finally {
            reentrantLock.unlock();
        }

        return username;
    }
}
