package com.babl.citybank.modules.workflow.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignaturePhotoDTO {
    private int appId;
    private String image;
    private String type;
}
