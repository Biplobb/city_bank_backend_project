package com.babl.citybank.modules.login;

import com.babl.citybank.modules.adminPanel.resources.RoleAndMenuOfUserResource;
import com.babl.citybank.modules.adminPanel.services.RoleMenuService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LoginController {

    private final RoleMenuService roleMenuService;

    @RequestMapping(value = "/logMeIn", method = RequestMethod.GET)
    public boolean login(){
        return true;
    }

    @RequestMapping(value = "/get/roleAndMenu/{username}", method = RequestMethod.GET)
    public RoleAndMenuOfUserResource getRoleMenu(@PathVariable("username") final String username){
        return roleMenuService.getRoleMenuOfUser(username);
    }
}
