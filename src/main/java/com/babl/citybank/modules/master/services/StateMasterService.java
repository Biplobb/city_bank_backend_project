package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.datasources.oracle.master.repositories.CountryMasterRepository;
import com.babl.citybank.datasources.oracle.master.schemas.CountryMaster;
import com.babl.citybank.modules.master.dtos.StateMasterAddUpdateDTO;
import com.babl.citybank.datasources.oracle.master.repositories.StateMasterRepository;
import com.babl.citybank.modules.master.resources.StateMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.StateMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class StateMasterService {
    private final StateMasterRepository stateMasterRepository;
    private final AuditLogService auditLogService;
    private final CountryMasterRepository countryMasterRepository;


    public boolean addStateMaster(StateMasterAddUpdateDTO stateMasterAddUpdateDTO) {
        StateMaster stateMaster =stateMasterRepository.findByStateName(stateMasterAddUpdateDTO.getStateName());
        if(stateMaster!=null){
            log.info("StateMaster already assigned");
            return false;
        }
        stateMaster = new StateMaster();
        stateMaster.setStateName(stateMasterAddUpdateDTO.getStateName());
        CountryMaster countryMaster=countryMasterRepository.findById(stateMasterAddUpdateDTO.getCountryId());
        stateMaster.setCountryMaster(countryMaster);
        stateMaster.setStatus(stateMasterAddUpdateDTO.getStatus());
        stateMasterRepository.save(stateMaster);
        log.info("StateMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("state_name", stateMaster.getStateName());
        mapData.put("country_id", stateMaster.getCountryMaster());
        mapData.put("status", stateMaster.getStatus());

        auditLogService.addAuditLog(stateMaster, "state_master", stateMaster.getId(), EventType.INSERT.toString(), mapData.toString());


        return true;
    }

    public boolean updateStateMaster(StateMasterAddUpdateDTO stateMasterAddUpdateDTO, int id) {
        StateMaster stateMaster = stateMasterRepository.findById(id);
        if (stateMaster == null) {
            log.info("StateMaster not found with id = " + id);
            return false;
        }
        StateMaster existStateMaster = stateMasterRepository.findByStateName(stateMasterAddUpdateDTO.getStateName());
        if(existStateMaster!=null){
            log.info("StateMaster already assigned");
            return false;
        }
        stateMaster.setStateName(stateMasterAddUpdateDTO.getStateName());
        CountryMaster countryMaster=countryMasterRepository.findById(stateMasterAddUpdateDTO.getCountryId());
        stateMaster.setCountryMaster(countryMaster);
        stateMaster.setStatus(stateMasterAddUpdateDTO.getStatus());
        stateMasterRepository.save(stateMaster);
        log.info("StateMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("state_name", stateMaster.getStateName());
        mapData.put("country_id", stateMaster.getCountryMaster());
        mapData.put("status", stateMaster.getStatus());

        auditLogService.addAuditLog(stateMaster, "state_master", stateMaster.getId(), EventType.UPDATE.toString(), mapData.toString());


        return true;
    }

    public boolean deleteStateMaster(int id) {
        StateMaster stateMaster = stateMasterRepository.findById(id);
        if (stateMaster == null) {
            log.info("StateMaster not found with id = " + id);
            return false;
        }
        stateMasterRepository.delete(stateMaster);
        log.info("StateMaster deleted sucessfully with id = " + id);
        return true;
    }

    public StateMasterResource getStateMasterResource(int id){
        StateMaster stateMaster = stateMasterRepository.findById(id);
        if (stateMaster == null){
            log.info("StateMaster not found with id = " + id);
            return null;
        }
        StateMasterResource stateMasterResource = new StateMasterResource();
        stateMasterResource.setId(stateMaster.getId());
        stateMasterResource.setStateName(stateMaster.getStateName());
        stateMasterResource.setCountryId(stateMaster.getCountryMaster().getId());
        stateMasterResource.setStatus(stateMaster.getStatus());
        stateMasterResource.setCreatedBy(stateMaster.getCreatedBy());
        stateMasterResource.setCreatedDate(stateMaster.getCreatedDate());
        stateMasterResource.setModifiedBy(stateMaster.getModifiedBy());
        stateMasterResource.setModifiedDate(stateMaster.getModifiedDate());
        log.info("StateMaster returned sucessfully with id = " + id);
        return stateMasterResource;
    }

    public List<StateMaster> getAllStateMaster() {
        return stateMasterRepository.findAll();
    }

}