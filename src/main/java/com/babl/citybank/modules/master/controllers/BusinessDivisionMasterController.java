package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.resources.BusinessDivisionMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.BusinessDivisionMaster;
import com.babl.citybank.modules.master.dtos.BusinessDivisionMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.BusinessDivisionMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BusinessDivisionMasterController {
    private final BusinessDivisionMasterService businessDivisionMasterService;

    @RequestMapping(value = "businessDivisionMaster/add", method = RequestMethod.POST)
    public StatusResource addBusinessDivisionMaster(@RequestBody @Validated final BusinessDivisionMasterAddUpdateDTO businessDivisionMasterAddUpdateDTO) {
        boolean status = businessDivisionMasterService.addBusinessDivisionMaster(businessDivisionMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "businessDivisionMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateBusinessDivisionMaster(@RequestBody @Validated final BusinessDivisionMasterAddUpdateDTO businessDivisionMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = businessDivisionMasterService.updateBusinessDivisionMaster(businessDivisionMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "businessDivisionMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteBusinessDivisionMaster(@PathVariable("id") final int id) {
        boolean status = businessDivisionMasterService.deleteBusinessDivisionMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "businessDivisionMaster/get/{id}", method = RequestMethod.GET)
    public BusinessDivisionMasterResource getBusinessDivisionMaster(@PathVariable("id") final int id) {
        return businessDivisionMasterService.getBusinessDivisionMasterResource(id);
    }

    @RequestMapping(value = "businessDivisionMaster/getAll", method = RequestMethod.GET)
    public List<BusinessDivisionMaster> getAllBusinessDivisionMaster() {
        return businessDivisionMasterService.getAllBusinessDivisionMaster();
    }

}