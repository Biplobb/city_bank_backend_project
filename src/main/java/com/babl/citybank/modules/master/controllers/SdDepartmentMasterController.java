package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.common.StatusResource;
import com.babl.citybank.modules.master.dtos.SdDepartmentMasterDTO;
import com.babl.citybank.modules.master.resources.SdDepartmentMasterResource;
import com.babl.citybank.datasources.oracle.workflow.schemas.SdDepartmentMaster;
import com.babl.citybank.modules.master.services.SdDepartmentMasterService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SdDepartmentMasterController {
    private  final SdDepartmentMasterService sdDepartmentMasterService;

    @RequestMapping(value = "sdDepartmentMaster/add",method= RequestMethod.POST)
    public StatusResource addSdDepartmentMaster(@RequestBody @Validated SdDepartmentMasterDTO sdDepartmentMasterDTO ){
        boolean status=sdDepartmentMasterService.addSdDepartmentMaster(sdDepartmentMasterDTO);
        StatusResource statusResource=new StatusResource("Successfully","");
        if(!status){
            statusResource.setStatus("Unsuccessfully");
            return statusResource;
        }
        return statusResource;
    }

    @RequestMapping(value = "sdDepartmentMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteSdDepartmentMaster(@PathVariable("id") final int id) {
        boolean status = sdDepartmentMasterService.deleteSdDepartmentMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "sdDepartment/get/{id}", method = RequestMethod.GET)
    public SdDepartmentMasterResource getSdDepartmentMaster(@PathVariable("id") final int id) {
        return sdDepartmentMasterService.getDepartmentMasterResource(id);
    }
    @RequestMapping(value = "sdDepartmentMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateSdDepartmentMaster(@RequestBody @Validated final SdDepartmentMasterDTO sdDepartmentMasterDTO, @PathVariable("id") final int id) {
        boolean status = sdDepartmentMasterService.updateSdDepartmentMaster(sdDepartmentMasterDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }
    @RequestMapping(value = "sdDepartmentMaster/getAll", method = RequestMethod.GET)
    public List<SdDepartmentMaster> getAllSdDepartmentMaster(){
        return sdDepartmentMasterService.getAllSdDepartmentMaster();
    }
}
