package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.resources.TypeMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.TypeMaster;
import com.babl.citybank.modules.master.dtos.TypeMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.TypeMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TypeMasterController {
    private final TypeMasterService typeMasterService;

    @RequestMapping(value = "typeMaster/add", method = RequestMethod.POST)
    public StatusResource addTypeMaster(@RequestBody @Validated final TypeMasterAddUpdateDTO typeMasterAddUpdateDTO) {
        boolean status = typeMasterService.addTypeMaster(typeMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "typeMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateTypeMaster(@RequestBody @Validated final TypeMasterAddUpdateDTO typeMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = typeMasterService.updateTypeMaster(typeMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "typeMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteTypeMaster(@PathVariable("id") final int id) {
        boolean status = typeMasterService.deleteTypeMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "typeMaster/get/{id}", method = RequestMethod.GET)
    public TypeMasterResource getTypeMaster(@PathVariable("id") final int id) {
        return typeMasterService.getTypeMasterResource(id);
    }

    @RequestMapping(value = "typeMaster/getAll", method = RequestMethod.GET)
    public List<TypeMaster> getAllTypeMaster() {
        return typeMasterService.getAllTypeMaster();
    }

}