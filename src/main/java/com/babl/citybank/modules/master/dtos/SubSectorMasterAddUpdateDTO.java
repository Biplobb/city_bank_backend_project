package com.babl.citybank.modules.master.dtos;

import com.babl.citybank.common.Status;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class SubSectorMasterAddUpdateDTO {
    @NotEmpty
    private String subSectorCode;

    @NotEmpty
    private String subSectorName;
    private int sectorId;

    private Status status;

}