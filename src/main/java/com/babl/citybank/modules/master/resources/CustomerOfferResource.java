package com.babl.citybank.modules.master.resources;

import lombok.Data;

import javax.persistence.criteria.CriteriaBuilder;

@Data
public class CustomerOfferResource {
    private Integer id;
    private String customerId;
    private String productCode;
}
