package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.resources.SectorMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.SectorMaster;
import com.babl.citybank.modules.master.dtos.SectorMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.SectorMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SectorMasterController {
    private final SectorMasterService sectorMasterService;

    @RequestMapping(value = "sectorMaster/add", method = RequestMethod.POST)
    public StatusResource addSectorMaster(@RequestBody @Validated final SectorMasterAddUpdateDTO sectorMasterAddUpdateDTO) {
        boolean status = sectorMasterService.addSectorMaster(sectorMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "sectorMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateSectorMaster(@RequestBody @Validated final SectorMasterAddUpdateDTO sectorMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = sectorMasterService.updateSectorMaster(sectorMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "sectorMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteSectorMaster(@PathVariable("id") final int id) {
        boolean status = sectorMasterService.deleteSectorMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "sectorMaster/get/{id}", method = RequestMethod.GET)
    public SectorMasterResource getSectorMaster(@PathVariable("id") final int id) {
        return sectorMasterService.getSectorMasterResource(id);
    }

    @RequestMapping(value = "sectorMaster/getAll", method = RequestMethod.GET)
    public List<SectorMaster> getAllSectorMaster() {
        return sectorMasterService.getAllSectorMaster();
    }

}