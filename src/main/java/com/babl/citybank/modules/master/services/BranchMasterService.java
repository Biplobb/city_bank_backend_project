package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.BranchMasterAddUpdateDTO;
import com.babl.citybank.datasources.oracle.workflow.repositories.BranchMasterRepository;
import com.babl.citybank.modules.master.resources.BranchMasterResource;
import com.babl.citybank.datasources.oracle.workflow.schemas.BranchMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BranchMasterService {
    private final BranchMasterRepository branchMasterRepository;
    private final AuditLogService auditLogService;


    public boolean addBranchMaster(BranchMasterAddUpdateDTO branchMasterAddUpdateDTO) {
        BranchMaster branchMaster = branchMasterRepository.findByBranchNameAndSolId(branchMasterAddUpdateDTO.getBranchName(),branchMasterAddUpdateDTO.getSolId());
        if(branchMaster!=null){
            log.info("Branch Name already assigned");

            return false;
        }
        branchMaster = new BranchMaster();
        branchMaster.setBranchName(branchMasterAddUpdateDTO.getBranchName());
        branchMaster.setSolId(branchMasterAddUpdateDTO.getSolId());
        branchMaster.setStatus(branchMasterAddUpdateDTO.getStatus());
        branchMasterRepository.save(branchMaster);
        log.info("BranchMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("branch_name", branchMaster.getBranchName());
        mapData.put("sol_id", branchMaster.getSolId());
        mapData.put("status", branchMaster.getStatus());

        auditLogService.addAuditLog(branchMaster, "branch_master", branchMaster.getId(), EventType.INSERT.toString(), mapData.toString());


        return true;
    }

    public boolean updateBranchMaster(BranchMasterAddUpdateDTO branchMasterAddUpdateDTO, int id) {
        BranchMaster branchMaster = branchMasterRepository.findById(id);
        if (branchMaster == null) {
            log.info("BranchMaster not found with id = " + id);
            return false;
        }
       BranchMaster existBranchMaster = branchMasterRepository.findByBranchNameAndSolId(branchMasterAddUpdateDTO.getBranchName(),branchMasterAddUpdateDTO.getSolId());
        if(existBranchMaster!=null){
            log.info("BranchMaster already assigned");
            return false;
        }
        branchMaster.setBranchName(branchMasterAddUpdateDTO.getBranchName());
        branchMaster.setSolId(branchMasterAddUpdateDTO.getSolId());
        branchMaster.setStatus(branchMasterAddUpdateDTO.getStatus());
        branchMasterRepository.save(branchMaster);
        log.info("BranchMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("branch_name", branchMaster.getBranchName());
        mapData.put("sol_id", branchMaster.getSolId());
        mapData.put("status", branchMaster.getStatus());

        auditLogService.addAuditLog(branchMaster, "branch_master", branchMaster.getId(), EventType.UPDATE.toString(), mapData.toString());


        return true;
    }

    public boolean deleteBranchMaster(int id) {
        BranchMaster branchMaster = branchMasterRepository.findById(id);
        if (branchMaster == null) {
            log.info("BranchMaster not found with id = " + id);
            return false;
        }
        branchMasterRepository.delete(branchMaster);
        log.info("BranchMaster deleted sucessfully with id = " + id);
        return true;
    }

    public BranchMasterResource getBranchMasterResource(int id){
        BranchMaster branchMaster = branchMasterRepository.findById(id);
        if (branchMaster == null){
            log.info("BranchMaster not found with id = " + id);
            return null;
        }
        BranchMasterResource branchMasterResource = new BranchMasterResource();
        branchMasterResource.setId(branchMaster.getId());
        branchMasterResource.setBranchName(branchMaster.getBranchName());
        branchMasterResource.setSolId(branchMaster.getSolId());
        branchMasterResource.setStatus(branchMaster.getStatus());
        branchMasterResource.setCreatedBy(branchMaster.getCreatedBy());
        branchMasterResource.setCreatedDate(branchMaster.getCreatedDate());
        branchMasterResource.setModifiedBy(branchMaster.getModifiedBy());
        branchMasterResource.setModifiedDate(branchMaster.getModifiedDate());
        log.info("BranchMaster returned sucessfully with id = " + id);
        return branchMasterResource;
    }


    public List<BranchMaster> getAllBranchMaster() {
        return branchMasterRepository.findAll();
    }

}