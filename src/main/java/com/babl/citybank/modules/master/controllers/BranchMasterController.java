package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.resources.BranchMasterResource;
import com.babl.citybank.datasources.oracle.workflow.schemas.BranchMaster;
import com.babl.citybank.modules.master.dtos.BranchMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.BranchMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BranchMasterController {
    private final BranchMasterService branchMasterService;

    @RequestMapping(value = "branchMaster/add", method = RequestMethod.POST)
    public StatusResource addBranchMaster(@RequestBody @Validated final BranchMasterAddUpdateDTO branchMasterAddUpdateDTO) {
        boolean status = branchMasterService.addBranchMaster(branchMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "branchMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateBranchMaster(@RequestBody @Validated final BranchMasterAddUpdateDTO branchMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = branchMasterService.updateBranchMaster(branchMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "branchMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteBranchMaster(@PathVariable("id") final int id) {
        boolean status = branchMasterService.deleteBranchMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "branchMaster/get/{id}", method = RequestMethod.GET)
    public BranchMasterResource getBranchMaster(@PathVariable("id") final int id) {
        return branchMasterService.getBranchMasterResource(id);
    }

    @RequestMapping(value = "branchMaster/getAll", method = RequestMethod.GET)
    public List<BranchMaster> getAllBranchMaster() {
        return branchMasterService.getAllBranchMaster();
    }

}