package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.resources.BandMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.BandMaster;
import com.babl.citybank.modules.master.dtos.BandMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.BandMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BandMasterController {
    private final BandMasterService bandMasterService;

    @RequestMapping(value = "bandMaster/add", method = RequestMethod.POST)
    public StatusResource addBandMaster(@RequestBody @Validated final BandMasterAddUpdateDTO bandMasterAddUpdateDTO) {
        boolean status = bandMasterService.addBandMaster(bandMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "bandMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateBandMaster(@RequestBody @Validated final BandMasterAddUpdateDTO bandMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = bandMasterService.updateBandMaster(bandMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "bandMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteBandMaster(@PathVariable("id") final int id) {
        boolean status = bandMasterService.deleteBandMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "bandMaster/get/{id}", method = RequestMethod.GET)
    public BandMasterResource getBandMaster(@PathVariable("id") final int id) {
        return bandMasterService.getBandMaster(id);
    }

    @RequestMapping(value = "bandMaster/getAll", method = RequestMethod.GET)
    public List<BandMaster> getAllBandMaster() {
        return bandMasterService.getAllBandMaster();
    }

}