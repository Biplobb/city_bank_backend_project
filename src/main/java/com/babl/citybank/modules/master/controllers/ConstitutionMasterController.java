package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.resources.ConstitutionMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.ConstitutionMaster;
import com.babl.citybank.modules.master.dtos.ConstitutionMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.ConstitutionMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ConstitutionMasterController {
    private final ConstitutionMasterService constitutionMasterService;

    @RequestMapping(value = "constitutionMaster/add", method = RequestMethod.POST)
    public StatusResource addConstitutionMaster(@RequestBody @Validated final ConstitutionMasterAddUpdateDTO constitutionMasterAddUpdateDTO) {
        boolean status = constitutionMasterService.addConstitutionMaster(constitutionMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "constitutionMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateConstitutionMaster(@RequestBody @Validated final ConstitutionMasterAddUpdateDTO constitutionMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = constitutionMasterService.updateConstitutionMaster(constitutionMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "constitutionMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteConstitutionMaster(@PathVariable("id") final int id) {
        boolean status = constitutionMasterService.deleteConstitutionMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "constitutionMaster/get/{id}", method = RequestMethod.GET)
    public ConstitutionMasterResource getConstitutionMaster(@PathVariable("id") final int id) {
        return constitutionMasterService.getConstitutionMasterResource(id);
    }

    @RequestMapping(value = "constitutionMaster/getAll", method = RequestMethod.GET)
    public List<ConstitutionMaster> getAllConstitutionMaster() {
        return constitutionMasterService.getAllConstitutionMaster();
    }

}