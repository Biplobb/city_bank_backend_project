package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.datasources.oracle.master.repositories.StateMasterRepository;
import com.babl.citybank.datasources.oracle.master.schemas.StateMaster;
import com.babl.citybank.modules.master.dtos.CityMasterAddUpdateDTO;
import com.babl.citybank.datasources.oracle.master.repositories.CityMasterRepository;
import com.babl.citybank.modules.master.resources.CityMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.CityMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CityMasterService {
    private final CityMasterRepository cityMasterRepository;
    private final AuditLogService auditLogService;
    private final StateMasterRepository stateMasterRepository;

    public boolean addCityMaster(CityMasterAddUpdateDTO cityMasterAddUpdateDTO) {
        CityMaster ExistCityMaster =cityMasterRepository.findByCityName(cityMasterAddUpdateDTO.getCityName());
        if(ExistCityMaster!=null){
            log.info("City Master already assigned");
            return false;
        }
        CityMaster cityMaster = new CityMaster();
        cityMaster.setCityName(cityMasterAddUpdateDTO.getCityName());
        StateMaster stateMaster =stateMasterRepository.findById(cityMasterAddUpdateDTO.getStateId());
        cityMaster.setStateMaster(stateMaster);
        cityMaster.setStatus(cityMasterAddUpdateDTO.getStatus());
        cityMasterRepository.save(cityMaster);
        log.info("CityMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("city_name", cityMaster.getCityName());
        mapData.put("state_id", cityMaster.getStateMaster());
        mapData.put("status", cityMaster.getStatus());

        auditLogService.addAuditLog(cityMaster, "city_master", cityMaster.getId(), EventType.INSERT.toString(), mapData.toString());


        return true;
    }

    public boolean updateCityMaster(CityMasterAddUpdateDTO cityMasterAddUpdateDTO, int id) {
        CityMaster cityMaster = cityMasterRepository.findById(id);
        if (cityMaster == null) {
            log.info("CityMaster not found with id = " + id);
            return false;
        }
        CityMaster existCityMaster = cityMasterRepository.findByCityName(cityMasterAddUpdateDTO.getCityName());
        if(existCityMaster!=null){
            log.info("CityMaster already assigned");
            return false;
        }
        cityMaster.setCityName(cityMasterAddUpdateDTO.getCityName());
        StateMaster stateMaster =stateMasterRepository.findById(cityMasterAddUpdateDTO.getStateId());
        cityMaster.setStateMaster(stateMaster);
        cityMaster.setStatus(cityMasterAddUpdateDTO.getStatus());
        cityMasterRepository.save(cityMaster);
        log.info("CityMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("city_name", cityMaster.getCityName());
        mapData.put("state_id", cityMaster.getStateMaster());
        mapData.put("status", cityMaster.getStatus());

        auditLogService.addAuditLog(cityMaster, "city_master", cityMaster.getId(), EventType.UPDATE.toString(), mapData.toString());

        return true;
    }

    public boolean deleteCityMaster(int id) {
        CityMaster cityMaster = cityMasterRepository.findById(id);
        if (cityMaster == null) {
            log.info("CityMaster not found with id = " + id);
            return false;
        }
        cityMasterRepository.delete(cityMaster);
        log.info("CityMaster deleted sucessfully with id = " + id);
        return true;
    }

    public CityMasterResource getCityMasterResource(int id){
        CityMaster cityMaster = cityMasterRepository.findById(id);
        if (cityMaster == null){
            log.info("CityMaster not found with id = " + id);
            return null;
        }
        CityMasterResource cityMasterResource = new CityMasterResource();
        cityMasterResource.setId(cityMaster.getId());
        cityMasterResource.setCityName(cityMaster.getCityName());
        cityMasterResource.setStateId(cityMaster.getStateMaster().getId());
        cityMasterResource.setStatus(cityMaster.getStatus());
        cityMasterResource.setCreatedBy(cityMaster.getCreatedBy());
        cityMasterResource.setCreatedDate(cityMaster.getCreatedDate());
        cityMasterResource.setModifiedBy(cityMaster.getModifiedBy());
        cityMasterResource.setModifiedDate(cityMaster.getModifiedDate());
        log.info("CityMaster returned sucessfully with id = " + id);
        return cityMasterResource;
    }


    public List<CityMaster> getAllCityMaster() {
        return cityMasterRepository.findAll();
    }

}