package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.CountryMasterAddUpdateDTO;
import com.babl.citybank.datasources.oracle.master.repositories.CountryMasterRepository;
import com.babl.citybank.modules.master.resources.CountryMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.CountryMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CountryMasterService {
    private final CountryMasterRepository countryMasterRepository;
    private final AuditLogService auditLogService;

    public boolean addCountryMaster(CountryMasterAddUpdateDTO countryMasterAddUpdateDTO) {
        CountryMaster countryMaster =countryMasterRepository.findByCountryName(countryMasterAddUpdateDTO.getCountryName());
        if(countryMaster!=null){
            log.info("countryMaster already assigned");
            return false;
        }
        countryMaster = new CountryMaster();
        countryMaster.setCountryName(countryMasterAddUpdateDTO.getCountryName());
        countryMaster.setStatus(countryMasterAddUpdateDTO.getStatus());
        countryMasterRepository.save(countryMaster);
        log.info("CountryMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("country_name", countryMaster.getCountryName());
        mapData.put("status", countryMaster.getStatus());

        auditLogService.addAuditLog(countryMaster, "country_master", countryMaster.getId(), EventType.INSERT.toString(), mapData.toString());


        return true;
    }

    public boolean updateCountryMaster(CountryMasterAddUpdateDTO countryMasterAddUpdateDTO, int id) {
        CountryMaster countryMaster = countryMasterRepository.findById(id);
        if (countryMaster == null) {
            log.info("CountryMaster not found with id = " + id);
            return false;
        }
        CountryMaster existCountryMaster = countryMasterRepository.findByCountryName(countryMasterAddUpdateDTO.getCountryName());
        if(existCountryMaster!=null){
            log.info("CountryMaster already assigned");
             return false;
        }
        countryMaster.setCountryName(countryMasterAddUpdateDTO.getCountryName());
        countryMaster.setStatus(countryMasterAddUpdateDTO.getStatus());
        countryMasterRepository.save(countryMaster);
        log.info("CountryMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("country_name", countryMaster.getCountryName());
        mapData.put("status", countryMaster.getStatus());

        auditLogService.addAuditLog(countryMaster, "country_master", countryMaster.getId(), EventType.UPDATE.toString(), mapData.toString());

        return true;
    }

    public boolean deleteCountryMaster(int id) {
        CountryMaster countryMaster = countryMasterRepository.findById(id);
        if (countryMaster == null) {
            log.info("CountryMaster not found with id = " + id);
            return false;
        }
        countryMasterRepository.delete(countryMaster);
        log.info("CountryMaster deleted sucessfully with id = " + id);
        return true;
    }

    public CountryMasterResource getCountryMasterResource(int id){
        CountryMaster countryMaster = countryMasterRepository.findById(id);
        if (countryMaster == null){
            log.info("CountryMaster not found with id = " + id);
            return null;
        }
        CountryMasterResource countryMasterResource = new CountryMasterResource();
        countryMasterResource.setId(countryMaster.getId());
        countryMasterResource.setCountryName(countryMaster.getCountryName());
        countryMasterResource.setStatus(countryMaster.getStatus());
        countryMasterResource.setCreatedBy(countryMaster.getCreatedBy());
        countryMasterResource.setCreatedDate(countryMaster.getCreatedDate());
        countryMasterResource.setModifiedBy(countryMaster.getModifiedBy());
        countryMasterResource.setModifiedDate(countryMaster.getModifiedDate());
        log.info("CountryMaster returned sucessfully with id = " + id);
        return countryMasterResource;
    }

    public List<CountryMaster> getAllCountryMaster() {
        return countryMasterRepository.findAll();
    }

}