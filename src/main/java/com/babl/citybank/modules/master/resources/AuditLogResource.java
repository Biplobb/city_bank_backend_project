package com.babl.citybank.modules.master.resources;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Map;

@Data
public class AuditLogResource {
    private String auditor;

    private LocalDateTime auditingTime;

    private String action;

    private int objectId;

    private Map<String, String> changes;
}
