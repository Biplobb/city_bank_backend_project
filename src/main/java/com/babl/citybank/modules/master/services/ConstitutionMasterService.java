package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.ConstitutionMasterAddUpdateDTO;
import com.babl.citybank.datasources.oracle.master.repositories.ConstitutionMasterRepository;
import com.babl.citybank.modules.master.resources.ConstitutionMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.ConstitutionMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ConstitutionMasterService {
    private final ConstitutionMasterRepository constitutionMasterRepository;
    private final AuditLogService auditLogService;

    public boolean addConstitutionMaster(ConstitutionMasterAddUpdateDTO constitutionMasterAddUpdateDTO) {
        ConstitutionMaster constitutionMaster =constitutionMasterRepository.findByConstitutionName(constitutionMasterAddUpdateDTO.getConstitutionName());
        if(constitutionMaster!=null){
            log.info("constitutionMaster already assigned");
            return false;
        }
        constitutionMaster = new ConstitutionMaster();
        constitutionMaster.setConstitutionName(constitutionMasterAddUpdateDTO.getConstitutionName());
        constitutionMaster.setStatus(constitutionMasterAddUpdateDTO.getStatus());
        constitutionMasterRepository.save(constitutionMaster);
        log.info("ConstitutionMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("constitution_name", constitutionMaster.getConstitutionName());
        mapData.put("status", constitutionMaster.getStatus());

        auditLogService.addAuditLog(constitutionMaster, "constitution_master", constitutionMaster.getId(), EventType.INSERT.toString(), mapData.toString());


        return true;
    }

    public boolean updateConstitutionMaster(ConstitutionMasterAddUpdateDTO constitutionMasterAddUpdateDTO, int id) {
        ConstitutionMaster constitutionMaster = constitutionMasterRepository.findById(id);
        if (constitutionMaster == null) {
            log.info("ConstitutionMaster not found with id = " + id);
            return false;
        }
        ConstitutionMaster existconstitutionMaster = constitutionMasterRepository.findByConstitutionName(constitutionMasterAddUpdateDTO.getConstitutionName());
        if(existconstitutionMaster!=null){
            log.info("ConstitutionMaster already assigned");
            return false;
        }
        constitutionMaster.setConstitutionName(constitutionMasterAddUpdateDTO.getConstitutionName());
        constitutionMaster.setStatus(constitutionMasterAddUpdateDTO.getStatus());
        constitutionMasterRepository.save(constitutionMaster);
        log.info("ConstitutionMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("constitution_name", constitutionMaster.getConstitutionName());
        mapData.put("status", constitutionMaster.getStatus());

        auditLogService.addAuditLog(constitutionMaster, "constitution_master", constitutionMaster.getId(), EventType.UPDATE.toString(), mapData.toString());


        return true;
    }

    public boolean deleteConstitutionMaster(int id) {
        ConstitutionMaster constitutionMaster = constitutionMasterRepository.findById(id);
        if (constitutionMaster == null) {
            log.info("ConstitutionMaster not found with id = " + id);
            return false;
        }
        constitutionMasterRepository.delete(constitutionMaster);
        log.info("ConstitutionMaster deleted sucessfully with id = " + id);
        return true;
    }

    public ConstitutionMasterResource getConstitutionMasterResource(int id){
        ConstitutionMaster constitutionMaster = constitutionMasterRepository.findById(id);
        if (constitutionMaster == null){
            log.info("ConstitutionMaster not found with id = " + id);
            return null;
        }
        ConstitutionMasterResource constitutionMasterResource = new ConstitutionMasterResource();
        constitutionMasterResource.setId(constitutionMaster.getId());
        constitutionMasterResource.setConstitutionName(constitutionMaster.getConstitutionName());
        constitutionMasterResource.setStatus(constitutionMaster.getStatus());
        constitutionMasterResource.setCreatedBy(constitutionMaster.getCreatedBy());
        constitutionMasterResource.setCreatedDate(constitutionMaster.getCreatedDate());
        constitutionMasterResource.setModifiedBy(constitutionMaster.getModifiedBy());
        constitutionMasterResource.setModifiedDate(constitutionMaster.getModifiedDate());
        log.info("ConstitutionMaster returned sucessfully with id = " + id);
        return constitutionMasterResource;
    }

    public List<ConstitutionMaster> getAllConstitutionMaster() {
        return constitutionMasterRepository.findAll();
    }

}