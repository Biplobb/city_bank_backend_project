package com.babl.citybank.modules.master.services;

import com.babl.citybank.datasources.oracle.master.repositories.CustomerOfferTemporaryRepository;


import com.babl.citybank.datasources.oracle.master.schemas.CustomerOfferTemporary;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @_(@Autowired))
public class FileUploadToReadService {
    protected final CustomerOfferService customerOfferService;
    private final CustomerOfferTemporaryRepository customerOfferTemporaryRepository;
    private String getCellValue(Cell cell){
        String cellValue = "";
        CellType cellType = cell.getCellTypeEnum();
        if (cellType.equals(CellType.STRING)){
            cellValue = cell.getStringCellValue().trim();

        }

        else if(cellType.equals(CellType.NUMERIC)){
            cellValue = Double.toString(cell.getNumericCellValue());

        }

        return cellValue;
    }


    public void getMultipartFile(MultipartFile multipartFile) {
        List<CustomerOfferTemporary> customerOfferTemporaryList = new ArrayList<>();

        if (multipartFile.isEmpty()) {

        }

        try {
            InputStream inputStream = multipartFile.getInputStream();
            XSSFRow row;
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            XSSFSheet spreadsheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = spreadsheet.iterator();
            row = (XSSFRow) rowIterator.next();
            while (rowIterator.hasNext()) {
                String customerId = "";
                String productCode = "";
                CustomerOfferTemporary customerOfferTemporary = new CustomerOfferTemporary();
                row = (XSSFRow) rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                //fileName = cellIterator.next().getStringCellValue().trim().toLowerCase();
                Cell cell = cellIterator.next();

                customerOfferTemporary.setCustomerId( getCellValue(cell));

                cell = cellIterator.next();

                customerOfferTemporary.setProductCode(getCellValue(cell));

                System.out.println(customerId);
                System.out.println(productCode);
                customerOfferTemporaryList.add(customerOfferTemporary);
                //customerOfferService.addCustomerOffers(customerId,productCode);


            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        customerOfferService.restrictedData(customerOfferTemporaryList);

    }
}
