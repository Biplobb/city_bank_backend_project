package com.babl.citybank.modules.master.dtos;
import com.babl.citybank.common.Status;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class NotificationMasterAddUpdateDTO {
    @NotEmpty
    private String type;


    private String message;

    private Status status;
}
