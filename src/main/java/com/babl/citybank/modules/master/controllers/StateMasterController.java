package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.resources.StateMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.StateMaster;
import com.babl.citybank.modules.master.dtos.StateMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.StateMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class StateMasterController {
    private final StateMasterService stateMasterService;

    @RequestMapping(value = "stateMaster/add", method = RequestMethod.POST)
    public StatusResource addStateMaster(@RequestBody @Validated final StateMasterAddUpdateDTO stateMasterAddUpdateDTO) {
        boolean status = stateMasterService.addStateMaster(stateMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "stateMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateStateMaster(@RequestBody @Validated final StateMasterAddUpdateDTO stateMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = stateMasterService.updateStateMaster(stateMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "stateMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteStateMaster(@PathVariable("id") final int id) {
        boolean status = stateMasterService.deleteStateMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "stateMaster/get/{id}", method = RequestMethod.GET)
    public StateMasterResource getStateMaster(@PathVariable("id") final int id) {
        return stateMasterService.getStateMasterResource(id);
    }

    @RequestMapping(value = "stateMaster/getAll", method = RequestMethod.GET)
    public List<StateMaster> getAllStateMaster() {
        return stateMasterService.getAllStateMaster();
    }

}