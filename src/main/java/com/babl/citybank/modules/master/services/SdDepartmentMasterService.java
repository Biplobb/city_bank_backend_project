package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.SdDepartmentMasterDTO;
import com.babl.citybank.datasources.oracle.workflow.repositories.SdDepartmentMasterRepository;
import com.babl.citybank.modules.master.resources.SdDepartmentMasterResource;
import com.babl.citybank.datasources.oracle.workflow.schemas.SdDepartmentMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Transactional
@Service
@RequiredArgsConstructor(onConstructor = @_(@Autowired))
public class SdDepartmentMasterService {
    private final SdDepartmentMasterRepository sdDepartmentMasterRepository;
    private final AuditLogService auditLogService;

    public boolean addSdDepartmentMaster(SdDepartmentMasterDTO sdDepartmentMasterDTO ){
        SdDepartmentMaster sdDepartmentMaster=sdDepartmentMasterRepository.findByDepartmentName(sdDepartmentMasterDTO.getDepartmentName());
        if(sdDepartmentMaster!=null){
            log.info("SdDepartmentMaster already assigned");
        }
        sdDepartmentMaster=new SdDepartmentMaster();
        sdDepartmentMaster.setDepartmentName(sdDepartmentMasterDTO.getDepartmentName());
        sdDepartmentMaster.setStatus(sdDepartmentMasterDTO.getStatus());
        sdDepartmentMasterRepository.save(sdDepartmentMaster);
        Map<String,Object> mapData=new HashMap<>();
        mapData.put("department_name",sdDepartmentMasterDTO.getDepartmentName());
        mapData.put("status",sdDepartmentMasterDTO.getStatus());
        auditLogService.addAuditLog(sdDepartmentMaster,"sd_department_master",sdDepartmentMaster.getId(), EventType.INSERT.toString(),mapData.toString());
        return true;
    }


    public boolean deleteSdDepartmentMaster(int id) {
        SdDepartmentMaster sdDepartmentMaster = sdDepartmentMasterRepository.getOne(id);
        if (sdDepartmentMaster == null) {
            log.info("SdDepartmentMaster not found with id = " + id);
            return false;
        }
        sdDepartmentMasterRepository.delete(sdDepartmentMaster);
        log.info("SdDepartmentMaster deleted sucessfully with id = " + id);
        return true;
    }

    public SdDepartmentMasterResource getDepartmentMasterResource(int id){
        SdDepartmentMaster sdDepartmentMaster = sdDepartmentMasterRepository.getOne(id);
        if (sdDepartmentMaster == null){
            log.info("Sd department id not found with id = " + id);
            return null;
        }
        SdDepartmentMasterResource sdDepartmentMasterResource = new SdDepartmentMasterResource();
        sdDepartmentMasterResource.setId(sdDepartmentMaster.getId());
        sdDepartmentMasterResource.setDepartmentName(sdDepartmentMaster.getDepartmentName());

        sdDepartmentMasterResource.setStatus(sdDepartmentMaster.getStatus());
        sdDepartmentMasterResource.setCreatedBy(sdDepartmentMaster.getCreatedBy());
        sdDepartmentMasterResource.setCreatedDate(sdDepartmentMaster.getCreatedDate());
        sdDepartmentMasterResource.setModifiedBy(sdDepartmentMaster.getModifiedBy());
        sdDepartmentMasterResource.setModifiedDate(sdDepartmentMaster.getModifiedDate());
        log.info("Sd department returned sucessfully with id = " + id);
        return sdDepartmentMasterResource;
    }
    public boolean updateSdDepartmentMaster(SdDepartmentMasterDTO sdDepartmentMasterDTO, int id) {
        SdDepartmentMaster sdDepartmentMaster = sdDepartmentMasterRepository.getOne(id);
        if (sdDepartmentMaster == null) {
            log.info("SdDepartmentMaster not found with id = " + id);
            return false;
        }
       SdDepartmentMaster existSdDepartmentMaster = sdDepartmentMasterRepository.findByDepartmentName(sdDepartmentMasterDTO.getDepartmentName());

        if(existSdDepartmentMaster!=null){
            log.info("sdDepartmentMaster already assigned");
        }
        sdDepartmentMaster.setDepartmentName(sdDepartmentMasterDTO.getDepartmentName());
        sdDepartmentMaster.setStatus(sdDepartmentMasterDTO.getStatus());
        sdDepartmentMasterRepository.save(sdDepartmentMaster);
        log.info("SdDepartmentMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("department_name", sdDepartmentMaster.getDepartmentName());
        mapData.put("status", sdDepartmentMaster.getStatus());

        auditLogService.addAuditLog(sdDepartmentMaster, "sd_department_master", sdDepartmentMaster.getId(), EventType.UPDATE.toString(), mapData.toString());


        return true;
    }
    public List<SdDepartmentMaster> getAllSdDepartmentMaster(){
        return sdDepartmentMasterRepository.findAll();

    }
}
