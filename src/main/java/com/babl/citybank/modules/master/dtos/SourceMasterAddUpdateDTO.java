package com.babl.citybank.modules.master.dtos;

import com.babl.citybank.common.Status;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class SourceMasterAddUpdateDTO {
    @NotEmpty
    private String sourceCode;

    private String description;

    private Status status;

}