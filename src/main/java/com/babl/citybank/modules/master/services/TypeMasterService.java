package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.TypeMasterAddUpdateDTO;
import com.babl.citybank.datasources.oracle.master.repositories.TypeMasterRepository;
import com.babl.citybank.modules.master.resources.TypeMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.TypeMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TypeMasterService {
    private final TypeMasterRepository typeMasterRepository;
    private final AuditLogService auditLogService;

    public boolean addTypeMaster(TypeMasterAddUpdateDTO typeMasterAddUpdateDTO) {
        TypeMaster typeMaster =typeMasterRepository.findByTypeName(typeMasterAddUpdateDTO.getTypeName());
        if(typeMaster!=null){
            log.info("TypeMaster already assigned");
            return false;
        }
        typeMaster = new TypeMaster();
        typeMaster.setTypeName(typeMasterAddUpdateDTO.getTypeName());
        typeMaster.setStatus(typeMasterAddUpdateDTO.getStatus());
        typeMasterRepository.save(typeMaster);
        log.info("TypeMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("type_name", typeMaster.getTypeName());
        mapData.put("status", typeMaster.getStatus());

        auditLogService.addAuditLog(typeMaster, "type_master", typeMaster.getId(), EventType.INSERT.toString(), mapData.toString());


        return true;
    }

    public boolean updateTypeMaster(TypeMasterAddUpdateDTO typeMasterAddUpdateDTO, int id) {
        TypeMaster typeMaster = typeMasterRepository.findById(id);
        if (typeMaster == null) {
            log.info("TypeMaster not found with id = " + id);
            return false;
        }
       TypeMaster existTypeMaster = typeMasterRepository.findByTypeName(typeMasterAddUpdateDTO.getTypeName());
        if (existTypeMaster != null) {
            log.info("TypeMaster already assigned!!");
            return false;
        }
        typeMaster.setTypeName(typeMasterAddUpdateDTO.getTypeName());
        typeMaster.setStatus(typeMasterAddUpdateDTO.getStatus());
        typeMasterRepository.save(typeMaster);
        log.info("TypeMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("type_name", typeMaster.getTypeName());
        mapData.put("status", typeMaster.getStatus());

        auditLogService.addAuditLog(typeMaster, "type_master", typeMaster.getId(), EventType.UPDATE.toString(), mapData.toString());


        return true;
    }

    public boolean deleteTypeMaster(int id) {
        TypeMaster typeMaster = typeMasterRepository.findById(id);
        if (typeMaster == null) {
            log.info("TypeMaster not found with id = " + id);
            return false;
        }
        typeMasterRepository.delete(typeMaster);
        log.info("TypeMaster deleted sucessfully with id = " + id);
        return true;
    }

    public TypeMasterResource getTypeMasterResource(int id){
        TypeMaster typeMaster = typeMasterRepository.findById(id);
        if (typeMaster == null){
            log.info("TypeMaster not found with id = " + id);
            return null;
        }
        TypeMasterResource typeMasterResource = new TypeMasterResource();
        typeMasterResource.setId(typeMaster.getId());
        typeMasterResource.setTypeName(typeMaster.getTypeName());
        typeMasterResource.setStatus(typeMaster.getStatus());
        typeMasterResource.setCreatedBy(typeMaster.getCreatedBy());
        typeMasterResource.setCreatedDate(typeMaster.getCreatedDate());
        typeMasterResource.setModifiedBy(typeMaster.getModifiedBy());
        typeMasterResource.setModifiedDate(typeMaster.getModifiedDate());
        log.info("TypeMaster returned sucessfully with id = " + id);
        return typeMasterResource;
    }

    public List<TypeMaster> getAllTypeMaster() {
        return typeMasterRepository.findAll();
    }

}