package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.resources.CityMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.CityMaster;
import com.babl.citybank.modules.master.dtos.CityMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.CityMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CityMasterController {
    private final CityMasterService cityMasterService;

    @RequestMapping(value = "cityMaster/add", method = RequestMethod.POST)
    public StatusResource addCityMaster(@RequestBody @Validated final CityMasterAddUpdateDTO cityMasterAddUpdateDTO) {
        boolean status = cityMasterService.addCityMaster(cityMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "cityMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateCityMaster(@RequestBody @Validated final CityMasterAddUpdateDTO cityMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = cityMasterService.updateCityMaster(cityMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "cityMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteCityMaster(@PathVariable("id") final int id) {
        boolean status = cityMasterService.deleteCityMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "cityMaster/get/{id}", method = RequestMethod.GET)
    public CityMasterResource getCityMaster(@PathVariable("id") final int id) {
        return cityMasterService.getCityMasterResource(id);
    }

    @RequestMapping(value = "cityMaster/getAll", method = RequestMethod.GET)
    public List<CityMaster> getAllCityMaster() {
        return cityMasterService.getAllCityMaster();
    }

}