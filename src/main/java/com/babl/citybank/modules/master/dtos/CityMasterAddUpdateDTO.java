package com.babl.citybank.modules.master.dtos;

import com.babl.citybank.common.Status;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class CityMasterAddUpdateDTO {
    @NotEmpty
    private String cityName;

    @NotNull
    private int stateId;

    private Status status;

}