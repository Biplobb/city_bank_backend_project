package com.babl.citybank.modules.master.services;

import com.babl.citybank.modules.master.dtos.GetAuditLogDTO;
import com.babl.citybank.datasources.oracle.master.repositories.AuditLogRepository;
import com.babl.citybank.modules.master.resources.AuditLogResource;
import com.babl.citybank.datasources.oracle.master.schemas.AuditLog;
import com.babl.citybank.datasources.oracle.master.schemas.BaseAuditingEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AuditLogService {
    private final AuditLogRepository auditLogRepository;

    public boolean addAuditLog(BaseAuditingEntity auditingEntity, String objectType, int objectId, String eventType, String mapData) {
        AuditLog auditLog = new AuditLog();

        auditLog.setObjectType(objectType);
        auditLog.setObjectId(objectId);
        auditLog.setEventType(eventType);
        auditLog.setEventTime(auditingEntity.getModifiedDate());
        auditLog.setUsername(auditingEntity.getModifiedBy());
        auditLog.setData(mapData);

        auditLogRepository.save(auditLog);
        log.info("Audited log for object = " + objectType + ", id = " + objectId);
        return true;

    }

    public List<AuditLogResource> getAuditLog(GetAuditLogDTO getAuditLogDTO){
        List<AuditLog> auditLogList = auditLogRepository
                .findByObjectTypeAndEventTimeBetween(getAuditLogDTO.getTableName(),
                        getAuditLogDTO.getStart(), getAuditLogDTO.getEnd());

        List<AuditLogResource> auditLogResources = new ArrayList<>();

        for (AuditLog auditLog : auditLogList){
            AuditLogResource auditLogResource = new AuditLogResource();
            auditLogResource.setAuditor(auditLog.getUsername());
            auditLogResource.setAuditingTime(auditLog.getEventTime());
            auditLogResource.setObjectId(auditLog.getObjectId());
            auditLogResource.setAction(auditLog.getEventType());

            Map<String, String> changes = new HashMap<>();

            String formatedChanges = auditLog.getData().replace("{", "");
            formatedChanges = formatedChanges.replace("}", "");
            String[] keyValues = formatedChanges.split(",|=");

            for (int i=0; i<keyValues.length; i = i+2){
                changes.put(keyValues[i].trim(), keyValues[i+1].trim());
            }

            auditLogResource.setChanges(changes);

            auditLogResources.add(auditLogResource);
        }

        return auditLogResources;
    }
}
