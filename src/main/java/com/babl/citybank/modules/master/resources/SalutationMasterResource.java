package com.babl.citybank.modules.master.resources;

import com.babl.citybank.common.Status;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SalutationMasterResource {
    private int id;

    private String salutationName;

    private Status status;

    private String createdBy;

    private LocalDateTime createdDate;

    private String modifiedBy;

    private LocalDateTime modifiedDate;

}