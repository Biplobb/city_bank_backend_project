package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.BandMasterAddUpdateDTO;
import com.babl.citybank.datasources.oracle.master.repositories.BandMasterRepository;
import com.babl.citybank.modules.master.resources.BandMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.BandMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BandMasterService {
    private final BandMasterRepository bandMasterRepository;
    private final AuditLogService auditLogService;

    public boolean addBandMaster(BandMasterAddUpdateDTO bandMasterAddUpdateDTO) {
        BandMaster bandMaster=bandMasterRepository.findByBandNo(bandMasterAddUpdateDTO.getBandNo());
        if(bandMaster!=null){
            log.info("Band already assigned");
            return  false;
        }
        bandMaster = new BandMaster();
        bandMaster.setBandNo(bandMasterAddUpdateDTO.getBandNo());
        bandMaster.setStatus(bandMasterAddUpdateDTO.getStatus());
        bandMasterRepository.save(bandMaster);
        log.info("BandMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("band_no", bandMaster.getBandNo());
        mapData.put("status", bandMaster.getStatus());

        auditLogService.addAuditLog(bandMaster, "band_master", bandMaster.getId(), EventType.INSERT.toString(), mapData.toString());

        return true;
    }

    public boolean addBulk(){
        BandMaster bandMaster = new BandMaster();
        List<BandMaster> bandMasters = new ArrayList<>();
        bandMasters.add(bandMaster);
        bandMasterRepository.saveAll(bandMasters);
        return true;
    }

    public boolean updateBandMaster(BandMasterAddUpdateDTO bandMasterAddUpdateDTO, int id) {
        BandMaster bandMaster = bandMasterRepository.findById(id);
        if (bandMaster == null) {
            log.info("BandMaster not found with id = " + id);
            return false;
        }
       BandMaster existBandMaster = bandMasterRepository.findByBandNo(bandMasterAddUpdateDTO.getBandNo());
        if(existBandMaster!=null){
            log.info("BandMaster already assigned");
            return false;
        }
        bandMaster.setBandNo(bandMasterAddUpdateDTO.getBandNo());
        bandMaster.setStatus(bandMasterAddUpdateDTO.getStatus());
        bandMasterRepository.save(bandMaster);
        log.info("BandMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("band_no", bandMaster.getBandNo());
        mapData.put("status", bandMaster.getStatus());

        auditLogService.addAuditLog(bandMaster, "band_master", bandMaster.getId(), EventType.UPDATE.toString(), mapData.toString());


        return true;
    }

    public boolean deleteBandMaster(int id) {
        BandMaster bandMaster = bandMasterRepository.findById(id);
        if (bandMaster == null) {
            log.info("BandMaster not found with id = " + id);
            return false;
        }
        bandMasterRepository.delete(bandMaster);
        log.info("BandMaster deleted successfully with id = " + id);
        return true;
    }

    public BandMasterResource getBandMaster(int id) {
        BandMaster bandMaster = bandMasterRepository.findById(id);
        if (bandMaster == null) {
            log.info("BandMaster not found with id = " + id);
            return null;
        }
        BandMasterResource bandMasterResource = new BandMasterResource();
        bandMasterResource.setId(bandMaster.getId());
        bandMasterResource.setBandNo(bandMaster.getBandNo());
        bandMasterResource.setStatus(bandMaster.getStatus());
        bandMasterResource.setCreatedBy(bandMaster.getCreatedBy());
        bandMasterResource.setCreatedDate(bandMaster.getCreatedDate());
        bandMasterResource.setModifiedBy(bandMaster.getModifiedBy());
        bandMasterResource.setModifiedDate(bandMaster.getModifiedDate());
        return bandMasterResource;
    }


    public List<BandMaster> getAllBandMaster() {
        return bandMasterRepository.findAll();
    }

}