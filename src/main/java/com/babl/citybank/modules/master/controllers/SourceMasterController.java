package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.resources.SourceMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.SourceMaster;
import com.babl.citybank.modules.master.dtos.SourceMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.SourceMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SourceMasterController {
    private final SourceMasterService sourceMasterService;

    @RequestMapping(value = "sourceMaster/add", method = RequestMethod.POST)
    public StatusResource addSourceMaster(@RequestBody @Validated final SourceMasterAddUpdateDTO sourceMasterAddUpdateDTO) {
        boolean status = sourceMasterService.addSourceMaster(sourceMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "sourceMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateSourceMaster(@RequestBody @Validated final SourceMasterAddUpdateDTO sourceMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = sourceMasterService.updateSourceMaster(sourceMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "sourceMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteSourceMaster(@PathVariable("id") final int id) {
        boolean status = sourceMasterService.deleteSourceMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "sourceMaster/get/{id}", method = RequestMethod.GET)
    public SourceMasterResource getSourceMaster(@PathVariable("id") final int id) {
        return sourceMasterService.getSourceMasterResource(id);
    }

    @RequestMapping(value = "sourceMaster/getAll", method = RequestMethod.GET)
    public List<SourceMaster> getAllSourceMaster() {
        return sourceMasterService.getAllSourceMaster();
    }

}