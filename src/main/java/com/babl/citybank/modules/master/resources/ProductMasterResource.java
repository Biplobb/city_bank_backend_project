package com.babl.citybank.modules.master.resources;

import com.babl.citybank.common.ProductType;
import com.babl.citybank.common.Status;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class ProductMasterResource {
    private int id;

    private String productCode;
    private String productName;
    private String description;
    private ProductType productType;
    private LocalDate effectiveFrom;
    private LocalDate validTo;


    private Status status;

    private String createdBy;

    private LocalDateTime createdDate;

    private String modifiedBy;

    private LocalDateTime modifiedDate;

}
