package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.SalutationMasterAddUpdateDTO;
import com.babl.citybank.datasources.oracle.master.repositories.SalutationMasterRepository;
import com.babl.citybank.modules.master.resources.SalutationMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.SalutationMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SalutationMasterService {
    private final SalutationMasterRepository salutationMasterRepository;
    private final AuditLogService auditLogService;

    public boolean addSalutationMaster(SalutationMasterAddUpdateDTO salutationMasterAddUpdateDTO) {
        SalutationMaster salutationMaster =salutationMasterRepository.findBySalutationName(salutationMasterAddUpdateDTO.getSalutationName());
        if(salutationMaster!=null){
            log.info("SalutationMaster already assigned");
            return false;
        }
        salutationMaster = new SalutationMaster();
        salutationMaster.setSalutationName(salutationMasterAddUpdateDTO.getSalutationName());
        salutationMaster.setStatus(salutationMasterAddUpdateDTO.getStatus());
        salutationMasterRepository.save(salutationMaster);
        log.info("SalutationMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("salutation_name", salutationMaster.getSalutationName());
        mapData.put("status", salutationMaster.getStatus());

        auditLogService.addAuditLog(salutationMaster, "salutation_master", salutationMaster.getId(), EventType.INSERT.toString(), mapData.toString());


        return true;
    }

    public boolean updateSalutationMaster(SalutationMasterAddUpdateDTO salutationMasterAddUpdateDTO, int id) {
        SalutationMaster salutationMaster = salutationMasterRepository.findById(id);
        if (salutationMaster == null) {
            log.info("SalutationMaster not found with id = " + id);
            return false;
        }
        SalutationMaster existSalutationMaster = salutationMasterRepository.findBySalutationName(salutationMasterAddUpdateDTO.getSalutationName());
        if(existSalutationMaster!=null){
            log.info("SalutationMaster already assigned");
            return false;
        }
        salutationMaster.setSalutationName(salutationMasterAddUpdateDTO.getSalutationName());
        salutationMaster.setStatus(salutationMasterAddUpdateDTO.getStatus());
        salutationMasterRepository.save(salutationMaster);
        log.info("SalutationMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("salutation_name", salutationMaster.getSalutationName());
        mapData.put("status", salutationMaster.getStatus());

        auditLogService.addAuditLog(salutationMaster, "salutation_master", salutationMaster.getId(), EventType.UPDATE.toString(), mapData.toString());


        return true;
    }

    public boolean deleteSalutationMaster(int id) {
        SalutationMaster salutationMaster = salutationMasterRepository.findById(id);
        if (salutationMaster == null) {
            log.info("SalutationMaster not found with id = " + id);
            return false;
        }
        salutationMasterRepository.delete(salutationMaster);
        log.info("SalutationMaster deleted sucessfully with id = " + id);
        return true;
    }

    public SalutationMasterResource getSalutationMasterResource(int id){
        SalutationMaster salutationMaster = salutationMasterRepository.findById(id);
        if (salutationMaster == null){
            log.info("SalutationMaster not found with id = " + id);
            return null;
        }
        SalutationMasterResource salutationMasterResource = new SalutationMasterResource();
        salutationMasterResource.setId(salutationMaster.getId());
        salutationMasterResource.setSalutationName(salutationMaster.getSalutationName());
        salutationMasterResource.setStatus(salutationMaster.getStatus());
        salutationMasterResource.setCreatedBy(salutationMaster.getCreatedBy());
        salutationMasterResource.setCreatedDate(salutationMaster.getCreatedDate());
        salutationMasterResource.setModifiedBy(salutationMaster.getModifiedBy());
        salutationMasterResource.setModifiedDate(salutationMaster.getModifiedDate());
        log.info("SalutationMaster returned sucessfully with id = " + id);
        return salutationMasterResource;
    }

    public List<SalutationMaster> getAllSalutationMaster() {
        return salutationMasterRepository.findAll();
    }

}