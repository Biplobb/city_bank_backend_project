package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.resources.OccupationMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.OccupationMaster;
import com.babl.citybank.modules.master.dtos.OccupationMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.OccupationMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class OccupationMasterController {
    private final OccupationMasterService occupationMasterService;

    @RequestMapping(value = "occupationMaster/add", method = RequestMethod.POST)
    public StatusResource addOccupationMaster(@RequestBody @Validated final OccupationMasterAddUpdateDTO occupationMasterAddUpdateDTO) {
        boolean status = occupationMasterService.addOccupationMaster(occupationMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "occupationMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateOccupationMaster(@RequestBody @Validated final OccupationMasterAddUpdateDTO occupationMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = occupationMasterService.updateOccupationMaster(occupationMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "occupationMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteOccupationMaster(@PathVariable("id") final int id) {
        boolean status = occupationMasterService.deleteOccupationMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "occupationMaster/get/{id}", method = RequestMethod.GET)
    public OccupationMasterResource getOccupationMaster(@PathVariable("id") final int id) {
        return occupationMasterService.getOccupationMasterResource(id);
    }

    @RequestMapping(value = "occupationMaster/getAll", method = RequestMethod.GET)
    public List<OccupationMaster> getAllOccupationMaster() {
        return occupationMasterService.getAllOccupationMaster();
    }

}