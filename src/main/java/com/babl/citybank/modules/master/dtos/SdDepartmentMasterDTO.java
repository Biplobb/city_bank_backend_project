package com.babl.citybank.modules.master.dtos;

import com.babl.citybank.common.Status;
import lombok.Data;


@Data
public class SdDepartmentMasterDTO {
    private String departmentName;
    private String departmentId;
    private Status status;
}
