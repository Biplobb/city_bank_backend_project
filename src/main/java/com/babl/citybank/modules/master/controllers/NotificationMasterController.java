package com.babl.citybank.modules.master.controllers;


import com.babl.citybank.modules.master.resources.NotificationMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.NotificationMaster;
import com.babl.citybank.modules.master.dtos.NotificationMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.NotificationMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class NotificationMasterController {
    private final NotificationMasterService notificationMasterService;

    @RequestMapping(value = "notificationMaster/add", method = RequestMethod.POST)
    public StatusResource addNotificationMaster(@RequestBody @Validated final NotificationMasterAddUpdateDTO notificationMasterAddUpdateDTO) {
        boolean status = notificationMasterService.addNotificationMaster(notificationMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "notificationMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateNotificationMaster(@RequestBody @Validated final NotificationMasterAddUpdateDTO notificationMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = notificationMasterService.updateNotificationMaster(notificationMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "notificationMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteNotificationMaster(@PathVariable("id") final int id) {
        boolean status = notificationMasterService.deleteNotificationMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "notificationMaster/get/{id}", method = RequestMethod.GET)
    public NotificationMasterResource getNotificationMaster(@PathVariable("id") final int id) {
        return notificationMasterService.getNotificationMasterResource(id);
    }

    @RequestMapping(value = "notificationMaster/getAll", method = RequestMethod.GET)
    public List<NotificationMaster> getAllNotificationMaster() {
        return notificationMasterService.getAllNotificationMaster();
    }

}