package com.babl.citybank.modules.master.dtos;

import com.babl.citybank.common.Status;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class StateMasterAddUpdateDTO {
    @NotEmpty
    private String stateName;
    @NotNull
    private int countryId;
    private Status status;

}