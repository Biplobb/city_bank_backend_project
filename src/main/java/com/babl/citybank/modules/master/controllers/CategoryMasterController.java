package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.resources.CategoryMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.CategoryMaster;
import com.babl.citybank.modules.master.dtos.CategoryMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.CategoryMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CategoryMasterController {
    private final CategoryMasterService categoryMasterService;

    @RequestMapping(value = "categoryMaster/add", method = RequestMethod.POST)
    public StatusResource addCategoryMaster(@RequestBody @Validated final CategoryMasterAddUpdateDTO categoryMasterAddUpdateDTO) {
        boolean status = categoryMasterService.addCategoryMaster(categoryMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "categoryMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateCategoryMaster(@RequestBody @Validated final CategoryMasterAddUpdateDTO categoryMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = categoryMasterService.updateCategoryMaster(categoryMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "categoryMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteCategoryMaster(@PathVariable("id") final int id) {
        boolean status = categoryMasterService.deleteCategoryMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "categoryMaster/get/{id}", method = RequestMethod.GET)
    public CategoryMasterResource getCategoryMaster(@PathVariable("id") final int id) {
        return categoryMasterService.getCategoryMasterResource(id);
    }

    @RequestMapping(value = "categoryMaster/getAll", method = RequestMethod.GET)
    public List<CategoryMaster> getAllCategoryMaster() {
        return categoryMasterService.getAllCategoryMaster();
    }

}