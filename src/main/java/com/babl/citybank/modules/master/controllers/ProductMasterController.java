package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.resources.ProductMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.ProductMaster;
import com.babl.citybank.modules.master.dtos.ProductMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.ProductMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProductMasterController {
    private final ProductMasterService productMasterService;

    @RequestMapping(value = "productMaster/add", method = RequestMethod.POST)
    public StatusResource addProductMaster(@RequestBody @Validated final ProductMasterAddUpdateDTO productMasterAddUpdateDTO) {
        boolean status = productMasterService.addProductMaster(productMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "productMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateProductMaster(@RequestBody @Validated final ProductMasterAddUpdateDTO productMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = productMasterService.updateProductMaster(productMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "productMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteProductMaster(@PathVariable("id") final int id) {
        boolean status = productMasterService.deleteProductMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "productMaster/get/{id}", method = RequestMethod.GET)
    public ProductMasterResource getProductMaster(@PathVariable("id") final int id) {
        return productMasterService.getProductMaster(id);
    }

    @RequestMapping(value = "productMaster/getAll", method = RequestMethod.GET)
    public List<ProductMaster> getAllProductMaster() {
        return productMasterService.getAllProductMaster();
    }

}
