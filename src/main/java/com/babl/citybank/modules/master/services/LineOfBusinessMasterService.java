package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.LineOfBusinessMasterAddUpdateDTO;
import com.babl.citybank.datasources.oracle.master.repositories.LineOfBusinessMasterRepository;
import com.babl.citybank.modules.master.resources.LineOfBusinessMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.LineOfBusinessMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LineOfBusinessMasterService {
    private final LineOfBusinessMasterRepository lineOfBusinessMasterRepository;
    private final AuditLogService auditLogService;

    public boolean addLineOfBusinessMaster(LineOfBusinessMasterAddUpdateDTO lineOfBusinessMasterAddUpdateDTO) {
        LineOfBusinessMaster lineOfBusinessMaster =lineOfBusinessMasterRepository.findByLineOfBusinessName(lineOfBusinessMasterAddUpdateDTO.getLineOfBusinessName());
        if(lineOfBusinessMaster!=null){
            log.info("LineOfBusinessMaster already assigned");
            return false;

        }
        lineOfBusinessMaster = new LineOfBusinessMaster();
        lineOfBusinessMaster.setLineOfBusinessName(lineOfBusinessMasterAddUpdateDTO.getLineOfBusinessName());
        lineOfBusinessMaster.setStatus(lineOfBusinessMasterAddUpdateDTO.getStatus());
        lineOfBusinessMasterRepository.save(lineOfBusinessMaster);
        log.info("LineOfBusinessMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("line_of_business_name", lineOfBusinessMaster.getLineOfBusinessName());
        mapData.put("status", lineOfBusinessMaster.getStatus());

        auditLogService.addAuditLog(lineOfBusinessMaster, "line_of_business_master", lineOfBusinessMaster.getId(), EventType.INSERT.toString(), mapData.toString());


        return true;
    }

    public boolean updateLineOfBusinessMaster(LineOfBusinessMasterAddUpdateDTO lineOfBusinessMasterAddUpdateDTO, int id) {
        LineOfBusinessMaster lineOfBusinessMaster = lineOfBusinessMasterRepository.findById(id);
        if (lineOfBusinessMaster == null) {
            log.info("LineOfBusinessMaster not found with id = " + id);
            return false;
        }
        LineOfBusinessMaster existLineOfBusinessMaster = lineOfBusinessMasterRepository.findByLineOfBusinessName(lineOfBusinessMasterAddUpdateDTO.getLineOfBusinessName());
        if(existLineOfBusinessMaster!=null){
            log.info("LineOfBusinessMaster already assigned");
            return false;
        }
        lineOfBusinessMaster.setLineOfBusinessName(lineOfBusinessMasterAddUpdateDTO.getLineOfBusinessName());
        lineOfBusinessMaster.setStatus(lineOfBusinessMasterAddUpdateDTO.getStatus());
        lineOfBusinessMasterRepository.save(lineOfBusinessMaster);
        log.info("LineOfBusinessMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("line_of_business_name", lineOfBusinessMaster.getLineOfBusinessName());
        mapData.put("status", lineOfBusinessMaster.getStatus());

        auditLogService.addAuditLog(lineOfBusinessMaster, "line_of_business_master", lineOfBusinessMaster.getId(), EventType.UPDATE.toString(), mapData.toString());


        return true;
    }

    public boolean deleteLineOfBusinessMaster(int id) {
        LineOfBusinessMaster lineOfBusinessMaster = lineOfBusinessMasterRepository.findById(id);
        if (lineOfBusinessMaster == null) {
            log.info("LineOfBusinessMaster not found with id = " + id);
            return false;
        }
        lineOfBusinessMasterRepository.delete(lineOfBusinessMaster);
        log.info("LineOfBusinessMaster deleted sucessfully with id = " + id);
        return true;
    }

    public LineOfBusinessMasterResource getLineOfBusinessMasterResource(int id){
        LineOfBusinessMaster lineOfBusinessMaster = lineOfBusinessMasterRepository.findById(id);
        if (lineOfBusinessMaster == null){
            log.info("LineOfBusinessMaster not found with id = " + id);
            return null;
        }
        LineOfBusinessMasterResource lineOfBusinessMasterResource = new LineOfBusinessMasterResource();
        lineOfBusinessMasterResource.setId(lineOfBusinessMaster.getId());
        lineOfBusinessMasterResource.setLineOfBusinessName(lineOfBusinessMaster.getLineOfBusinessName());
        lineOfBusinessMasterResource.setStatus(lineOfBusinessMaster.getStatus());
        lineOfBusinessMasterResource.setCreatedBy(lineOfBusinessMaster.getCreatedBy());
        lineOfBusinessMasterResource.setCreatedDate(lineOfBusinessMaster.getCreatedDate());
        lineOfBusinessMasterResource.setModifiedBy(lineOfBusinessMaster.getModifiedBy());
        lineOfBusinessMasterResource.setModifiedDate(lineOfBusinessMaster.getModifiedDate());
        log.info("LineOfBusinessMaster returned sucessfully with id = " + id);
        return lineOfBusinessMasterResource;
    }

    public List<LineOfBusinessMaster> getAllLineOfBusinessMaster() {
        return lineOfBusinessMasterRepository.findAll();
    }

}