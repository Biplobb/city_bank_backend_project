package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.SegmentMasterAddUpdateDTO;
import com.babl.citybank.datasources.oracle.master.repositories.SegmentMasterRepository;
import com.babl.citybank.modules.master.resources.SegmentMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.SegmentMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SegmentMasterService {
    private final SegmentMasterRepository segmentMasterRepository;
    private final AuditLogService auditLogService;

    public boolean addSegmentMaster(SegmentMasterAddUpdateDTO segmentMasterAddUpdateDTO) {
        SegmentMaster segmentMaster =segmentMasterRepository.findBySegmentName(segmentMasterAddUpdateDTO.getSegmentName());
        if(segmentMaster!=null){
            log.info("SegmentMaster already assigned");
            return false;
        }
        segmentMaster = new SegmentMaster();
        segmentMaster.setSegmentName(segmentMasterAddUpdateDTO.getSegmentName());
        segmentMaster.setStatus(segmentMasterAddUpdateDTO.getStatus());
        segmentMasterRepository.save(segmentMaster);
        log.info("SegmentMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("segment_name", segmentMaster.getSegmentName());
        mapData.put("status", segmentMaster.getStatus());

        auditLogService.addAuditLog(segmentMaster, "segment_master", segmentMaster.getId(), EventType.INSERT.toString(), mapData.toString());


        return true;
    }

    public boolean updateSegmentMaster(SegmentMasterAddUpdateDTO segmentMasterAddUpdateDTO, int id) {
        SegmentMaster segmentMaster = segmentMasterRepository.findById(id);
        if (segmentMaster == null) {
            log.info("SegmentMaster not found with id = " + id);
            return false;
        }
       SegmentMaster existSegmentMaster = segmentMasterRepository.findBySegmentName(segmentMasterAddUpdateDTO.getSegmentName());
        if(existSegmentMaster!=null){
            log.info("SegmentMaster already assigned");
            return false;
        }
        segmentMaster.setSegmentName(segmentMasterAddUpdateDTO.getSegmentName());
        segmentMaster.setStatus(segmentMasterAddUpdateDTO.getStatus());
        segmentMasterRepository.save(segmentMaster);
        log.info("SegmentMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("segment_name", segmentMaster.getSegmentName());
        mapData.put("status", segmentMaster.getStatus());

        auditLogService.addAuditLog(segmentMaster, "segment_master", segmentMaster.getId(), EventType.UPDATE.toString(), mapData.toString());


        return true;
    }

    public boolean deleteSegmentMaster(int id) {
        SegmentMaster segmentMaster = segmentMasterRepository.findById(id);
        if (segmentMaster == null) {
            log.info("SegmentMaster not found with id = " + id);
            return false;
        }
        segmentMasterRepository.delete(segmentMaster);
        log.info("SegmentMaster deleted sucessfully with id = " + id);
        return true;
    }

    public SegmentMasterResource getSegmentMasterResource(int id){
        SegmentMaster segmentMaster = segmentMasterRepository.findById(id);
        if (segmentMaster == null){
            log.info("SegmentMaster not found with id = " + id);
            return null;
        }
        SegmentMasterResource segmentMasterResource = new SegmentMasterResource();
        segmentMasterResource.setId(segmentMaster.getId());
        segmentMasterResource.setSegmentName(segmentMaster.getSegmentName());
        segmentMasterResource.setStatus(segmentMaster.getStatus());
        segmentMasterResource.setCreatedBy(segmentMaster.getCreatedBy());
        segmentMasterResource.setCreatedDate(segmentMaster.getCreatedDate());
        segmentMasterResource.setModifiedBy(segmentMaster.getModifiedBy());
        segmentMasterResource.setModifiedDate(segmentMaster.getModifiedDate());
        log.info("SegmentMaster returned sucessfully with id = " + id);
        return segmentMasterResource;
    }

    public List<SegmentMaster> getAllSegmentMaster() {
        return segmentMasterRepository.findAll();
    }

}