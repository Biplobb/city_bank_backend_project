package com.babl.citybank.modules.master.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetAuditLogDTO {
    private String tableName;

    private LocalDateTime start;

    private LocalDateTime end;
}
