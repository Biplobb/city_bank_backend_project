package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.resources.CountryMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.CountryMaster;
import com.babl.citybank.modules.master.dtos.CountryMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.CountryMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CountryMasterController {
    private final CountryMasterService countryMasterService;

    @RequestMapping(value = "countryMaster/add", method = RequestMethod.POST)
    public StatusResource addCountryMaster(@RequestBody @Validated final CountryMasterAddUpdateDTO countryMasterAddUpdateDTO) {
        boolean status = countryMasterService.addCountryMaster(countryMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "countryMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateCountryMaster(@RequestBody @Validated final CountryMasterAddUpdateDTO countryMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = countryMasterService.updateCountryMaster(countryMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "countryMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteCountryMaster(@PathVariable("id") final int id) {
        boolean status = countryMasterService.deleteCountryMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "countryMaster/get/{id}", method = RequestMethod.GET)
    public CountryMasterResource getCountryMaster(@PathVariable("id") final int id) {
        return countryMasterService.getCountryMasterResource(id);
    }

    @RequestMapping(value = "countryMaster/getAll", method = RequestMethod.GET)
    public List<CountryMaster> getAllCountryMaster() {
        return countryMasterService.getAllCountryMaster();
    }

}