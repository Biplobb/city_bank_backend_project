package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.SourceMasterAddUpdateDTO;
import com.babl.citybank.datasources.oracle.master.repositories.SourceMasterRepository;
import com.babl.citybank.modules.master.resources.SourceMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.SourceMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SourceMasterService {
    private final SourceMasterRepository sourceMasterRepository;
    private final AuditLogService auditLogService;

    public boolean addSourceMaster(SourceMasterAddUpdateDTO sourceMasterAddUpdateDTO) {
        SourceMaster sourceMaster =sourceMasterRepository.findBySourceCodeAndDescription(sourceMasterAddUpdateDTO.getSourceCode(),sourceMasterAddUpdateDTO.getDescription());
        if(sourceMaster!=null){
            log.info("SourceMaster already assigned");
            return false;
        }
        sourceMaster = new SourceMaster();
        sourceMaster.setSourceCode(sourceMasterAddUpdateDTO.getSourceCode());
        sourceMaster.setDescription(sourceMasterAddUpdateDTO.getDescription());
        sourceMaster.setStatus(sourceMasterAddUpdateDTO.getStatus());
        sourceMasterRepository.save(sourceMaster);
        log.info("SourceMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("source_code", sourceMaster.getSourceCode());
        mapData.put("description", sourceMaster.getDescription());
        mapData.put("status", sourceMaster.getStatus());

        auditLogService.addAuditLog(sourceMaster, "source_master", sourceMaster.getId(), EventType.INSERT.toString(), mapData.toString());


        return true;
    }

    public boolean updateSourceMaster(SourceMasterAddUpdateDTO sourceMasterAddUpdateDTO, int id) {
        SourceMaster sourceMaster = sourceMasterRepository.findById(id);
        if (sourceMaster == null) {
            log.info("SourceMaster not found with id = " + id);
            return false;
        }
       SourceMaster existSourceMaster = sourceMasterRepository.findBySourceCodeAndDescription(sourceMasterAddUpdateDTO.getSourceCode(),sourceMasterAddUpdateDTO.getDescription());
        if(existSourceMaster!=null){
            log.info("SourceMaster already assigned");
            return false;
        }
        sourceMaster.setSourceCode(sourceMasterAddUpdateDTO.getSourceCode());
        sourceMaster.setDescription(sourceMasterAddUpdateDTO.getDescription());
        sourceMaster.setStatus(sourceMasterAddUpdateDTO.getStatus());
        sourceMasterRepository.save(sourceMaster);
        log.info("SourceMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("source_code", sourceMaster.getSourceCode());
        mapData.put("description", sourceMaster.getDescription());
        mapData.put("status", sourceMaster.getStatus());

        auditLogService.addAuditLog(sourceMaster, "source_master", sourceMaster.getId(), EventType.UPDATE.toString(), mapData.toString());


        return true;
    }

    public boolean deleteSourceMaster(int id) {
        SourceMaster sourceMaster = sourceMasterRepository.findById(id);
        if (sourceMaster == null) {
            log.info("SourceMaster not found with id = " + id);
            return false;
        }
        sourceMasterRepository.delete(sourceMaster);
        log.info("SourceMaster deleted sucessfully with id = " + id);
        return true;
    }

    public SourceMasterResource getSourceMasterResource(int id){
        SourceMaster sourceMaster = sourceMasterRepository.findById(id);
        if (sourceMaster == null){
            log.info("SourceMaster not found with id = " + id);
            return null;
        }
        SourceMasterResource sourceMasterResource = new SourceMasterResource();
        sourceMasterResource.setId(sourceMaster.getId());
        sourceMasterResource.setSourceCode(sourceMaster.getSourceCode());
        sourceMasterResource.setDescription(sourceMaster.getDescription());
        sourceMasterResource.setStatus(sourceMaster.getStatus());
        sourceMasterResource.setCreatedBy(sourceMaster.getCreatedBy());
        sourceMasterResource.setCreatedDate(sourceMaster.getCreatedDate());
        sourceMasterResource.setModifiedBy(sourceMaster.getModifiedBy());
        sourceMasterResource.setModifiedDate(sourceMaster.getModifiedDate());
        log.info("SourceMaster returned sucessfully with id = " + id);
        return sourceMasterResource;
    }

    public List<SourceMaster> getAllSourceMaster() {
        return sourceMasterRepository.findAll();
    }

}