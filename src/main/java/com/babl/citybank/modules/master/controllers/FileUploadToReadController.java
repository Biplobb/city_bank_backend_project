package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.services.FileUploadToReadService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @_(@Autowired))
public class FileUploadToReadController {
    private  final FileUploadToReadService fileUploadToReadService;
    @RequestMapping(value = "fileUpload/add",method= RequestMethod.POST)
    public void fileUpload(@RequestParam("file") final MultipartFile multipartFile){

        fileUploadToReadService.getMultipartFile(multipartFile);



    }



}
