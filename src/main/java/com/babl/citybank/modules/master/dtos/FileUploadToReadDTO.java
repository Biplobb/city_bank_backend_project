package com.babl.citybank.modules.master.dtos;

import lombok.Data;



@Data
public class FileUploadToReadDTO {

    private String fileName;
}
