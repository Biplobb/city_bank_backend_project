package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.CategoryMasterAddUpdateDTO;
import com.babl.citybank.datasources.oracle.master.repositories.CategoryMasterRepository;
import com.babl.citybank.modules.master.resources.CategoryMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.CategoryMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CategoryMasterService {
    private final CategoryMasterRepository categoryMasterRepository;
    private final AuditLogService auditLogService;

    public boolean addCategoryMaster(CategoryMasterAddUpdateDTO categoryMasterAddUpdateDTO) {
        CategoryMaster categoryMaster =categoryMasterRepository.findByCategoryName(categoryMasterAddUpdateDTO.getCategoryName());
        if(categoryMaster!=null){
           log.info("categoryMaster Already assigned");
           return false;
        }
        categoryMaster = new CategoryMaster();
        categoryMaster.setCategoryName(categoryMasterAddUpdateDTO.getCategoryName());
        categoryMaster.setStatus(categoryMasterAddUpdateDTO.getStatus());
        categoryMasterRepository.save(categoryMaster);
        log.info("CategoryMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("category_name", categoryMaster.getCategoryName());
        mapData.put("status", categoryMaster.getStatus());

        auditLogService.addAuditLog(categoryMaster, "category_master", categoryMaster.getId(), EventType.INSERT.toString(), mapData.toString());


        return true;
    }

    public boolean updateCategoryMaster(CategoryMasterAddUpdateDTO categoryMasterAddUpdateDTO, int id) {
        CategoryMaster categoryMaster = categoryMasterRepository.findById(id);
        if (categoryMaster == null) {
            log.info("CategoryMaster not found with id = " + id);
            return false;
        }
       CategoryMaster existCategoryMaster = categoryMasterRepository.findByCategoryName(categoryMasterAddUpdateDTO.getCategoryName());
        if(existCategoryMaster!=null){
            log.info("CategoryMaster already assigned");
            return false;
        }
        categoryMaster.setCategoryName(categoryMasterAddUpdateDTO.getCategoryName());
        categoryMaster.setStatus(categoryMasterAddUpdateDTO.getStatus());
        categoryMasterRepository.save(categoryMaster);
        log.info("CategoryMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("category_name", categoryMaster.getCategoryName());
        mapData.put("status", categoryMaster.getStatus());

        auditLogService.addAuditLog(categoryMaster, "category_master", categoryMaster.getId(), EventType.UPDATE.toString(), mapData.toString());


        return true;
    }

    public boolean deleteCategoryMaster(int id) {
        CategoryMaster categoryMaster = categoryMasterRepository.findById(id);
        if (categoryMaster == null) {
            log.info("CategoryMaster not found with id = " + id);
            return false;
        }
        categoryMasterRepository.delete(categoryMaster);
        log.info("CategoryMaster deleted sucessfully with id = " + id);
        return true;
    }

    public CategoryMasterResource getCategoryMasterResource(int id){
        CategoryMaster categoryMaster = categoryMasterRepository.findById(id);
        if (categoryMaster == null){
            log.info("CategoryMaster not found with id = " + id);
            return null;
        }
        CategoryMasterResource categoryMasterResource = new CategoryMasterResource();
        categoryMasterResource.setId(categoryMaster.getId());
        categoryMasterResource.setCategoryName(categoryMaster.getCategoryName());
        categoryMasterResource.setStatus(categoryMaster.getStatus());
        categoryMasterResource.setCreatedBy(categoryMaster.getCreatedBy());
        categoryMasterResource.setCreatedDate(categoryMaster.getCreatedDate());
        categoryMasterResource.setModifiedBy(categoryMaster.getModifiedBy());
        categoryMasterResource.setModifiedDate(categoryMaster.getModifiedDate());
        log.info("CategoryMaster returned sucessfully with id = " + id);
        return categoryMasterResource;
    }

    public List<CategoryMaster> getAllCategoryMaster() {
        return categoryMasterRepository.findAll();
    }

}