package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.ProductMasterAddUpdateDTO;
import com.babl.citybank.datasources.oracle.master.repositories.ProductMasterRepository;
import com.babl.citybank.modules.master.resources.ProductMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.ProductMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;


@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProductMasterService {
    private final ProductMasterRepository productMasterRepository;
    private final AuditLogService auditLogService;

    public boolean addProductMaster(ProductMasterAddUpdateDTO productMasterAddUpdateDTO) {
        ProductMaster productMaster =productMasterRepository.findByProductNameAndProductCode(productMasterAddUpdateDTO.getProductName(),productMasterAddUpdateDTO.getProductCode());
        if(productMaster!=null){
            log.info("ProductMaster already assigned!");
            return false;
        }
        productMaster = new ProductMaster();
        productMaster.setProductCode(productMasterAddUpdateDTO.getProductCode());
        productMaster.setProductName(productMasterAddUpdateDTO.getProductName());
        productMaster.setDescription(productMasterAddUpdateDTO.getDescription());
        productMaster.setProductType(productMasterAddUpdateDTO.getProductType());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy");


        StringTokenizer tokenizer1 = new StringTokenizer(productMasterAddUpdateDTO.getValidTo(), ",");
        String validTo = tokenizer1.nextToken();
        System.out.println(validTo);
        productMaster.setValidTo(LocalDate.parse(validTo,formatter));

        StringTokenizer tokenizer = new StringTokenizer(productMasterAddUpdateDTO.getEffectiveFrom(), ",");
        String effectiveFrom = tokenizer.nextToken();

        productMaster.setEffectiveFrom(LocalDate.parse(effectiveFrom,formatter));


        productMaster.setStatus(productMasterAddUpdateDTO.getStatus());
        //System.out.println(productMaster);
        productMasterRepository.save(productMaster);
        log.info("ProductMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("product_code",productMaster.getProductCode());
        mapData.put("product_name",productMaster.getProductName());
        mapData.put("description",productMaster.getDescription());
        mapData.put("product_type",productMaster.getProductType());
        mapData.put("effective_from",productMaster.getEffectiveFrom());
        mapData.put("valid_to",productMaster.getValidTo());
        mapData.put("status", productMaster.getStatus());

        auditLogService.addAuditLog(productMaster, "product_master", productMaster.getId(), EventType.INSERT.toString(), mapData.toString());

        return true;
    }

    public boolean updateProductMaster(ProductMasterAddUpdateDTO productMasterAddUpdateDTO, int id) {
        ProductMaster productMaster = productMasterRepository.findById(id);
        if (productMaster == null) {
            log.info("ProductMaster not found with id = " + id);
            return false;
        }
       ProductMaster existProductMaster = productMasterRepository.findByProductNameAndProductCode(productMasterAddUpdateDTO.getProductName(),productMasterAddUpdateDTO.getProductCode());
        if(existProductMaster!=null){
            log.info("ProductMaster already assigned");
        }


        productMaster.setProductCode(productMasterAddUpdateDTO.getProductCode());
        productMaster.setProductName(productMasterAddUpdateDTO.getProductName());
        productMaster.setDescription(productMasterAddUpdateDTO.getDescription());
        productMaster.setProductType(productMasterAddUpdateDTO.getProductType());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy");

        StringTokenizer tokenizer1 = new StringTokenizer(productMasterAddUpdateDTO.getValidTo(), ",");
        String validTo = tokenizer1.nextToken();
        System.out.println(validTo);
        productMaster.setValidTo(LocalDate.parse(validTo,formatter));

        StringTokenizer tokenizer = new StringTokenizer(productMasterAddUpdateDTO.getEffectiveFrom(), ",");
        String effectiveFrom = tokenizer.nextToken();

        productMaster.setEffectiveFrom(LocalDate.parse(effectiveFrom,formatter));

        productMaster.setStatus(productMasterAddUpdateDTO.getStatus());

        productMasterRepository.save(productMaster);
        log.info("ProductMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("product_code",productMaster.getProductCode());
        mapData.put("product_name",productMaster.getProductName());
        mapData.put("description",productMaster.getDescription());
        mapData.put("product_type",productMaster.getProductType());
        mapData.put("effective_from",productMaster.getEffectiveFrom());
        mapData.put("valid_to",productMaster.getValidTo());

        mapData.put("status", productMaster.getStatus());

        auditLogService.addAuditLog(productMaster, "product_master", productMaster.getId(), EventType.UPDATE.toString(), mapData.toString());


        return true;
    }

    public boolean deleteProductMaster(int id) {
        ProductMaster productMaster = productMasterRepository.findById(id);
        if (productMaster == null) {
            log.info("ProductMaster not found with id = " + id);
            return false;
        }
        productMasterRepository.delete(productMaster);
        log.info("ProductMaster deleted successfully with id = " + id);
        return true;
    }

    public ProductMasterResource getProductMaster(int id) {
        ProductMaster productMaster = productMasterRepository.findById(id);
        if (productMaster == null) {
            log.info("ProductMaster not found with id = " + id);
            return null;
        }
        ProductMasterResource productMasterResource = new ProductMasterResource();
        productMasterResource.setId(productMaster.getId());
        productMasterResource.setProductCode(productMaster.getProductCode());
        productMasterResource.setProductName(productMaster.getProductName());
        productMasterResource.setDescription(productMaster.getDescription());
        productMasterResource.setProductType(productMaster.getProductType());
        productMasterResource.setEffectiveFrom(productMaster.getEffectiveFrom());
        productMasterResource.setValidTo(productMaster.getValidTo());
        productMasterResource.setStatus(productMaster.getStatus());
        productMasterResource.setCreatedBy(productMaster.getCreatedBy());
        productMasterResource.setCreatedDate(productMaster.getCreatedDate());
        productMasterResource.setModifiedBy(productMaster.getModifiedBy());
        productMasterResource.setModifiedDate(productMaster.getModifiedDate());
        return productMasterResource;
    }


    public List<ProductMaster> getAllProductMaster() {
        return productMasterRepository.findAll();
    }

}