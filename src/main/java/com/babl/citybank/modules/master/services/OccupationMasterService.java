package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.OccupationMasterAddUpdateDTO;
import com.babl.citybank.datasources.oracle.master.repositories.OccupationMasterRepository;
import com.babl.citybank.modules.master.resources.OccupationMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.OccupationMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class OccupationMasterService {
    private final OccupationMasterRepository occupationMasterRepository;
    private final AuditLogService auditLogService;

    public boolean addOccupationMaster(OccupationMasterAddUpdateDTO occupationMasterAddUpdateDTO) {
        OccupationMaster occupationMaster =occupationMasterRepository.findByOccupationName(occupationMasterAddUpdateDTO.getOccupationName());
        if(occupationMaster!=null){
            log.info("OccupationMaster already assigned");
            return false;
        }
        occupationMaster = new OccupationMaster();
        occupationMaster.setOccupationName(occupationMasterAddUpdateDTO.getOccupationName());
        occupationMaster.setStatus(occupationMasterAddUpdateDTO.getStatus());
        occupationMasterRepository.save(occupationMaster);
        log.info("OccupationMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("occupation_name", occupationMaster.getOccupationName());
        mapData.put("status", occupationMaster.getStatus());

        auditLogService.addAuditLog(occupationMaster, "occupation_master", occupationMaster.getId(), EventType.INSERT.toString(), mapData.toString());


        return true;
    }

    public boolean updateOccupationMaster(OccupationMasterAddUpdateDTO occupationMasterAddUpdateDTO, int id) {
        OccupationMaster occupationMaster = occupationMasterRepository.findById(id);
        if (occupationMaster == null) {
            log.info("OccupationMaster not found with id = " + id);
            return false;
        }
        OccupationMaster existOccupationMaster = occupationMasterRepository.findByOccupationName(occupationMasterAddUpdateDTO.getOccupationName());
        if(existOccupationMaster!=null){
            log.info("OccupationMaster already assigned");
            return false;
        }
        occupationMaster.setOccupationName(occupationMasterAddUpdateDTO.getOccupationName());
        occupationMaster.setStatus(occupationMasterAddUpdateDTO.getStatus());
        occupationMasterRepository.save(occupationMaster);
        log.info("OccupationMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("occupation_name", occupationMaster.getOccupationName());
        mapData.put("status", occupationMaster.getStatus());

        auditLogService.addAuditLog(occupationMaster, "occupation_master", occupationMaster.getId(), EventType.UPDATE.toString(), mapData.toString());


        return true;
    }

    public boolean deleteOccupationMaster(int id) {
        OccupationMaster occupationMaster = occupationMasterRepository.findById(id);
        if (occupationMaster == null) {
            log.info("OccupationMaster not found with id = " + id);
            return false;
        }
        occupationMasterRepository.delete(occupationMaster);
        log.info("OccupationMaster deleted sucessfully with id = " + id);
        return true;
    }

    public OccupationMasterResource getOccupationMasterResource(int id){
        OccupationMaster occupationMaster = occupationMasterRepository.findById(id);
        if (occupationMaster == null){
            log.info("OccupationMaster not found with id = " + id);
            return null;
        }

        OccupationMasterResource occupationMasterResource = new OccupationMasterResource();
        occupationMasterResource.setId(occupationMaster.getId());
        occupationMasterResource.setOccupationName(occupationMaster.getOccupationName());
        occupationMasterResource.setStatus(occupationMaster.getStatus());
        occupationMasterResource.setCreatedBy(occupationMaster.getCreatedBy());
        occupationMasterResource.setCreatedDate(occupationMaster.getCreatedDate());
        occupationMasterResource.setModifiedBy(occupationMaster.getModifiedBy());
        occupationMasterResource.setModifiedDate(occupationMaster.getModifiedDate());
        log.info("OccupationMaster returned sucessfully with id = " + id);
        return occupationMasterResource;
    }

    public List<OccupationMaster> getAllOccupationMaster() {
        return occupationMasterRepository.findAll();
    }

}