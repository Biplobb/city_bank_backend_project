package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.resources.GenderMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.GenderMaster;
import com.babl.citybank.modules.master.dtos.GenderMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.GenderMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GenderMasterController {
    private final GenderMasterService genderMasterService;

    @RequestMapping(value = "genderMaster/add", method = RequestMethod.POST)
    public StatusResource addGenderMaster(@RequestBody @Validated final GenderMasterAddUpdateDTO genderMasterAddUpdateDTO) {
        boolean status = genderMasterService.addGenderMaster(genderMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "genderMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateGenderMaster(@RequestBody @Validated final GenderMasterAddUpdateDTO genderMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = genderMasterService.updateGenderMaster(genderMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "genderMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteGenderMaster(@PathVariable("id") final int id) {
        boolean status = genderMasterService.deleteGenderMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "genderMaster/get/{id}", method = RequestMethod.GET)
    public GenderMasterResource getGenderMaster(@PathVariable("id") final int id) {
        return genderMasterService.getGenderMasterResource(id);
    }

    @RequestMapping(value = "genderMaster/getAll", method = RequestMethod.GET)
    public List<GenderMaster> getAllGenderMaster() {
        return genderMasterService.getAllGenderMaster();
    }

}