package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.BusinessDivisionMasterAddUpdateDTO;
import com.babl.citybank.datasources.oracle.master.repositories.BusinessDivisionMasterRepository;
import com.babl.citybank.modules.master.resources.BusinessDivisionMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.BusinessDivisionMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BusinessDivisionMasterService {
    private final BusinessDivisionMasterRepository businessDivisionMasterRepository;
    private final AuditLogService auditLogService;

    public boolean addBusinessDivisionMaster(BusinessDivisionMasterAddUpdateDTO businessDivisionMasterAddUpdateDTO) {
        BusinessDivisionMaster businessDivisionMaster =businessDivisionMasterRepository.findByBusinessDivisionName(businessDivisionMasterAddUpdateDTO.getBusinessDivisionName());
        if(businessDivisionMaster!=null){
            log.info("BusinessDivisionMaster already assigned");
            return false;
        }
        businessDivisionMaster = new BusinessDivisionMaster();
        businessDivisionMaster.setBusinessDivisionName(businessDivisionMasterAddUpdateDTO.getBusinessDivisionName());
        businessDivisionMaster.setStatus(businessDivisionMasterAddUpdateDTO.getStatus());
        businessDivisionMasterRepository.save(businessDivisionMaster);
        log.info("BusinessDivisionMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("business_division_name", businessDivisionMaster.getBusinessDivisionName());
        mapData.put("status", businessDivisionMaster.getStatus());

        auditLogService.addAuditLog(businessDivisionMaster, "business_division_master", businessDivisionMaster.getId(), EventType.INSERT.toString(), mapData.toString());


        return true;
    }

    public boolean updateBusinessDivisionMaster(BusinessDivisionMasterAddUpdateDTO businessDivisionMasterAddUpdateDTO, int id) {
        BusinessDivisionMaster businessDivisionMaster = businessDivisionMasterRepository.findById(id);
        if (businessDivisionMaster == null) {
            log.info("BusinessDivisionMaster not found with id = " + id);
            return false;
        }
       BusinessDivisionMaster existBusinessDivisionMaster = businessDivisionMasterRepository.findByBusinessDivisionName(businessDivisionMasterAddUpdateDTO.getBusinessDivisionName());
        if(existBusinessDivisionMaster!=null){
            log.info("BusinessDivisionMaster already assigned");
            return false;
        }
        businessDivisionMaster.setBusinessDivisionName(businessDivisionMasterAddUpdateDTO.getBusinessDivisionName());
        businessDivisionMaster.setStatus(businessDivisionMasterAddUpdateDTO.getStatus());
        businessDivisionMasterRepository.save(businessDivisionMaster);
        log.info("BusinessDivisionMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("business_division_name", businessDivisionMaster.getBusinessDivisionName());
        mapData.put("status", businessDivisionMaster.getStatus());

        auditLogService.addAuditLog(businessDivisionMaster, "business_division_master", businessDivisionMaster.getId(), EventType.UPDATE.toString(), mapData.toString());


        return true;
    }

    public boolean deleteBusinessDivisionMaster(int id) {
        BusinessDivisionMaster businessDivisionMaster = businessDivisionMasterRepository.findById(id);
        if (businessDivisionMaster == null) {
            log.info("BusinessDivisionMaster not found with id = " + id);
            return false;
        }
        businessDivisionMasterRepository.delete(businessDivisionMaster);
        log.info("BusinessDivisionMaster deleted sucessfully with id = " + id);
        return true;
    }

    public BusinessDivisionMasterResource getBusinessDivisionMasterResource(int id){
        BusinessDivisionMaster businessDivisionMaster = businessDivisionMasterRepository.findById(id);
        if (businessDivisionMaster == null){
            log.info("BusinessDivisionMaster not found with id = " + id);
            return null;
        }
        BusinessDivisionMasterResource businessDivisionMasterResource = new BusinessDivisionMasterResource();
        businessDivisionMasterResource.setId(businessDivisionMaster.getId());
        businessDivisionMasterResource.setBusinessDivisionName(businessDivisionMaster.getBusinessDivisionName());
        businessDivisionMasterResource.setStatus(businessDivisionMaster.getStatus());
        businessDivisionMasterResource.setCreatedBy(businessDivisionMaster.getCreatedBy());
        businessDivisionMasterResource.setCreatedDate(businessDivisionMaster.getCreatedDate());
        businessDivisionMasterResource.setModifiedBy(businessDivisionMaster.getModifiedBy());
        businessDivisionMasterResource.setModifiedDate(businessDivisionMaster.getModifiedDate());
        log.info("BusinessDivisionMaster returned sucessfully with id = " + id);
        return businessDivisionMasterResource;
    }


    public List<BusinessDivisionMaster> getAllBusinessDivisionMaster() {
        return businessDivisionMasterRepository.findAll();
    }

}