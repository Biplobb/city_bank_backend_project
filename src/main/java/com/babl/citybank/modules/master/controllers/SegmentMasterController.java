package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.resources.SegmentMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.SegmentMaster;
import com.babl.citybank.modules.master.dtos.SegmentMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.SegmentMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SegmentMasterController {
    private final SegmentMasterService segmentMasterService;

    @RequestMapping(value = "segmentMaster/add", method = RequestMethod.POST)
    public StatusResource addSegmentMaster(@RequestBody @Validated final SegmentMasterAddUpdateDTO segmentMasterAddUpdateDTO) {
        boolean status = segmentMasterService.addSegmentMaster(segmentMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "segmentMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateSegmentMaster(@RequestBody @Validated final SegmentMasterAddUpdateDTO segmentMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = segmentMasterService.updateSegmentMaster(segmentMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "segmentMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteSegmentMaster(@PathVariable("id") final int id) {
        boolean status = segmentMasterService.deleteSegmentMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "segmentMaster/get/{id}", method = RequestMethod.GET)
    public SegmentMasterResource getSegmentMaster(@PathVariable("id") final int id) {
        return segmentMasterService.getSegmentMasterResource(id);
    }

    @RequestMapping(value = "segmentMaster/getAll", method = RequestMethod.GET)
    public List<SegmentMaster> getAllSegmentMaster() {
        return segmentMasterService.getAllSegmentMaster();
    }

}