package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.NotificationMasterAddUpdateDTO;
import com.babl.citybank.datasources.oracle.master.repositories.NotificationMasterRepository;
import com.babl.citybank.modules.master.resources.NotificationMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.NotificationMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class NotificationMasterService {
    private final NotificationMasterRepository notificationMasterRepository;
    private final AuditLogService auditLogService;

    public boolean addNotificationMaster(NotificationMasterAddUpdateDTO notificationMasterAddUpdateDTO) {
        NotificationMaster notificationMaster = new NotificationMaster();
        notificationMaster.setType(notificationMasterAddUpdateDTO.getType());
        notificationMaster.setMessage(notificationMasterAddUpdateDTO.getMessage());
        notificationMaster.setStatus(notificationMasterAddUpdateDTO.getStatus());
        notificationMasterRepository.save(notificationMaster);
        log.info("NotificationMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("type", notificationMaster.getType());
        mapData.put("message",notificationMaster.getMessage());
        mapData.put("status", notificationMaster.getStatus());

        auditLogService.addAuditLog(notificationMaster, "notification_master", notificationMaster.getId(), EventType.INSERT.toString(), mapData.toString());


        return true;
    }

    public boolean updateNotificationMaster(NotificationMasterAddUpdateDTO notificationMasterAddUpdateDTO, int id) {
        NotificationMaster notificationMaster = notificationMasterRepository.findById(id);
        if (notificationMaster == null) {
            log.info("NotificationMaster not found with id = " + id);
            return false;
        }
        notificationMaster.setType(notificationMasterAddUpdateDTO.getType());
        notificationMaster.setMessage(notificationMasterAddUpdateDTO.getMessage());
        notificationMaster.setStatus(notificationMasterAddUpdateDTO.getStatus());
        notificationMasterRepository.save(notificationMaster);
        log.info("NotificationMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("type", notificationMaster.getType());
        mapData.put("message",notificationMaster.getMessage());
        mapData.put("status", notificationMaster.getStatus());

        auditLogService.addAuditLog(notificationMaster, "notification_master", notificationMaster.getId(), EventType.UPDATE.toString(), mapData.toString());

        return true;
    }

    public boolean deleteNotificationMaster(int id) {
        NotificationMaster notificationMaster = notificationMasterRepository.findById(id);
        if (notificationMaster == null) {
            log.info("NotificationMaster not found with id = " + id);
            return false;
        }
        notificationMasterRepository.delete(notificationMaster);
        log.info("NotificationMaster deleted sucessfully with id = " + id);
        return true;
    }

    public NotificationMasterResource getNotificationMasterResource(int id){
        NotificationMaster notificationMaster = notificationMasterRepository.findById(id);
        if (notificationMaster == null){
            log.info("NotificationMaster not found with id = " + id);
            return null;
        }
        NotificationMasterResource notificationMasterResource = new NotificationMasterResource();
        notificationMasterResource.setId(notificationMaster.getId());
        notificationMasterResource.setType(notificationMaster.getType());
        notificationMasterResource.setMessage(notificationMaster.getMessage());
        notificationMasterResource.setStatus(notificationMaster.getStatus());
        notificationMasterResource.setCreatedBy(notificationMaster.getCreatedBy());
        notificationMasterResource.setCreatedDate(notificationMaster.getCreatedDate());
        notificationMasterResource.setModifiedBy(notificationMaster.getModifiedBy());
        notificationMasterResource.setModifiedDate(notificationMaster.getModifiedDate());
        log.info("NotificationMaster returned sucessfully with id = " + id);
        return notificationMasterResource;
    }


    public List<NotificationMaster> getAllNotificationMaster() {
        return notificationMasterRepository.findAll();
    }

}