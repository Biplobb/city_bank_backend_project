package com.babl.citybank.modules.master.resources;

import com.babl.citybank.common.Status;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SubSectorMasterResource {
    private int id;

    private String subSectorCode;

    private String subSectorName;
    private int sectorId;

    private Status status;

    private String createdBy;

    private LocalDateTime createdDate;

    private String modifiedBy;

    private LocalDateTime modifiedDate;

}