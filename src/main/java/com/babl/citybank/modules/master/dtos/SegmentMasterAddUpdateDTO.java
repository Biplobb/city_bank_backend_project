package com.babl.citybank.modules.master.dtos;

import com.babl.citybank.common.Status;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class SegmentMasterAddUpdateDTO {
    @NotEmpty
    private String segmentName;

    private Status status;

}