package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.GenderMasterAddUpdateDTO;
import com.babl.citybank.datasources.oracle.master.repositories.GenderMasterRepository;
import com.babl.citybank.modules.master.resources.GenderMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.GenderMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GenderMasterService {
    private final GenderMasterRepository genderMasterRepository;
    private final AuditLogService auditLogService;

    public boolean addGenderMaster(GenderMasterAddUpdateDTO genderMasterAddUpdateDTO) {
        GenderMaster genderMaster =genderMasterRepository.findByGenderName(genderMasterAddUpdateDTO.getGenderName());
        if(genderMaster!=null){
            log.info("GenderMaster already assigned");
            return false;
        }
        genderMaster = new GenderMaster();
        genderMaster.setGenderName(genderMasterAddUpdateDTO.getGenderName());
        genderMaster.setStatus(genderMasterAddUpdateDTO.getStatus());
        genderMasterRepository.save(genderMaster);
        log.info("GenderMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("gender_name", genderMaster.getGenderName());
        mapData.put("status", genderMaster.getStatus());

        auditLogService.addAuditLog(genderMaster, "gender_master", genderMaster.getId(), EventType.INSERT.toString(), mapData.toString());


        return true;
    }

    public boolean updateGenderMaster(GenderMasterAddUpdateDTO genderMasterAddUpdateDTO, int id) {
        GenderMaster genderMaster = genderMasterRepository.findById(id);
        if (genderMaster == null) {
            log.info("GenderMaster not found with id = " + id);
            return false;
        }
        GenderMaster existGenderMaster = genderMasterRepository.findByGenderName(genderMasterAddUpdateDTO.getGenderName());
        if(existGenderMaster!=null){
            log.info("GenderMaster already assigned");
            return false;
        }
        genderMaster.setGenderName(genderMasterAddUpdateDTO.getGenderName());
        genderMaster.setStatus(genderMasterAddUpdateDTO.getStatus());
        genderMasterRepository.save(genderMaster);
        log.info("GenderMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("gender_name", genderMaster.getGenderName());
        mapData.put("status", genderMaster.getStatus());

        auditLogService.addAuditLog(genderMaster, "gender_master", genderMaster.getId(), EventType.UPDATE.toString(), mapData.toString());


        return true;
    }

    public boolean deleteGenderMaster(int id) {
        GenderMaster genderMaster = genderMasterRepository.findById(id);
        if (genderMaster == null) {
            log.info("GenderMaster not found with id = " + id);
            return false;
        }
        genderMasterRepository.delete(genderMaster);
        log.info("GenderMaster deleted sucessfully with id = " + id);
        return true;
    }

    public GenderMasterResource getGenderMasterResource(int id){
        GenderMaster genderMaster = genderMasterRepository.findById(id);
        if (genderMaster == null){
            log.info("GenderMaster not found with id = " + id);
            return null;
        }
        GenderMasterResource genderMasterResource = new GenderMasterResource();
        genderMasterResource.setId(genderMaster.getId());
        genderMasterResource.setGenderName(genderMaster.getGenderName());
        genderMasterResource.setStatus(genderMaster.getStatus());
        genderMasterResource.setCreatedBy(genderMaster.getCreatedBy());
        genderMasterResource.setCreatedDate(genderMaster.getCreatedDate());
        genderMasterResource.setModifiedBy(genderMaster.getModifiedBy());
        genderMasterResource.setModifiedDate(genderMaster.getModifiedDate());
        log.info("GenderMaster returned sucessfully with id = " + id);
        return genderMasterResource;
    }

    public List<GenderMaster> getAllGenderMaster() {
        return genderMasterRepository.findAll();
    }

}