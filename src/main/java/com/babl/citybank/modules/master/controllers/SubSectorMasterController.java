package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.resources.SubSectorMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.SubSectorMaster;
import com.babl.citybank.modules.master.dtos.SubSectorMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.SubSectorMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SubSectorMasterController {
    private final SubSectorMasterService subSectorMasterService;

    @RequestMapping(value = "subSectorMaster/add", method = RequestMethod.POST)
    public StatusResource addSubSectorMaster(@RequestBody @Validated final SubSectorMasterAddUpdateDTO subSectorMasterAddUpdateDTO) {
        boolean status = subSectorMasterService.addSubSectorMaster(subSectorMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "subSectorMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateSubSectorMaster(@RequestBody @Validated final SubSectorMasterAddUpdateDTO subSectorMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = subSectorMasterService.updateSubSectorMaster(subSectorMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "subSectorMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteSubSectorMaster(@PathVariable("id") final int id) {
        boolean status = subSectorMasterService.deleteSubSectorMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "subSectorMaster/get/{id}", method = RequestMethod.GET)
    public SubSectorMasterResource getSubSectorMaster(@PathVariable("id") final int id) {
        return subSectorMasterService.getSubSectorMasterResource(id);
    }

    @RequestMapping(value = "subSectorMaster/getAll", method = RequestMethod.GET)
    public List<SubSectorMaster> getAllSubSectorMaster() {
        return subSectorMasterService.getAllSubSectorMaster();
    }

}