package com.babl.citybank.modules.master.dtos;

import com.babl.citybank.common.ProductType;
import com.babl.citybank.common.Status;
import lombok.Data;

import javax.validation.constraints.NotEmpty;


@Data
public class ProductMasterAddUpdateDTO {
    @NotEmpty
    private String productCode;
    private String productName;
    private String description;
    private ProductType productType;
    private String effectiveFrom;
    private String validTo;
    private Status status;

}
