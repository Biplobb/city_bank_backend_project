package com.babl.citybank.modules.master.dtos;

import com.babl.citybank.common.Status;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Data
public class BandMasterAddUpdateDTO {
    @NotEmpty
    private String bandNo;

    private Status status;

}