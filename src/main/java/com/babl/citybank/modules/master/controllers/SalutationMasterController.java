package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.resources.SalutationMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.SalutationMaster;
import com.babl.citybank.modules.master.dtos.SalutationMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.SalutationMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SalutationMasterController {
    private final SalutationMasterService salutationMasterService;

    @RequestMapping(value = "salutationMaster/add", method = RequestMethod.POST)
    public StatusResource addSalutationMaster(@RequestBody @Validated final SalutationMasterAddUpdateDTO salutationMasterAddUpdateDTO) {
        boolean status = salutationMasterService.addSalutationMaster(salutationMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "salutationMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateSalutationMaster(@RequestBody @Validated final SalutationMasterAddUpdateDTO salutationMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = salutationMasterService.updateSalutationMaster(salutationMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "salutationMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteSalutationMaster(@PathVariable("id") final int id) {
        boolean status = salutationMasterService.deleteSalutationMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "salutationMaster/get/{id}", method = RequestMethod.GET)
    public SalutationMasterResource getSalutationMaster(@PathVariable("id") final int id) {
        return salutationMasterService.getSalutationMasterResource(id);
    }

    @RequestMapping(value = "salutationMaster/getAll", method = RequestMethod.GET)
    public List<SalutationMaster> getAllSalutationMaster() {
        return salutationMasterService.getAllSalutationMaster();
    }

}