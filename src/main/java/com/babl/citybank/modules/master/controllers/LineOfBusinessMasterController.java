package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.resources.LineOfBusinessMasterResource;
import com.babl.citybank.datasources.oracle.master.schemas.LineOfBusinessMaster;
import com.babl.citybank.modules.master.dtos.LineOfBusinessMasterAddUpdateDTO;
import com.babl.citybank.modules.master.services.LineOfBusinessMasterService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LineOfBusinessMasterController {
    private final LineOfBusinessMasterService lineOfBusinessMasterService;

    @RequestMapping(value = "lineOfBusinessMaster/add", method = RequestMethod.POST)
    public StatusResource addLineOfBusinessMaster(@RequestBody @Validated final LineOfBusinessMasterAddUpdateDTO lineOfBusinessMasterAddUpdateDTO) {
        boolean status = lineOfBusinessMasterService.addLineOfBusinessMaster(lineOfBusinessMasterAddUpdateDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "lineOfBusinessMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateLineOfBusinessMaster(@RequestBody @Validated final LineOfBusinessMasterAddUpdateDTO lineOfBusinessMasterAddUpdateDTO, @PathVariable("id") final int id) {
        boolean status = lineOfBusinessMasterService.updateLineOfBusinessMaster(lineOfBusinessMasterAddUpdateDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "lineOfBusinessMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteLineOfBusinessMaster(@PathVariable("id") final int id) {
        boolean status = lineOfBusinessMasterService.deleteLineOfBusinessMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "lineOfBusinessMaster/get/{id}", method = RequestMethod.GET)
    public LineOfBusinessMasterResource getLineOfBusinessMaster(@PathVariable("id") final int id) {
        return lineOfBusinessMasterService.getLineOfBusinessMasterResource(id);
    }

    @RequestMapping(value = "lineOfBusinessMaster/getAll", method = RequestMethod.GET)
    public List<LineOfBusinessMaster> getAllLineOfBusinessMaster() {
        return lineOfBusinessMasterService.getAllLineOfBusinessMaster();
    }

}