package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.common.StatusResource;
import com.babl.citybank.modules.master.dtos.CustomerOfferDTO;
import com.babl.citybank.modules.master.resources.CustomerOfferResource;
import com.babl.citybank.datasources.oracle.master.schemas.CustomerOffer;
import com.babl.citybank.modules.master.services.CustomerOfferService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;
@RestController
@RequiredArgsConstructor(onConstructor = @_(@Autowired))
public class CustomerOfferController {
    private final CustomerOfferService customerOfferService;
    @RequestMapping(value = "customerOffer/add",method = RequestMethod.POST)
    public StatusResource addCustomerOffer(@RequestBody @Validated CustomerOfferDTO customerOfferDTO){
        boolean status=customerOfferService.addCustomerOffer(customerOfferDTO);
        StatusResource statusResource=new StatusResource("SUCCESSFULL","");
        if(!status){

          statusResource.setStatus("UNSUCCESSFULL");
          return statusResource;
        }
        return statusResource;
    }
    @RequestMapping(value="customerOffer/delete/{id}",method = RequestMethod.DELETE)
    public StatusResource deleteCustomerOffer(@PathVariable("id") final int id){
        boolean status=customerOfferService.deleteCustomerOffer(id);
        StatusResource statusResource=new StatusResource("Successfully","");
        if(!status){
            statusResource.setStatus("Unsuccessfully");
            return statusResource;
        }

        return statusResource;
    }
    @RequestMapping(value = "customerOffer/get/{id}",method = RequestMethod.GET)
    public CustomerOfferResource getCustomerOffer(@PathVariable("id") final int id){
        return customerOfferService.getCustomerOffer(id);
    }
     @RequestMapping(value = "customerOffer/update/{id}",method =RequestMethod.POST)
     public StatusResource updateCustomerOffer(@PathVariable("id") final int id,@RequestBody @Validated CustomerOfferDTO customerOfferDTO){
        boolean status=customerOfferService.updateCustomerOffer(id,customerOfferDTO);

        StatusResource statusResource=new StatusResource("Successfull","");

        if(!status){
            statusResource.setStatus("Unsuccessfull");
            return statusResource;

        }

        return statusResource;

}
    @RequestMapping(value="customerOffer/getAll",method = RequestMethod.GET)
    public List<CustomerOffer> getAllCustomerOffer(){
        return customerOfferService.getAllCustomerOffer();


    }

}
