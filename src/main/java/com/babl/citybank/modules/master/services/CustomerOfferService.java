package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.CustomerOfferDTO;
import com.babl.citybank.datasources.oracle.master.repositories.CustomerOfferRepository;
import com.babl.citybank.datasources.oracle.master.repositories.CustomerOfferTemporaryRepository;
import com.babl.citybank.datasources.oracle.master.repositories.ProductMasterRepository;
import com.babl.citybank.modules.master.resources.CustomerOfferResource;
import com.babl.citybank.datasources.oracle.master.schemas.CustomerOffer;
import com.babl.citybank.datasources.oracle.master.schemas.CustomerOfferTemporary;
import com.babl.citybank.datasources.oracle.master.schemas.ProductMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @_(@Autowired))
@Transactional
public class CustomerOfferService {

    private final CustomerOfferRepository customerOfferRepository;
    private final ProductMasterRepository productMasterRepository;
    private final AuditLogService auditLogService;
    private final CustomerOfferTemporaryRepository customerOfferTemporaryRepository;
    private final EntityManager entityManager;

    public boolean addCustomerOffer(CustomerOfferDTO customerOfferDTO) {

        ProductMaster productMaster = productMasterRepository.findByProductCode(customerOfferDTO.getProductCode());

        if (productMaster == null) {
            return false;
        }
        CustomerOffer customerOffer = new CustomerOffer();
        customerOffer.setCustomerId(customerOfferDTO.getCustomerId());
        customerOffer.setProductCode(customerOfferDTO.getProductCode());
        customerOfferRepository.save(customerOffer);
        log.info(" customerOfferSuccessfully Added");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("customer_id", customerOffer.getCustomerId());
        mapData.put("product_code", customerOffer.getProductCode());
        auditLogService.addAuditLog(customerOffer, "customer_offer", customerOffer.getId(), EventType.INSERT.toString(), mapData.toString());

        return true;


    }

    public boolean deleteCustomerOffer(int id) {
        CustomerOffer customerOffer = customerOfferRepository.findById(id);

        if (customerOffer == null) {
            //log.info("This id is not found");
            return false;
        }
        customerOfferRepository.delete(customerOffer);
        //log.info("Successfully Deleted");
        return true;
    }

    public CustomerOfferResource getCustomerOffer(int id) {
        CustomerOffer customerOffer = customerOfferRepository.findById(id);
        CustomerOfferResource customerOfferResource = new CustomerOfferResource();
        if (customerOffer == null) {
            //log.info("This id is not found");
            return null;

        }
        customerOfferResource.setCustomerId(customerOffer.getCustomerId());
        customerOfferResource.setProductCode(customerOffer.getProductCode());
        return customerOfferResource;
    }

    public boolean updateCustomerOffer(int id, CustomerOfferDTO customerOfferDTO) {
        CustomerOffer customerOffer = customerOfferRepository.findById(id);

        if (customerOffer == null) {
            log.info("This " + id + " is not found");
            return false;

        }


        ProductMaster productMaster = productMasterRepository.findByProductCode(customerOffer.getProductCode());

        if (productMaster == null) {
            return false;

        }

        customerOffer = customerOfferRepository.findById(id);
        customerOffer.setId(customerOffer.getId());
        customerOffer.setCustomerId(customerOfferDTO.getCustomerId());
        customerOffer.setProductCode(customerOfferDTO.getProductCode());
        customerOfferRepository.save(customerOffer);
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("customer_id", customerOffer.getCustomerId());
        mapData.put("product_code", customerOffer.getProductCode());
        auditLogService.addAuditLog(customerOffer, "customer_offer", customerOffer.getId(), EventType.UPDATE.toString(), mapData.toString());

        return true;

    }

    public List<CustomerOffer> getAllCustomerOffer() {
        return customerOfferRepository.findAll();
    }

    public void restrictedData(List<CustomerOfferTemporary> customerOfferTemporaryList) {


        String mappingQuery = "insert into customer_offer (customer_id, product_code)" +
                " SELECT  c.customer_id, c.product_code" +
                " FROM product_master p inner join customer_offer_temporary c" +
                " on p.product_code=c.product_code where SYSDATE between p.effective_from and p.valid_to";


        String deleteQueryString = "delete from customer_offer_temporary";

        //EntityTransaction entityTransaction = entityManager.getTransaction();
        try {

            //entityTransaction.begin();

            Query deleteQuery = entityManager.createNativeQuery(deleteQueryString);
            deleteQuery.executeUpdate();

            for (CustomerOfferTemporary customerOfferTemporary : customerOfferTemporaryList){
                String insertQueryString = "insert into customer_offer_temporary(customer_id, product_code) values('" + customerOfferTemporary.getCustomerId() + "', '" + customerOfferTemporary.getProductCode()+ "')";
                Query insertQuery = entityManager.createNativeQuery(insertQueryString);
                insertQuery.executeUpdate();
            }

            Query query = entityManager.createNativeQuery(mappingQuery);
            int i = query.executeUpdate();
            //entityTransaction.commit();
        }
        catch (Exception e) {
            //entityTransaction.rollback();
            e.printStackTrace();
        }


    }

}
