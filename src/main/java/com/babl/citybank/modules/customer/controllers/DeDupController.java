package com.babl.citybank.modules.customer.controllers;

import com.babl.citybank.datasources.elasticsearch.schemas.DedupSNDResultDocument;
import com.babl.citybank.modules.customer.dtos.DedupDTO;
import com.babl.citybank.modules.customer.dtos.JointDedupDTO;
import com.babl.citybank.modules.customer.services.DeDupSearchService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DeDupController {
    private final DeDupSearchService deDupSearchService;

    @PostMapping(value = "/dedup")
    public Object dedup(@RequestBody DedupDTO dedupDTO){
        return deDupSearchService.dedupEngine(dedupDTO, false);
    }

    @PostMapping(value = "/jointDedup")
    public Object jointDedup(@RequestBody JointDedupDTO jointDedupDTO){
        return deDupSearchService.jointDedupEngine(jointDedupDTO.getCustomerInfoList());
    }

    @PostMapping(value = "/dedup/save")
    public String saveDedup(@RequestBody DedupSNDResultDocument dedupSNDResultDocument){
        return deDupSearchService.saveDedupSNDResult(dedupSNDResultDocument);
    }

    @GetMapping(value = "/dedup/get/{dedupId}")
    public Object getDedupById(@PathVariable("dedupId") String dedupId){
        return deDupSearchService.getById(dedupId);
    }
}
