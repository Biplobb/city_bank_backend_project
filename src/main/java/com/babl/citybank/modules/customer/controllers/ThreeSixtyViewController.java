package com.babl.citybank.modules.customer.controllers;

import com.babl.citybank.datasources.oracle.customer.schemas.Grouping;
import com.babl.citybank.modules.customer.services.ThreeSixtyViewService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ThreeSixtyViewController {

    private final ThreeSixtyViewService threeSixtyViewService;

    @GetMapping("/view/360/{customerId}")
    public Object threeSixtyCoreValue(@PathVariable("customerId") String customerId){
        return threeSixtyViewService.threeSixtyViewService(customerId);
    }


    @GetMapping("/demographic/relatedCB/{customerId}")
    public Object getRelatedCB(@PathVariable("customerId") String customerId){
        return threeSixtyViewService.getRelatedCB(customerId);
    }



    @GetMapping("/view/360/groupProductHoldings/{customerId}")
    public Object getGroupProductHoldings(@PathVariable("customerId") String customerId){
        return threeSixtyViewService.groupProductsHolding(customerId);
    }


    @GetMapping("/demographic/accounts/{customerId}")
    public List<?> demographicAccountList(@PathVariable("customerId") String customerId){
        return threeSixtyViewService.getAllAccount(customerId);
    }

    @GetMapping("/demographic/cards/{customerId}")
    public List<?> demographicCardList(@PathVariable("customerId") String customerId){
        return threeSixtyViewService.getListOfCard(customerId);
    }


    @GetMapping("/demographic/card/{cardNo}")
    public List<?> demographicCardDetails(@PathVariable("cardNo") String cardNo){
        return threeSixtyViewService.getCardDetails(cardNo);
    }

    @GetMapping("/demographic/account/{type}/{accountNo}")
    public Object demographicAccountDetails(@PathVariable("type") String type,
            @PathVariable("accountNo") String accountNo){
        return threeSixtyViewService.getAccountDetails(accountNo, type);
    }



    @RequestMapping(value = "getAllGroupMemberGroupUid/{groupUid}",method = RequestMethod.GET)
    public List<Grouping> getAllGroupMemberListGroupUid(@PathVariable("groupUid") String groupUid){
        return threeSixtyViewService.getAllGroupMemberGroupUid(groupUid);
    }

    @RequestMapping(value = "getAllGroupMember/{customerId}",method = RequestMethod.GET)
    public List<Grouping> getAllGroupMemberList(@PathVariable("customerId") String customerId){
        return threeSixtyViewService.getAllGroupMember(customerId);
    }


    @RequestMapping(value = "servicePropensity/getAll/{productType}/{customerId}",method = RequestMethod.GET)
    public List<?> getAllServicePropensity(@PathVariable("productType") String productType,
                                           @PathVariable("customerId") String customerId){

        return threeSixtyViewService.getAllServicePropensity(productType, customerId);
    }

    @RequestMapping(value = "customerInfo/{cb}", method = RequestMethod.GET)
    public Object getCustomerByCB(@PathVariable("cb") String cb){
        return threeSixtyViewService.getCustomerByCB(cb);
    }


}
