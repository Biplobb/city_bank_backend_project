package com.babl.citybank.modules.customer.dtos;

import lombok.Data;

@Data
public class AddMemberDTO {
    private String customerId;

    private String customerName;

    private String groupUid;
    private String descriptionOfGrouping;

}
