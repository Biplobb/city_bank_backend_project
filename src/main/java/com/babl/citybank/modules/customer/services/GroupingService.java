package com.babl.citybank.modules.customer.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.common.Status;
import com.babl.citybank.datasources.oracle.adminPanel.repositories.UserRepository;
import com.babl.citybank.datasources.oracle.adminPanel.schemas.User;
import com.babl.citybank.modules.customer.dtos.AddMemberDTO;
import com.babl.citybank.modules.customer.dtos.GroupApprovalDTO;
import com.babl.citybank.modules.customer.resources.GroupingApprovalResource;
import com.babl.citybank.modules.master.services.AuditLogService;
import com.babl.citybank.modules.customer.dtos.GroupingDTO;
import com.babl.citybank.datasources.oracle.customer.repositories.GroupingRepository;
import com.babl.citybank.datasources.oracle.customer.schemas.Grouping;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
public class GroupingService {

    @Autowired
    private GroupingRepository groupingRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private EntityManager entityManager;

    public boolean createGroup(GroupingDTO groupingDTO) {
        Grouping grouping = groupingRepository.findByCustomerId(groupingDTO.getKeyCustomerId());

        if (grouping != null) {
            log.info(groupingDTO.getKeyCustomerId() + " already exist!!");
            return false;
        }
        grouping = new Grouping();
        grouping.setCustomerName(groupingDTO.getKeyCustomerName());
        grouping.setCustomerId(groupingDTO.getKeyCustomerId());
        grouping.setDescriptionOfGrouping(groupingDTO.getDescriptionOfGrouping());
        grouping.setGroupUid(groupingDTO.getKeyCustomerId());
        grouping.setDescriptionOfGrouping(groupingDTO.getDescriptionOfGrouping());
        grouping.setRole("KEY");
        grouping.setStatus(Status.WAITING);



        String username = httpServletRequest.getUserPrincipal().getName();
        String auditors = "created_by:" + username;
        User user = userRepository.findByUsername(username);
        grouping.setBranchId(user.getBranch().getId());
        grouping.setAuditors(auditors);


        groupingRepository.save(grouping);
        log.info("Grouping added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("customer_name", grouping.getCustomerName());
        mapData.put("customer_id", grouping.getCustomerId());
        mapData.put("description_of_grouping", grouping.getDescriptionOfGrouping());
        mapData.put("group_uid", grouping.getCustomerId());
        mapData.put("role", "KEY");
        mapData.put("status", grouping.getStatus());
        auditLogService.addAuditLog(grouping, "grouping", grouping.getId(), EventType.INSERT.toString(), mapData.toString());

        for (AddMemberDTO addMemberDTO : groupingDTO.getAddMemberDTOS()) {
            addMemberDTO.setGroupUid(grouping.getGroupUid());
            addMemberDTO.setDescriptionOfGrouping(grouping.getDescriptionOfGrouping());
            addMember(addMemberDTO);

        }
        return true;
    }

    public boolean addMember(AddMemberDTO addMemberDTO) {
        List<Grouping> notExistGrouping = groupingRepository.findByGroupUid(addMemberDTO.getGroupUid());
        if (notExistGrouping == null) {
            return false;
        }

        Grouping grouping = groupingRepository.findByCustomerId(addMemberDTO.getCustomerId());
        if (grouping != null) {
            log.info(addMemberDTO.getCustomerId() + " Already Exist");
            return false;
        }
        grouping = new Grouping();
        grouping.setCustomerId(addMemberDTO.getCustomerId());
        grouping.setCustomerName(addMemberDTO.getCustomerName());
        grouping.setGroupUid(addMemberDTO.getGroupUid());
        grouping.setDescriptionOfGrouping(addMemberDTO.getDescriptionOfGrouping());
        grouping.setRole("ASSOCIATE");
        grouping.setStatus(Status.WAITING);

        String username = httpServletRequest.getUserPrincipal().getName();
        String auditors = "created_by:" + username;
        User user = userRepository.findByUsername(username);
        grouping.setBranchId(user.getBranch().getId());
        grouping.setAuditors(auditors);

        groupingRepository.save(grouping);
        log.info("Member added successfully");
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("customer_name", grouping.getCustomerName());
        mapData.put("customer_id", grouping.getCustomerId());
        mapData.put("description_of_grouping", grouping.getDescriptionOfGrouping());
        mapData.put("group_uid", grouping.getGroupUid());
        mapData.put("role", grouping.getRole());
        mapData.put("status", grouping.getStatus());
        auditLogService.addAuditLog(grouping, "grouping", grouping.getId(), EventType.INSERT.toString(), mapData.toString());
        return true;
    }

    public Object getGroupingForApproval() {
        List<Grouping> groupingList = groupingRepository.findByStatus(Status.WAITING);
        Map<String, GroupingApprovalResource> groupingApprovalResourceHashMap = new HashMap<>();
        //GroupingDTO groupingDTO=new GroupingDTO();

        for (Grouping grouping : groupingList) {
            if (groupingApprovalResourceHashMap.containsKey(grouping.getGroupUid())) {
                GroupingApprovalResource groupingApprovalResource = groupingApprovalResourceHashMap.get(grouping.getGroupUid());
                if (grouping.getRole().equals("KEY")) {
                    groupingApprovalResource.setKeyCustomerName(grouping.getCustomerName());
                    groupingApprovalResource.setKeyCustomerId(grouping.getCustomerId());
                    groupingApprovalResource.getMembersId().add(grouping.getId());
                } else {
                    AddMemberDTO addMemberDTO = new AddMemberDTO();
                    addMemberDTO.setCustomerId(grouping.getCustomerId());
                    addMemberDTO.setCustomerName(grouping.getCustomerName());
                    groupingApprovalResource.getAddMemberDTOS().add(addMemberDTO);
                    groupingApprovalResource.getMembersId().add(grouping.getId());

                }

                groupingApprovalResourceHashMap.put(grouping.getGroupUid(), groupingApprovalResource);


            } else {
                GroupingApprovalResource groupingApprovalResource = new GroupingApprovalResource();
                groupingApprovalResource.setGroupUid(grouping.getGroupUid());
                groupingApprovalResource.setDescriptionOfGrouping(grouping.getDescriptionOfGrouping());
                groupingApprovalResource.setAddMemberDTOS(new ArrayList<>());
                groupingApprovalResource.setMembersId(new ArrayList<>());


                if (grouping.getRole().equals("KEY")) {
                    groupingApprovalResource.setKeyCustomerName(grouping.getCustomerName());
                    groupingApprovalResource.setKeyCustomerId(grouping.getCustomerId());
                    groupingApprovalResource.getMembersId().add(grouping.getId());
                } else {
                    AddMemberDTO addMemberDTO = new AddMemberDTO();
                    addMemberDTO.setCustomerId(grouping.getCustomerId());
                    addMemberDTO.setCustomerName(grouping.getCustomerName());
                    groupingApprovalResource.getAddMemberDTOS().add(addMemberDTO);
                    groupingApprovalResource.getMembersId().add(grouping.getId());

                }

                groupingApprovalResourceHashMap.put(grouping.getGroupUid(), groupingApprovalResource);

            }


        }

        return groupingApprovalResourceHashMap.values();
    }

    public boolean approveMultipleGroupingMember(GroupApprovalDTO groupApprovalDTO) {
        String membersFormattedId = "";
        int i = 1;
        int size = groupApprovalDTO.getMembersId().size();

        for (Integer id : groupApprovalDTO.getMembersId()) {
            membersFormattedId = membersFormattedId + id;
            if (i < size)
                membersFormattedId = membersFormattedId + ", ";

            i++;
        }

        String queryString = "update grouping set status = '"+groupApprovalDTO.getStatus()+"' where id in (" + membersFormattedId + ")";


        try {
            Query query = entityManager.createNativeQuery(queryString);
            query.executeUpdate();
            return true;

        } catch (Exception e) {
            System.out.println(e);

        }

        return false;
    }


    public boolean approveGroupMember(int id) {
        Grouping grouping = groupingRepository.findById(id);
        if (grouping == null) {
            log.info(id + " not exist");
            return false;
        }
        grouping.setStatus(Status.ACTIVE);
        groupingRepository.save(grouping);
        log.info("Succesfully Approved");
        return true;
    }


    public boolean deleteMember(AddMemberDTO addMemberDTO) {
        Grouping grouping = groupingRepository.findByGroupUidAndCustomerId(addMemberDTO.getGroupUid(), addMemberDTO.getCustomerId());
        if (grouping == null) {
            log.info(addMemberDTO.getGroupUid() + "and" + addMemberDTO.getCustomerId() + "Not Exist");
            return false;
        }

        grouping.setStatus(Status.INACTIVE);
        groupingRepository.save(grouping);
        log.info("Member Inactive");
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("customer_id", grouping.getCustomerId());
        mapData.put("group_uid", grouping.getGroupUid());
        mapData.put("status", grouping.getStatus());
        auditLogService.addAuditLog(grouping, "grouping", grouping.getId(), EventType.UPDATE.toString(), mapData.toString());
        return true;
    }

}