package com.babl.citybank.modules.customer.services;


import com.babl.citybank.datasources.elasticsearch.repositories.DedupSNDResultDocumentRepository;
import com.babl.citybank.datasources.elasticsearch.schemas.DedupSNDResultDocument;
import com.babl.citybank.modules.customer.dtos.DedupDTO;
import com.babl.citybank.datasources.elasticsearch.repositories.MappingElasticDocumentRepository;
import com.babl.citybank.modules.customer.resources.DedupResource;
import com.babl.citybank.datasources.elasticsearch.schemas.MappingElasticDocument;
import com.babl.citybank.modules.generic.services.OracleSelectGenericService;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;

@Service
public class DeDupSearchService {

    @Autowired
    private MappingElasticDocumentRepository mappingElasticDocumentRepository;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private OracleSelectGenericService oracleSelectGenericService;

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private DedupSNDResultDocumentRepository dedupSNDResultDocumentRepository;



    private Object relatedCBList(List<String> mainCbs, boolean isJoint){
        String formattedCBListString = "";

        int i = 0;
        for (String cb : mainCbs){
            formattedCBListString = formattedCBListString + "'" + cb + "'";
            i++;

            if (i < mainCbs.size()){
                formattedCBListString = formattedCBListString + ",";
            }
        }


        String queryString = "select maincb, maincustomername, relatedcustid, RELATEDCUSTOMERDESIGNATION from relatedparty where relatedcustid in (" + formattedCBListString + ")";
        List<Object[]> ll =  oracleSelectGenericService.selectFromDatabase(queryString, entityManager);

        List<Map> relatedCustomers = new ArrayList<>();

        for (Object[] l : ll){
            Map<String, String> customer = new HashMap<>();
            customer.put("relatedCB", (String)l[0]);
            customer.put("relatedCustomerName", (String)l[1]);
            customer.put("mainCB", (String)l[2]);
            customer.put("relationship", (String)l[3]);

            MappingElasticDocument document = mappingElasticDocumentRepository.findByMaincustid((String)l[0]);

            if (document != null)
                customer.put("identificationNo", document.getIdentification_no());
            else
                customer.put("identificationNo", null);

            if (isJoint){
//                if (document != null && document.getSz_contitution().equals("JNT")){
//                    relatedCustomers.add(customer);
//                }
                relatedCustomers.add(customer);
            }
            else
                relatedCustomers.add(customer);
        }

        return relatedCustomers;

    }



    private Object relatedDedup(List<String> relatedCbList){
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder()
                .minimumShouldMatch(1);

        for (String relatedCb : relatedCbList){
            boolQueryBuilder.should(matchQuery("maincustid", relatedCb));
        }

        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(boolQueryBuilder)
                .build();

        List<MappingElasticDocument> relatedCustomers =
                elasticsearchTemplate.queryForList(searchQuery, MappingElasticDocument.class);

        return relatedCustomers;

    }


    public DedupResource dedupEngine(DedupDTO dedupDTO, boolean isJoint){
        /*
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(matchQuery("sz_national_id", dedupDTO.getNid()))
                .withQuery(matchQuery("sz_passport", dedupDTO.getPassport())
                .operator(Operator.OR))
                .build();
        List<MappingElasticDocument> customerList = elasticsearchTemplate.queryForList(searchQuery, MappingElasticDocument.class);
        return customerList;*/

        if (dedupDTO.getCbNumber().length() > 0){
            SearchQuery searchQuery = new NativeSearchQueryBuilder()
                    .withQuery(matchQuery("maincustid", dedupDTO.getCbNumber()))
                    .build();
            List<MappingElasticDocument> customerList = elasticsearchTemplate.queryForList(searchQuery, MappingElasticDocument.class);

            Set<MappingElasticDocument> highMatchCustomerSet = new HashSet<>(customerList);

            Map<String, Set> sourceMapping = new HashMap<>();
            List<String> cbList = new ArrayList<>();

            for (MappingElasticDocument mappingElasticDocument : highMatchCustomerSet){
                cbList.add(mappingElasticDocument.getMaincustid());

                if (sourceMapping.containsKey(mappingElasticDocument.getIdentification_no())){
                    Set<String> sources = sourceMapping.get(mappingElasticDocument.getIdentification_no());
                    sources.add(mappingElasticDocument.getSz_source());
                    sourceMapping.put(mappingElasticDocument.getIdentification_no(), sources);
                }
                else{
                    Set<String> sources = new HashSet<>();
                    sources.add(mappingElasticDocument.getSz_source());
                    sourceMapping.put(mappingElasticDocument.getIdentification_no(), sources);
                }
            }

            Object relatedCustomers;

            if (cbList.size() > 0)
                relatedCustomers = relatedCBList(cbList, isJoint);
            else
                relatedCustomers = new ArrayList<>();

            DedupResource dedupResource = new DedupResource();
            dedupResource.setHighMatchCustomers(highMatchCustomerSet);
            dedupResource.setMediumMatchCustomers(new HashSet<>());
            dedupResource.setSourceMapping(sourceMapping);
            dedupResource.setRelatedCustomers(relatedCustomers);

            return dedupResource;

        }

        if (dedupDTO.getDob().trim().length() == 0){
            dedupDTO.setDob("1001-11-07");
        }

        BoolQueryBuilder sourceQueryBuilder = new BoolQueryBuilder().minimumShouldMatch(1);
        for (String source : dedupDTO.getSources()){
            sourceQueryBuilder.should(matchQuery("sz_source", source));
        }


        BoolQueryBuilder tinQueryBuilder = new BoolQueryBuilder().minimumShouldMatch(1);
        if (dedupDTO.getConstitutionType().equals("proprietorship")){
            tinQueryBuilder.should(
                    boolQuery().must(matchQuery("sz_contitution", "proprietorship"))
                    .must(matchQuery("sz_tin_number", dedupDTO.getTin()))
                    .must(matchQuery("sz_full_name", dedupDTO.getCustomerName()).operator(Operator.AND))
            );
        }
        else {
            tinQueryBuilder.should(matchQuery("sz_tin_number", dedupDTO.getTin()));
        }

        BoolQueryBuilder highMatchQueryBuilder = new BoolQueryBuilder()
                .minimumShouldMatch(1)
                .should(matchQuery("sz_national_id", dedupDTO.getNid()))
                .should(matchQuery("sz_passport", dedupDTO.getPassport()))
                .should(tinQueryBuilder)
                .should(matchQuery("sz_registration_no", dedupDTO.getRegistrationNo()))
                .must(sourceQueryBuilder);

        SearchQuery highMatchSearchQuery = new NativeSearchQueryBuilder()
                .withQuery(highMatchQueryBuilder)
                .build();

        List<MappingElasticDocument> highMatchCustomers = elasticsearchTemplate.queryForList(highMatchSearchQuery, MappingElasticDocument.class);

        boolean genderGiven = dedupDTO.getGender() != null && dedupDTO.getGender().trim().length() > 0;
        boolean contitutionGiven = dedupDTO.getConstitutionType() != null && dedupDTO.getConstitutionType().trim().length() > 0;

        BoolQueryBuilder genderContitutionMustNot = new BoolQueryBuilder().minimumShouldMatch(1);
        if (genderGiven) {
//            genderContitutionMustNot.should(
//                    boolQuery().mustNot(matchQuery("c_gender", dedupDTO.getGender()))
//            );
            genderContitutionMustNot.mustNot(matchQuery("c_gender", dedupDTO.getGender())
            );
        }
        if (contitutionGiven) {
//            genderContitutionMustNot.should(
//                    boolQuery().mustNot(matchQuery("sz_contitution", dedupDTO.getConstitutionType()))
//            );
            genderContitutionMustNot.mustNot(matchQuery("sz_contitution", dedupDTO.getConstitutionType())
            );
        }

        BoolQueryBuilder tinMediumQueryBuilder = new BoolQueryBuilder()
                .must(matchQuery("sz_tin_number", dedupDTO.getTin()))
                .must(genderContitutionMustNot);

        BoolQueryBuilder nidMediumQueryBuilder = new BoolQueryBuilder()
                .must(matchQuery("sz_national_id", dedupDTO.getNid()))
                .must(genderContitutionMustNot);

        BoolQueryBuilder passportMediumQueryBuilder = new BoolQueryBuilder()
                .must(matchQuery("sz_passport", dedupDTO.getPassport()))
                .must(genderContitutionMustNot);

        BoolQueryBuilder registrationNoMediumQueryBuilder = new BoolQueryBuilder()
                .must(matchQuery("sz_registration_no", dedupDTO.getRegistrationNo()))
                .must(genderContitutionMustNot);


        BoolQueryBuilder mediumMatchQueryBuilder = new BoolQueryBuilder()
                .minimumShouldMatch(1)
                .should(boolQuery()
                        .must(matchQuery("sz_full_name", dedupDTO.getCustomerName()).operator(Operator.AND))
                        .must(matchQuery("dt_birth", dedupDTO.getDob()))
                )
                .should(boolQuery()
                        .must(matchQuery("sz_full_name", dedupDTO.getCustomerName()).operator(Operator.AND))
                        .must(matchQuery("email_id", dedupDTO.getEmail()))
                )
                .should(boolQuery()
                        .must(matchQuery("sz_full_name", dedupDTO.getCustomerName()))
                        .must(matchQuery("cust_comu_phone_num", dedupDTO.getPhone()))
                )
                .must(sourceQueryBuilder);

        /*
        if (genderGiven || contitutionGiven){
            mediumMatchQueryBuilder.should(tinMediumQueryBuilder)
                    .should(passportMediumQueryBuilder)
                    .should(nidMediumQueryBuilder)
                    .should(registrationNoMediumQueryBuilder);
        }
        */

        ///No mother name
        ///last condition will satisfy if 4th last condition satisfies

        SearchQuery mediumMatchSearchQuery = new NativeSearchQueryBuilder()
                .withQuery(mediumMatchQueryBuilder)
                .build();

        List<MappingElasticDocument> mediumMatchCustomers = elasticsearchTemplate.queryForList(mediumMatchSearchQuery, MappingElasticDocument.class);

        Set<MappingElasticDocument> highMatchCustomerSet = new HashSet<>(highMatchCustomers);
        Set<MappingElasticDocument> mediumMatchCustomerSet = new HashSet<>(mediumMatchCustomers);




        if (mediumMatchCustomerSet != null)
            mediumMatchCustomerSet.removeAll(highMatchCustomerSet);

        Map<String, Set> sourceMapping = new HashMap<>();
        List<String> cBList = new ArrayList<>();

        for (MappingElasticDocument mappingElasticDocument : highMatchCustomerSet){
            cBList.add(mappingElasticDocument.getMaincustid());

            if (sourceMapping.containsKey(mappingElasticDocument.getIdentification_no())){
                Set<String> sources = sourceMapping.get(mappingElasticDocument.getIdentification_no());
                sources.add(mappingElasticDocument.getSz_source());
                sourceMapping.put(mappingElasticDocument.getIdentification_no(), sources);
            }
            else{
                Set<String> sources = new HashSet<>();
                sources.add(mappingElasticDocument.getSz_source());
                sourceMapping.put(mappingElasticDocument.getIdentification_no(), sources);
            }
        }

        for (MappingElasticDocument mappingElasticDocument : mediumMatchCustomerSet){
            cBList.add(mappingElasticDocument.getMaincustid());

            if (sourceMapping.containsKey(mappingElasticDocument.getIdentification_no())){
                Set<String> sources = sourceMapping.get(mappingElasticDocument.getIdentification_no());
                sources.add(mappingElasticDocument.getSz_source());
                sourceMapping.put(mappingElasticDocument.getIdentification_no(), sources);
            }
            else{
                Set<String> sources = new HashSet<>();
                sources.add(mappingElasticDocument.getSz_source());
                sourceMapping.put(mappingElasticDocument.getIdentification_no(), sources);
            }
        }

        Object relatedCustomers;

        if (cBList.size() > 0)
            relatedCustomers = relatedCBList(cBList, isJoint);
        else
            relatedCustomers = new ArrayList<>();

        DedupResource dedupResource = new DedupResource();
        dedupResource.setHighMatchCustomers(highMatchCustomerSet);
        dedupResource.setMediumMatchCustomers(mediumMatchCustomerSet);
        dedupResource.setSourceMapping(sourceMapping);
        dedupResource.setRelatedCustomers(relatedCustomers);

        return dedupResource;
    }


    public DedupResource jointDedupEngine(List<DedupDTO> dedupDTOList){
        DedupResource dedupResource = new DedupResource();
        List<Object> highMatchCustomers = new ArrayList<>();
        List<Object> mediumMatchCustomers = new ArrayList<>();
        Set<Object> relatedCustomers = new HashSet<>();



        for (DedupDTO dedupDTO : dedupDTOList){
            DedupResource dd = dedupEngine(dedupDTO, true);

            highMatchCustomers.add(dd.getHighMatchCustomers());
            mediumMatchCustomers.add(dd.getMediumMatchCustomers());
            relatedCustomers.addAll((List)dd.getRelatedCustomers());
        }

        dedupResource.setHighMatchCustomers(highMatchCustomers);
        dedupResource.setMediumMatchCustomers(mediumMatchCustomers);
        dedupResource.setRelatedCustomers(relatedCustomers);
        return dedupResource;
    }


    public String saveDedupSNDResult(DedupSNDResultDocument resultDocument){
        String username = httpServletRequest.getUserPrincipal().getName();
        resultDocument.setCurrentTime(LocalDateTime.now().toString());
        resultDocument.setUsername(username);
        dedupSNDResultDocumentRepository.save(resultDocument);
        return resultDocument.getId();
    }

    public Optional<DedupSNDResultDocument> getById(String id){
        return dedupSNDResultDocumentRepository.findById(id);
    }

}
