package com.babl.citybank.modules.customer.dtos;

import lombok.Data;

import java.util.List;

@Data
public class JointDedupDTO {
    private List<DedupDTO> customerInfoList;
}
