package com.babl.citybank.modules.customer.services;

import com.babl.citybank.datasources.oracle.customer.repositories.MappingTableRepository;
import com.babl.citybank.datasources.oracle.customer.schemas.MappingTable;
import com.babl.citybank.datasources.oracle.customer.repositories.GroupingRepository;
import com.babl.citybank.datasources.oracle.customer.schemas.Grouping;
import com.babl.citybank.datasources.oracle.master.repositories.CustomerOfferRepository;
import com.babl.citybank.modules.customer.resources.ThreeSixtyResource;
import com.babl.citybank.modules.generic.dtos.SelectGenericDTO;
import com.babl.citybank.modules.generic.services.OracleSelectGenericService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class ThreeSixtyViewService {
    @Autowired
    private OracleSelectGenericService oracleSelectGenericService;

    @Autowired
    private  GroupingRepository groupingRepository;

    @Autowired
    private CustomerOfferRepository customerOfferRepository;

    @Autowired
    private MappingTableRepository mappingTableRepository;

    @Autowired
    private EntityManager entityManager;

    public ThreeSixtyResource threeSixtyViewService(String customerId){
        String latestPriorityCB = getLatestCBPriority(customerId);
        if (latestPriorityCB == null)
            return null;

        SelectGenericDTO selectGenericDTO = new SelectGenericDTO();
        selectGenericDTO.setObjectName("BASICINFO");
        Map<String, Object> keys = new HashMap<>();
        keys.put("identification_no", customerId);
        keys.put("maincustid", latestPriorityCB);
        selectGenericDTO.setSearchFields(keys);

        ThreeSixtyResource threeSixtyResource = new ThreeSixtyResource();
        threeSixtyResource.setCoreValue(oracleSelectGenericService.selectAllQuery(selectGenericDTO, entityManager));
        threeSixtyResource.setGroupDetails(new ArrayList<>());
        threeSixtyResource.setProductXSell(new ArrayList<>());
        threeSixtyResource.setServiceXSell(new ArrayList<>());
        return threeSixtyResource;
    }


    public Object getRelatedCB(String customerId) {
        String relatedCBQuery = "select maincb, relatedcustid, relatedcustomername, relatedcustomerdesignation from mainparty where maincb in(select maincustid from dedup_temp_applicant_ind where " +
                "   identification_no = '"+ customerId +"' and sz_source in ('FINACLE', 'ABABIL'))" +
                "   union " +
                "   select relatedcustid, maincb, maincustomername, maindesignation  from relatedparty where relatedcustid in(select maincustid from dedup_temp_applicant_ind where " +
                "   identification_no = '"+ customerId +"' and sz_source in ('FINACLE', 'ABABIL'))";

        List<Object[]> relatedCBData = oracleSelectGenericService.selectFromDatabase(relatedCBQuery, entityManager);
        List<Map> relatedCBList = new ArrayList<>();

        for (Object[] relatedCB : relatedCBData){
            Map<String, Object> map = new HashMap<>();
            map.put("mainCb", relatedCB[0]);
            map.put("relatedCb", relatedCB[1]);
            map.put("relatedCustomerName", relatedCB[2]);
            map.put("relatedCustomerDesignation", relatedCB[3]);
            relatedCBList.add(map);
        }

        return relatedCBList;

    }

    public List<Grouping> getAllGroupMemberGroupUid(String groupUid) {
        List<Grouping> grouping = groupingRepository.findByGroupUid(groupUid);
        if (grouping == null) {
            log.info(groupUid + "Not Exist");
            return null;
        }
        return groupingRepository.findByGroupUid(groupUid);
    }

    public List<Grouping> getAllGroupMember(String customerId) {
        Grouping grouping = groupingRepository.findByCustomerId(customerId);
        if (grouping == null) {
            log.info(customerId + " Not Exist");
            return null;
        }

        return getAllGroupMemberGroupUid(grouping.getGroupUid());
    }





    public List<?> getAllServicePropensity(String productType, String customerId) {

        String mappingQuery = "SELECT c.product_code,c.customer_id,p.product_type  FROM product_master p " +
                "inner join customer_offer c " +
                "on c.product_code=p.product_code " +
                "where p.product_type= '" + productType + "' and c.customer_id = '" + customerId + "'";
        try {

            Query selectQuery = entityManager.createNativeQuery(mappingQuery);
            List<Object[]> productOfferList = selectQuery.getResultList();
            List<String> productCodeList = new ArrayList<>();

            for (Object[] productOffer : productOfferList) {
                productCodeList.add((String) productOffer[0]);
            }

            return productCodeList;

        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }


    public List<?> getListOfCard(String customerId){
        String queryString = "select clientid, cardholdername, contractno, contracttype, contractstatus from card where clientid in (select maincustid from dedup_temp_applicant_ind" +
                " where identification_no = '" + customerId + "' and sz_source = 'TRANZWARE')";

        List<Object[]> cardsData =  oracleSelectGenericService.selectFromDatabase(queryString, entityManager);

        List<Map> cardDataMap = new ArrayList<>();

        for (Object[] card : cardsData){
            Map<String, Object> map = new HashMap<>();

            map.put("cardNo", card[2]);
            map.put("cardHolderName", card[1]);
            map.put("cardType", card[3]);
            map.put("status", card[4]);

            cardDataMap.add(map);
        }

        return cardDataMap;

    }

    public List<?> getCardDetails(String cardNo){
        SelectGenericDTO selectGenericDTO = new SelectGenericDTO();
        selectGenericDTO.setObjectName("CARD");
        Map<String, Object> keys = new HashMap<>();
        keys.put("contractno", cardNo);
        selectGenericDTO.setSearchFields(keys);
        return oracleSelectGenericService.selectAllQuery(selectGenericDTO, entityManager);
    }

    private String getLatestCBPriority(String customerId){
        String latestSourceQueryString = "select t2.maincustid, t2.sz_source from " +
                "(select max(cust_opn_date) as cdate, sz_source from dedup_temp_applicant_ind where identification_no = '" + customerId + "' group by sz_source ) t1, " +
                "(select * from dedup_temp_applicant_ind where identification_no = '" + customerId + "') t2 " +
                "where t1.cdate = t2.cust_opn_date and t1.sz_source = t2.sz_source";


        Query latestSourceQuery = entityManager.createNativeQuery(latestSourceQueryString);
        try {
            List<Object[]> resultList = latestSourceQuery.getResultList();
            Map<String, String> cbSourceMap = new HashMap<>();

            for (Object[] cbSource : resultList){
                cbSourceMap.put((String)cbSource[1], (String)cbSource[0]);
            }

            if (cbSourceMap.containsKey("FINACLE"))
                return cbSourceMap.get("FINACLE");
            else if(cbSourceMap.containsKey("ABABIL"))
                return cbSourceMap.get("ABABIL");
            else if(cbSourceMap.containsKey("TRANZWARE"))
                return cbSourceMap.get("TRANZWARE");
            else
                return null;
        }
        catch (Exception e){
            System.out.println(e);
        }
        return null;
    }

    public Object getCustomerByCB(String cb){
        /*

        String queryString = "select identification_no, sz_full_name FROM dedup_temp_applicant_ind where maincustid = :mainCustId";

        Map<String, String> valueMap = new HashMap<>();
        try {
            Query query = neutralZoneEntityManager.createNativeQuery(queryString);
            query.setParameter("mainCustId", cb);
            List<Object[]> resultList = query.getResultList();

            if (resultList.size() > 0){
                Object[] firstResult = resultList.get(0);
                valueMap.put("customerId", (String)firstResult[0]);
                valueMap.put("fullName", (String)firstResult[1]);
            }


        }
        catch (Exception e){
            System.out.println(e);
        }

        return valueMap;*/
        MappingTable mappingTable = mappingTableRepository.findByMaincustid(cb);
        Map<String, String> valueMap = new HashMap<>();
        valueMap.put("customerId", mappingTable.getIdentificationNo());
        valueMap.put("fullName", mappingTable.getSzFullName());

        return valueMap;

    }


    public List<String> groupProductsHolding(String customerId){
        String membersIdQueryString = "select customer_id from grouping where group_uid in (" +
                " select group_uid from grouping where customer_id = '"+ customerId +"')";

        List<String> membersId = oracleSelectGenericService.selectFromDatabase(membersIdQueryString, entityManager);

        String membersIdFormatted = "";

        if (membersId.size() == 0)
            membersIdFormatted = "'" + customerId + "'";



        for (int j = 0; j < membersId.size(); j++) {
            membersIdFormatted = membersIdFormatted + "'" + membersId.get(j) + "'";
            if (j < membersId.size() - 1)
                membersIdFormatted  = membersIdFormatted + ",";
        }

        String queryString = "select productname from listofaccount where cust_id in( " +
                " select maincustid from dedup_temp_applicant_ind where " +
                " identification_no in( " + membersIdFormatted + " ) and sz_source in ('FINACLE', 'ABABIL')) " +
                " UNION " +
                " select contracttype from card where clientid in ( " +
                " select maincustid from dedup_temp_applicant_ind where " +
                " identification_no in( " + membersIdFormatted + ") and sz_source = 'TRANZWARE')";

        List<String> productsList = oracleSelectGenericService.selectFromDatabase(queryString, entityManager);

        return productsList;
    }

    public List<?> getAllAccount(String customerId){
        String queryString = "select accountno, cust_id, status, productname, productcode, effectivedate, schm_type from listofaccount where cust_id in( " +
                "    select maincustid from dedup_temp_applicant_ind where " +
                "    identification_no = '"+ customerId +"' and sz_source in ('FINACLE', 'ABABIL'))";

        List<Object[]> accounts = oracleSelectGenericService.selectFromDatabase(queryString, entityManager);

        List<Map> accountList = new ArrayList<>();

        for (Object[] account : accounts){
            Map<String, Object> valueMap = new HashMap();
            valueMap.put("accountNo", account[0]);
            valueMap.put("cbNumber", account[1]);
            valueMap.put("status", account[2]);
            valueMap.put("productName", account[3]);
            valueMap.put("productCode", account[4]);
            valueMap.put("effectiveDate", account[5]);
            valueMap.put("type", account[6]);
            accountList.add(valueMap);
        }

        return accountList;

    }

    public Object getAccountDetails(String accountNo, String type){
        SelectGenericDTO selectGenericDTO = new SelectGenericDTO();

        selectGenericDTO.setObjectName(type);
        Map map = new HashMap<>();

        if (type.equals("CURRENTSAVINGS"))
            map.put("accountno", accountNo);
        else if (type.equals("LOAN"))
            map.put("account", accountNo);
        else if (type.equals("TERMDEPOSIT"))
            map.put("foracid", accountNo);
            
        selectGenericDTO.setSearchFields(map);
        
        return oracleSelectGenericService.selectAllQuery(selectGenericDTO, entityManager);
    }






}
