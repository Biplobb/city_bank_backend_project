package com.babl.citybank.modules.customer.resources;
import com.babl.citybank.common.Status;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class GroupingResource {
    private int id;

    private String customerId;

    private String customerName;

    private String type;

    private String descriptionOfGrouping;

    private String groupUid;

    private String role;

    private String rvValue;

    private Status status;

    private String createdBy;

    private LocalDateTime createdDate;

    private String modifiedBy;

    private LocalDateTime modifiedDate;
}
