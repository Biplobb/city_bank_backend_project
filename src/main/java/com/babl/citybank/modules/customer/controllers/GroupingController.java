package com.babl.citybank.modules.customer.controllers;
import com.babl.citybank.common.Status;
import com.babl.citybank.modules.customer.dtos.AddMemberDTO;
import com.babl.citybank.modules.customer.dtos.GroupApprovalDTO;
import com.babl.citybank.modules.customer.dtos.GroupingDTO;
import com.babl.citybank.modules.customer.services.GroupingService;
import com.babl.citybank.common.StatusResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GroupingController {
    private final GroupingService groupingService;

    @RequestMapping(value = "group/create", method = RequestMethod.POST)
    public StatusResource addGrouping(@RequestBody @Validated final GroupingDTO groupingDTO) {
        boolean status = groupingService.createGroup(groupingDTO);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "grouping/getForApproval", method = RequestMethod.GET)
    public Object getAllForApproval(){
        return groupingService.getGroupingForApproval();
    }

    @RequestMapping(value = "grouping/approve", method = RequestMethod.POST)
    public boolean approveMulipleMember(@RequestBody GroupApprovalDTO groupApprovalDTO){
        return groupingService.approveMultipleGroupingMember(groupApprovalDTO);
    }


    @RequestMapping(value = "groupAndMember/approve/{id}",method = RequestMethod.POST)
    public StatusResource approveGroupAndMember(@RequestBody @Validated GroupingDTO groupingAddUpdateDTO , @PathVariable("id") int id){
        boolean status=groupingService.approveGroupMember(id);
        StatusResource statusResource=new StatusResource("Successfull","");
        if(status){
            return statusResource;
        }
        statusResource.setStatus("unsuccessfull");
        return statusResource;

    }
    @RequestMapping(value = "grouping/addMember",method = RequestMethod.POST)
    public  StatusResource addMember(@RequestBody @Validated AddMemberDTO addMemberDTO ) {
       boolean status=groupingService.addMember(addMemberDTO);
       StatusResource statusResource =new StatusResource("SUCCESSFULL","") ;
       if(status){
           return statusResource;
       }
       statusResource.setStatus("UNSUCCESSFULL");
       return statusResource;

    }

    @RequestMapping(value = "groupMember/delete",method = RequestMethod.POST)
    public StatusResource deleteMember(@RequestBody @Validated AddMemberDTO addMemberDTO){
        boolean message=groupingService.deleteMember(addMemberDTO);
        StatusResource statusResource =new StatusResource("SUCCESSFULL","") ;
        if(message)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }




}
