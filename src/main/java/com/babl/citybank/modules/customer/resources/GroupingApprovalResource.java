package com.babl.citybank.modules.customer.resources;

import com.babl.citybank.common.Status;
import com.babl.citybank.modules.customer.dtos.AddMemberDTO;
import lombok.Data;

import java.util.List;

@Data
public class GroupingApprovalResource {
    private String keyCustomerId;

    private String keyCustomerName;

    private String descriptionOfGrouping;

    private String groupUid;

    private List<AddMemberDTO> addMemberDTOS;

    private List<Integer> membersId;
}
