package com.babl.citybank.modules.customer.dtos;

import com.babl.citybank.common.Status;
import lombok.Data;

import java.util.List;

@Data
public class GroupApprovalDTO {
    private List<Integer> membersId;
    private Status status ;
}
