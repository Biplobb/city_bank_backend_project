package com.babl.citybank.modules.customer.dtos;

import lombok.Data;

import java.util.List;

@Data
public class DedupDTO {
    private String cbNumber;

    private String customerName;

    private String fathersName;

    private String mothersName;

    private String gender;

    private String dob;

    private String phone;

    private String email;

    private String nid;

    private String tin;

    private String passport;

    private String registrationNo;

    private String constitutionType;

    private List<String> sources;
}
