package com.babl.citybank.modules.customer.resources;

import lombok.Data;

import java.util.List;

@Data
public class ThreeSixtyResource {
    private Object coreValue;

    private List<GroupingResource> groupDetails;

    private List<String> productXSell;

    private List<String> serviceXSell;
}
