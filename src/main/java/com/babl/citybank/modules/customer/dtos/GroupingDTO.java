package com.babl.citybank.modules.customer.dtos;
import com.babl.citybank.common.Status;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.List;

@Data
public class GroupingDTO {

    private String keyCustomerId;

    private String keyCustomerName;

/*
    @Pattern(regexp = "FAMILY|CORPORATE")
    private String type;*/

    private String descriptionOfGrouping;


    private String groupUid;

/*
    @Pattern(regexp = "KEY|ASSOCIATE")
    private String role;*/

  /*  @NotEmpty
    private String rvValue;*/


    private Status status;
    private List<AddMemberDTO> addMemberDTOS;

}
