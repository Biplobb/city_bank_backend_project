package com.babl.citybank.modules.customer.services;

import com.babl.citybank.common.ProductType;
import com.babl.citybank.datasources.oracle.customer.repositories.GroupHoldingRepository;
import com.babl.citybank.datasources.oracle.customer.repositories.GroupingRepository;
import com.babl.citybank.datasources.oracle.customer.schemas.GroupHolding;
import com.babl.citybank.datasources.oracle.customer.schemas.Grouping;
import com.babl.citybank.modules.generic.services.OracleSelectGenericService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@EnableScheduling
public class NutralZoneDataSyncService {

    @Autowired
    private GroupHoldingRepository groupHoldingRepository;

    @Autowired
    private GroupingRepository groupingRepository;

    @Autowired
    private OracleSelectGenericService oracleSelectGenericService;

    @Autowired
    private EntityManager entityManager;

    private void Test(){
        Page<Grouping> groupingPage = groupingRepository.findAll(PageRequest.of(0, 100));

        for (int i =1; i<groupingPage.getTotalPages(); i++){
            Map<String, List> groupMembersMap = new HashMap<>();
            for (Grouping grouping : groupingPage){
                List<String> members = groupMembersMap.getOrDefault(grouping.getGroupUid(), new ArrayList());
                members.add(grouping.getCustomerId());
                groupMembersMap.put(grouping.getGroupUid(), members);
            }

            //Map<String, List> groupProductMap = new HashMap<>();
            List<GroupHolding> groupHoldingList = new ArrayList<>();

            for (String groupUid : groupMembersMap.keySet()){
                String membersId = "";

                List<String> members = groupMembersMap.get(groupUid);
                for (int j = 0; j < members.size(); j++) {
                    membersId = membersId + "'" + members.get(j) + "'";
                    if (j < members.size() - 1)
                        membersId  = membersId + ",";
                }

                String queryString = "select productname from listofaccount where cust_id in( " +
                        " select maincustid from dedup_temp_applicant_ind where " +
                        " identification_no in( " + membersId + " ) and sz_source in ('FINACLE', 'ABABIL')) " +
                        " UNION " +
                        " select contracttype from card where clientid in ( " +
                        " select maincustid from dedup_temp_applicant_ind where " +
                        " identification_no in( " + membersId + ") and sz_source = 'TRANZWARE')";

                List<String> productsList = oracleSelectGenericService.selectFromDatabase(queryString, entityManager);

                //groupProductMap.put(groupUid, productsList);
                for (String product : productsList){
                    GroupHolding groupHolding = new GroupHolding();
                    groupHolding.setGroupUid(groupUid);
                    groupHolding.setHoldingName(product);
                    groupHolding.setHoldingsType(ProductType.PRODUCT);
                    groupHoldingList.add(groupHolding);
                }
            }



            groupingPage = groupingRepository.findAll(PageRequest.of(i, 100));
        }


    }



}
