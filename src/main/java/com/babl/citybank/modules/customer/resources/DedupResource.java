package com.babl.citybank.modules.customer.resources;

import lombok.Data;

@Data
public class DedupResource {
    Object highMatchCustomers;
    Object mediumMatchCustomers;
    Object sourceMapping;
    Object relatedCustomers;

}
