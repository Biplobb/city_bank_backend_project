package com.babl.citybank;



import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.util.*;

public class Test {
//    private static  String toJson(Map<String, Object> hashMap){
//
//    }

    public static void relatedCBList(List<String> mainCbs){
        String formattedCBListString = "";

        int i = 0;
        for (String cb : mainCbs){
            formattedCBListString = formattedCBListString + "'" + cb + "'";
            i++;

            if (i < mainCbs.size()){
                formattedCBListString = formattedCBListString + ",";
            }
        }

        System.out.println(formattedCBListString);

    }


    public static boolean checkCondition(String expression, Map<String, String> variables) {
        String[] ss = expression.split("[ =()&|]+");
        List<Boolean> boolList = new ArrayList();

        int i = 0;

        if (expression.length() > 0 && expression.charAt(0) == '(')
            i = 1;


        for (; i < ss.length; ) {
            /*if (ss[i].equals("and") || ss[i].equals("or"))
                i++;*/
            System.out.println(i);
            boolean val = variables.get(ss[i]) == null ? ss[i + 1].equals("null") : variables.get(ss[i]).equals(ss[i + 1]);

            boolList.add(val);
            i = i + 2;
        }


        Stack<Object> stack = new Stack<>();

        int boolTaken = 0;


        for (int j = 0; j < expression.length(); j++) {
            if (j + 3 < expression.length() && expression.substring(j, j + 2).equals("&&")) {
                stack.push("and");
                j = j + 2;
            } else if (j + 2 < expression.length() && expression.substring(j, j + 2).equals("==")) {
                stack.push(boolList.get(boolTaken));
                boolTaken++;
                j++;
            } else if (j + 2 < expression.length() && expression.substring(j, j + 2).equals("||")) {
                stack.push("or");
                j++;
            } else if (expression.charAt(j) == '(')
                stack.push("(");
            else if (expression.charAt(j) == ')') {
                //stack.push(")");
                boolean bb = (boolean) stack.pop();

                while (!stack.peek().equals("(")) {
                    Object op = stack.pop();
                    boolean two = (boolean) stack.pop();
                    if (op.equals("and"))
                        bb = bb && two;
                    else
                        bb = bb || two;
                }
                stack.pop();
                stack.push(bb);
            }

        }


        boolean result = (boolean) stack.pop();
        while (!stack.empty()) {
            Object op = stack.pop();
            boolean two = (boolean) stack.pop();
            if (op.equals("and"))
                result = result && two;
            else
                result = result || two;
        }

        return result;

    }

    public static void main(String[] args){
        String csx = "(cs_deferal==YES  && bm_approval == null)|| cs_bearer == BM || cs_bearer == BOTH";
        Map<String, String> map = new HashMap<>();

        //map.put("bom_approval", "APPROVED");
        map.put("cs_deferal", "YES");

        System.out.println(checkCondition(csx, map));

    }
}
