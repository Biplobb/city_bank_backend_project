package com.babl.citybank.lib;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DatePickerFormatToLocaldate {
    public static LocalDate convert(String date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy");
        return LocalDate.parse(date, formatter);
    }
}
