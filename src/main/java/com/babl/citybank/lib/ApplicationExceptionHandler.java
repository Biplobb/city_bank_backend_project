package com.babl.citybank.lib;


import com.babl.citybank.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ApplicationExceptionHandler {
    @ExceptionHandler(StartCaseException.class)
    public ResponseEntity<ApiError> handleStartCaseException(StartCaseException exception){
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, "This user can not start case");
        return new ResponseEntity<ApiError>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CaseNotFoundException.class)
    public ResponseEntity<ApiError> handleCaseNoFoundException(CaseNotFoundException exception){
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, "Case not found");
        return new ResponseEntity<ApiError>(apiError, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UserAccessException.class)
    public ResponseEntity<ApiError> handleCaseNoFoundException(UserAccessException exception){
        ApiError apiError = new ApiError(HttpStatus.FORBIDDEN, "You are not permitted to perform this operation");
        return new ResponseEntity<ApiError>(apiError, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(VariableNotSetException.class)
    public ResponseEntity<ApiError> handleVariableNotSetException(VariableNotSetException variableNotSetException){
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, variableNotSetException.getMessage());
        return new ResponseEntity<ApiError>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ObjectNotFoundException.class)
    public ResponseEntity<ApiError> handleObjectNotFoundException(ObjectNotFoundException notFoundException){
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, notFoundException.getMessage());
        return new ResponseEntity<ApiError>(apiError, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(FormatException.class)
    public ResponseEntity<ApiError> handleFormatException(FormatException formatException){
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, formatException.getMessage());
        return new ResponseEntity<ApiError>(apiError, HttpStatus.BAD_REQUEST);
    }
}
