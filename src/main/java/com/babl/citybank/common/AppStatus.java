package com.babl.citybank.common;

public enum  AppStatus {
    ACTIVE, CLOSED, CANCELLED
}
