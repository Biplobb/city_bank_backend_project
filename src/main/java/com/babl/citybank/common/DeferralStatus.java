package com.babl.citybank.common;

public enum DeferralStatus {
    ACTIVE, INACTIVE, APPROVAL_WAITING, CLOSE_WAITING, NOTAPPROVED
}
