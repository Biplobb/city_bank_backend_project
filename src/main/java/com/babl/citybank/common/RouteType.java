package com.babl.citybank.common;

public enum RouteType {
    ALWAYS, CONDITIONAL, PARALLEL, SEC_JOIN
}
