package com.babl.citybank.common;

public enum AssignType {
    EVALUATE, SELF_SERVICE, CYCLICAL
}
