package com.babl.citybank.common;

public enum ApprovalStatus {
    APPROVED, REJECTED, RETURNED
}
